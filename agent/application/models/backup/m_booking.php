<?php

class m_booking extends CI_Model {
	
	private $table = 'products', $id = 'id' ;

	public function get_total($aColumns,$sWhere){

		$result= $this->db->query("SELECT COUNT(*) TOTAL FROM (
		SELECT a.*, b.`userID` as agentName, c.`productName`, d.`depdate`, 
			(SELECT aa.`gfname` FROM `booking_detail` aa WHERE aa.`bookingID`=a.`id` ORDER BY aa.id ASC LIMIT 1) AS guestName,
            CASE a.`bookingStatus`
                WHEN '0' THEN 'WL'
                WHEN '1' THEN 'DP-NP'
                WHEN '2' THEN 'DP-R'
                WHEN '3' THEN 'FP-NP'
                WHEN '4' THEN 'FP-R'
                WHEN '5' THEN 'COMPLETED'
                ELSE 'CANCEL'
            END AS ketStatus 
		FROM `booking` a
		INNER JOIN `ms_agent` b ON a.`agentID`=b.`id`
		INNER JOIN `products` c ON a.`productID`=c.`id`
		INNER JOIN `products_date` d ON a.`depdateID`=d.`id`) z WHERE 0=0 $sWhere");
		foreach ($result->result() as $row) {
			$total = $row->TOTAL;
		}
		return $total;
	}

	public function get_data($sLimit,$sWhere,$sOrder,$aColumns){
		$sql = "SELECT * FROM (
		SELECT a.*, b.`userID` as agentName, c.`productName`, d.`depdate`, 
			(SELECT aa.`gfname` FROM `booking_detail` aa WHERE aa.`bookingID`=a.`id` ORDER BY aa.id ASC LIMIT 1) AS guestName,
            CASE a.`bookingStatus`
                WHEN '0' THEN 'WL'
                WHEN '1' THEN 'DP-NP'
                WHEN '2' THEN 'DP-R'
                WHEN '3' THEN 'FP-NP'
                WHEN '4' THEN 'FP-R'
                WHEN '5' THEN 'COMPLETED'
                ELSE 'CANCEL'
            END AS ketStatus  
		FROM `booking` a
		INNER JOIN `ms_agent` b ON a.`agentID`=b.`id`
		INNER JOIN `products` c ON a.`productID`=c.`id`
		INNER JOIN `products_date` d ON a.`depdateID`=d.`id`) z
		WHERE 0=0 $sWhere $sOrder $sLimit";

		//echo $sql; exit;
		$result = $this->db->query($sql);
		$result = $result->result();
		return $result;
	}

	public function get_data_all($sLimit,$sWhere,$sOrder,$aColumns){
		$sql = "SELECT * FROM (
		SELECT a.*, b.`agentName`, c.`productName`, d.`depdate`, IF(a.`bookingStatus`=0,'WL',IF(a.`bookingStatus`=1,'DP-NP',IF(a.`bookingStatus`=2,'DP-R',IF(a.`bookingStatus`=3,'FP-NP',IF(a.`bookingStatus`=4,'FP-R',IF(a.`bookingStatus`=5,'COMPLETED','CANCEL')))))) as ketStatus FROM `booking` a
		INNER JOIN `ms_agent` b ON a.`agentID`=b.`id`
		INNER JOIN `products` c ON a.`productID`=c.`id`
		INNER JOIN `products_date` d ON a.`depdateID`=d.`id`) z
		WHERE 0=0 $sWhere $sOrder $sLimit";

		//echo $sql; exit;
		$result = $this->db->query($sql);
		$result = $result->result();
		return $result;
	}

	function del($data){
		$this->db->where($this->id,$data);
        $this->db->update($this->table, array('flag' => 1));
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            // any trans error?
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
	}

	function add($data){
		$this->db->insert('booking', $data);
		if($this->db->affected_rows()){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}

	function add_detil($data){
		$this->db->insert('booking_detail', $data);
		if($this->db->affected_rows()){
			return true;
		}else{
			return false;
		}
	}

	function add_log($data){
		$this->db->insert('booking_log', $data);
		if($this->db->affected_rows()){
			return true;
		}else{
			return false;
		}
	}

	function add_depdate($data,$id){
		$array =array();
		for($i=0;$i<count($data);$i++){
			if($data[$i]!=''){
				//depdate|allotment|remarks|dpAmount|patt|pas|pcwa|pcwb|pcnb|surcharge|surcharge2|discount|discount2
				$pecah_data = explode("|", $data[$i]);
				$pecah_depdate = explode("-", $pecah_data[0]); //dd-mm-yyyy
				$array['depdate'] 		= $pecah_depdate[2].'-'.$pecah_depdate[1].'-'.$pecah_depdate[0];
				$array['allotment'] 	= $pecah_data[1];
				$array['remarks']		= $pecah_data[2];
				$array['dpAmount']		= $pecah_data[3];
				$array['patt']			= $pecah_data[4];
				$array['pas']			= $pecah_data[5];
				$array['pcwa']			= $pecah_data[6];
				$array['pcwb']			= $pecah_data[7];
				$array['pcnb']			= $pecah_data[8];
				$array['surcharge']		= $pecah_data[9];
				$array['surcharge2']	= $pecah_data[10];
				$array['discount']		= $pecah_data[11];
				$array['discount2']		= $pecah_data[12];

				$array['productID']		= $id;
				$this->db->insert('products_date', $array);
			}
		}

	}

	function add_room($data,$id){
		$array =array();
		for($i=0;$i<count($data);$i++){
			$array['roomID']		= $data[$i];
			$array['productID']		= $id;
			$this->db->insert('products_room', $array);
		}
	}

	function edit($data){
		$this->db->where($this->id,$data[$this->id]);
        $this->db->update($this->table, $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            // any trans error?
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
	}

	function get_departure(){
		$this->db->select('*');
        $this->db->from('ms_departure');
        $this->db->where('flag', '0'); 
        $query = $this->db->get();


        if ($query->num_rows() > 0){
        	return $query->result();
        }else{
        	return NULL;
        }
	}

	function get_airline(){
		$this->db->select('*');
        $this->db->from('ms_airline');
        $this->db->where('flag', '0'); 
        $query = $this->db->get();


        if ($query->num_rows() > 0){
        	return $query->result();
        }else{
        	return NULL;
        }		
	}
	
	function get_category(){
		$this->db->select('*');
        $this->db->from('ms_category');
        $this->db->where('flag', '0'); 
        $query = $this->db->get();


        if ($query->num_rows() > 0){
        	return $query->result();
        }else{
        	return NULL;
        }		
	}

	function get_room($id){

		$this->db->select('ms_room_setting.*');
        $this->db->from('products_room');
        $this->db->join('ms_room_setting', 'products_room.roomID = ms_room_setting.id');
        $this->db->where('products_room.productID', $id); 
        $query = $this->db->get();

        if ($query->num_rows() > 0){
        	return $query->result();
        }else{
        	return NULL;
        }		
	}

	function get_depdate($id){
		$this->db->select('*');
        $this->db->from('products_date');
        $this->db->where('productID', $id); 
        $query = $this->db->get();


        if ($query->num_rows() > 0){
        	return $query->result();
        }else{
        	return NULL;
        }
	}

	function get_detil($id){
		$this->db->select('products.*,ms_departure.cityName,ms_airline.airlineName,ms_category.categoryName');
        $this->db->from('products');
        $this->db->join('ms_departure', 'products.departureID = ms_departure.id');
        $this->db->join('ms_category', 'products.categoryID = ms_category.id');
        $this->db->join('ms_airline', 'products.airlineID = ms_airline.id');
        $this->db->where('products.id', $id); 
        $query = $this->db->get();


        if ($query->num_rows() > 0){
        	return $query->row(); //Only 1 data
        }else{
        	return NULL;
        }
	}

	function get_booking($id){
		$this->db->select('booking.*,products_date.depdate,ms_room_setting.settingName,ms_agent.agentName');
        $this->db->from('booking');
        $this->db->join('products_date', 'products_date.id=booking.depdateID');
        $this->db->join('ms_room_setting', 'ms_room_setting.id=booking.roomID');
        $this->db->join('ms_agent', 'ms_agent.id=booking.agentID');
        $this->db->where('booking.id', $id); 
        $query = $this->db->get();


        if ($query->num_rows() > 0){
        	return $query->result();
        }else{
        	return NULL;
        }
	}

	function get_guest($id){
		$this->db->select('*');
        $this->db->from('booking_detail');
        $this->db->where('booking_detail.bookingID', $id); 
        $query = $this->db->get();


        if ($query->num_rows() > 0){
        	return $query->result();
        }else{
        	return NULL;
        }
	}

	function allotment_used($productID, $depdateID){
		$this->db->select('COUNT(*) as TOTAL');
        $this->db->from('booking');
		$this->db->join('booking', 'booking_detail.bookingID = booking.id');
        $this->db->where('booking.productID', $productID); 
        $this->db->where('booking.depdateID', $depdateID); 
        $this->db->where('booking.bookingStatus<', '6'); 
        $query = $this->db->get();
        return $query->row();
	}

	function allotment($productID, $depdateID){
		$this->db->select('products_date.allotment as TOTAL');
        $this->db->from('products_domain');
        $this->db->where('productID', $productID); 
        $this->db->where('id', $depdateID); 
        $query = $this->db->get();
        return $query->row();
	}

	function get_upload_file($bookingID,$typeID){
		$this->db->select('*');
		$this->db->from('booking_payment');
		$this->db->where('bookingID',$bookingID);
		$this->db->where('typeID',$typeID);
		$this->db->order_by('id','desc');
		//$this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() > 0){
        	return $query->row();
        }else{
        	return NULL;
        }
	}

	function get_upload_file_invoice($bookingID){
		$this->db->select('*');
		$this->db->from('booking_invoice');
		$this->db->where('bookingID',$bookingID);
		$this->db->order_by('id','desc');
		//$this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() > 0){
        	return $query->row();
        }else{
        	return NULL;
        }
	}

	function cancel($id){
		$this->db->where('id',$id);
        $this->db->update('booking', array('bookingStatus' => 6));
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            // any trans error?
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
	}

	function chengestatus($id,$newstatus){
		$this->db->where('id',$id);
        $this->db->update('booking', array('bookingStatus' => $newstatus));
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            // any trans error?
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
	}

	function upload($data){
		$this->db->insert('booking_invoice', $data);
		if($this->db->affected_rows()){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}

	/* == DATA BOOKING DETIL ONLY 1 15/11 ==*/
	function get_booking_detil($id){
		$this->db->select('booking.*,products_date.depdate,ms_room_setting.settingName,ms_agent.agentName,ms_agent_staff.staffName');
        $this->db->from('booking');
        $this->db->join('products_date', 'products_date.id=booking.depdateID');
        $this->db->join('ms_room_setting', 'ms_room_setting.id=booking.roomID');
        $this->db->join('ms_agent', 'ms_agent.id=booking.agentID');
        $this->db->join('ms_agent_staff', 'ms_agent_staff.id=booking.createdBy');
        $this->db->where('booking.id', $id); 
        $query = $this->db->get();


        if ($query->num_rows() > 0){
        	return $query->row();
        }else{
        	return NULL;
        }
	}

	function get_depdate_detil($id){
		$this->db->select('*');
        $this->db->from('products_date');
        $this->db->where('id', $id); 
        $query = $this->db->get();

        if ($query->num_rows() > 0){
        	return $query->row();
        }else{
        	return NULL;
        }
	}

	function get_room_detil($id){
		$this->db->select('ms_room_setting.*');
        $this->db->from('ms_room_setting');
        $this->db->where('id', $id); 
        $query = $this->db->get();

        if ($query->num_rows() > 0){
        	return $query->row();
        }else{
        	return NULL;
        }		
	}

	function get_guest_detil($id){
		$this->db->select('*');
        $this->db->from('booking_detail');
        $this->db->where('booking_detail.bookingID', $id); 
        $query = $this->db->get();

        if ($query->num_rows() > 0){
        	return $query->result();
        }else{
        	return NULL;
        }
	}

    function get_guest_total($id){
        $this->db->select('COUNT(*) as TOTAL');
        $this->db->from('booking_detail');
        $this->db->where('booking_detail.bookingID', $id); 
        $query = $this->db->get();

        if ($query->num_rows() > 0){
            return $query->row();
        }else{
            return NULL;
        }
    }

    function edit_guest($data){
        $this->db->where('id',$data[$this->id]);
        $this->db->update('booking_detail', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            // any trans error?
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function edit_timelimit($data){
        $this->db->where('id',$data[$this->id]);
        $this->db->update('booking', $data);
        if ($this->db->affected_rows() == '1') {
            return TRUE;
        } else {
            // any trans error?
            if ($this->db->trans_status() === FALSE) {
                return false;
            }
            return true;
        }
    }

    function stock_allotment($depdateID, $status){
        $this->db->select('COUNT(*) AS TOTAL');
        $this->db->from('booking_detail a');
        $this->db->join('booking b', 'a.bookingID=b.id');
        $this->db->where('b.depdateID', $depdateID); 
        $this->db->where('b.bookingStatus', $status); 
        $query = $this->db->get();

        if ($query->num_rows() > 0){
                return $query->row(); return $query->row();
        }else{
                return NULL;
        }
    }

    function all_allotment($id){
        $this->db->select('allotment as TOTAL');
        $this->db->from('products_date');
        $this->db->where('id', $id); 
        $query = $this->db->get();

        if ($query->num_rows() > 0){
            return $query->row();
        }else{
            return NULL;
        }
    }

    /* == ADD GNERATE INVOICE 22/11 ==*/
    function add_generate($data){
        $this->db->insert('generate', $data);
        if($this->db->affected_rows()){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    /* == FINAL FILES FROM AGENT 24/11 ==*/
    function get_final_files($bookingID){
        $this->db->select('*');
        $this->db->from('booking_files');
        $this->db->where('bookingID',$bookingID);
        $this->db->order_by('id','desc');
        //$this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() > 0){
            return $query->row();
        }else{
            return NULL;
        }
    }
}

?>