<?php

class m_agent extends CI_Model {
	
	private $table = 'ms_agent', $id = 'id' ;

	public function get_total($aColumns,$sWhere){
		$result= $this->db->query("SELECT COUNT(*) TOTAL FROM $this->table z WHERE 0=0 $sWhere");
		foreach ($result->result() as $row) {
			$total = $row->TOTAL;
		}
		return $total;
	}

	public function get_data($sLimit,$sWhere,$sOrder,$aColumns){
		/*
		$sql    = "SELECT ".implode($aColumns,',').", (SELECT COUNT(*) FROM `m_foto_a` b WHERE a.`id`=b.`aktifitasID`) AS jml FROM $this->table 
			INNER JOIN `users` c ON a.`staffID`=c.`id` 
			WHERE 0=0 $sWhere $sOrder $sLimit";
		*/
		$sql = "SELECT * FROM $this->table z WHERE 0=0 $sWhere $sOrder $sLimit";

		$result = $this->db->query($sql);
		$result = $result->result();
		return $result;
	}

	function del($data){
		/*
		$this->db->where($this->id, $data);
		$this->db->delete($this->table);
		if($this->db->affected_rows()){ 
			return true;
		}else{
			return false;
		}
		*/
		$this->db->where($this->id,$data[$this->id]);
        $this->db->update($this->table, $data);
        if ($this->db->affected_rows() == '1') {
		    return TRUE;
		} else {
		    // any trans error?
		    if ($this->db->trans_status() === FALSE) {
		        return false;
		    }
		    return true;
		}
	}

	function add($data){
		/*
		$last_id = $this->db->select($this->id)->limit("1")->order_by($this->id,"desc")->get($this->table)->result_array();
        
        if ($last_id) {
        	$data[$this->id] =$last_id[0][$this->id]+1;
        }else{
        	$data[$this->id] =1;
        }
        */
		$this->db->insert($this->table, $data);
		if($this->db->affected_rows()){
			return $this->db->insert_id();
			//return true;
		}else{
			return false;
		}
	}

	function add_staff($data){
		$this->db->insert('ms_agent_staff', $data);
		if($this->db->affected_rows()){
			return true;
		}else{
			return false;
		}
	}

	function edit($data){
		$this->db->where($this->id,$data[$this->id]);
        $this->db->update($this->table, $data);
        if ($this->db->affected_rows() == '1') {
		    return TRUE;
		} else {
		    // any trans error?
		    if ($this->db->trans_status() === FALSE) {
		        return false;
		    }
		    return true;
		}
		/*
        if($this->db->affected_rows()){
            return true;
        }
        else{
            return false;
        }
        */
	}

	function edit_setting($data){
		$this->db->where($this->id,$data[$this->id]);
        $this->db->update($this->table, $data);
        if ($this->db->affected_rows() == '1') {
			$this->db->where('agentID', $data[$this->id]);
			$this->db->where('staffUsername', 'superid');
			$this->db->where('flag', '0');
        	$this->db->update('ms_agent_staff', array('staffPassword'=>'123456'));
		    return TRUE;
		} else {
		    // any trans error?
		    if ($this->db->trans_status() === FALSE) {
		        return false;
		    }
			$this->db->where('agentID', $data[$this->id]);
			$this->db->where('staffUsername', 'superid');
			$this->db->where('flag', '0');
        	$this->db->update('ms_agent_staff', array('staffPassword'=>'123456'));
		    return true;
		}
	}
}

?>