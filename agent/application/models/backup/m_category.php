<?php

class m_category extends CI_Model {
	function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
    }
	
	private $table = 'ms_category', $id = 'id' ;

	public function get_total($aColumns,$sWhere){
		$result= $this->db->query("SELECT COUNT(*) TOTAL FROM $this->table z WHERE 0=0 $sWhere");
		foreach ($result->result() as $row) {
			$total = $row->TOTAL;
		}
		return $total;
	}

	public function get_data($sLimit,$sWhere,$sOrder,$aColumns){
		$sql = "SELECT * FROM $this->table z WHERE 0=0 $sWhere $sOrder $sLimit";

		$result = $this->db->query($sql);
		$result = $result->result();
		return $result;
	}

	function del($data){
		$this->db->where($this->id,$data[$this->id]);
        $this->db->update($this->table, $data);
        if ($this->db->affected_rows() == '1') {
		    return TRUE;
		} else {
		    // any trans error?
		    if ($this->db->trans_status() === FALSE) {
		        return false;
		    }
		    return true;
		}
	}

	function add($data){
		$this->db->insert($this->table, $data);
		if($this->db->affected_rows()){
			return true;
		}else{
			return false;
		}
	}

	function edit($data){
		$this->db->where($this->id,$data[$this->id]);
        $this->db->update($this->table, $data);
        if ($this->db->affected_rows() == '1') {
		    return TRUE;
		} else {
		    // any trans error?
		    if ($this->db->trans_status() === FALSE) {
		        return false;
		    }
		    return true;
		}
	}

	function get_product($categoryID){
		$this->db->select('a.categoryName,b.productName,b.highlight,b.id,(SELECT COUNT(*) FROM products_date WHERE products_date.productID=b.id AND products_date.depdate>="'.date('Y-m-d').'" AND products_date.flag=0) as TOTAL');
		$this->db->from('ms_category a');
		$this->db->join('products b', 'a.id=b.categoryID');
		$this->db->where('a.id',$categoryID);
		$this->db->order_by('b.productName','asc');
		$query = $this->db->get();

        if ($query->num_rows() > 0){
        	return $query->result();
        }else{
        	return NULL;
        }
	}

	function get_allproduct($depdateID){
		$this->db->select('a.categoryName,b.productName,c.depdate,c.id,c.allotment');
		$this->db->from('ms_category a');
		$this->db->join('products b', 'a.id=b.categoryID');
		$this->db->join('products_date c', 'b.id=c.productID');
		$this->db->where('b.id',$depdateID);
		$this->db->where('c.depdate >=',date('Y-m-d'));
		$this->db->where('c.flag','0');
		$this->db->order_by('b.productName','asc');
		$this->db->order_by('c.depdate','asc');
		$query = $this->db->get();

        if ($query->num_rows() > 0){
        	return $query->result();
        }else{
        	return NULL;
        }
	}

	function get_cat($categoryID){
		$this->db->select('a.categoryName');
		$this->db->from('ms_category a');
		$this->db->where('a.id',$categoryID);
		$query = $this->db->get();

        if ($query->num_rows() > 0){
        	return $query->row();
        }else{
        	return NULL;
        }
	}
}

?>