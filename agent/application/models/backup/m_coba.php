<?php

class m_coba extends CI_Model {
	
	function get_agentlist(){
	$this->db->select('a.*, COUNT(c.id) AS paxs');
        $this->db->from('ms_agent a');
        $this->db->join('booking b','b.agentID = a.id');
        $this->db->join('booking_detail c','c.bookingID = b.id');
        $this->db->where('a.flag','0');
        $this->db->where('b.bookingStatus <','6');
        $this->db->group_by('a.id'); 
        $this->db->having('COUNT(c.id) > 0');
        $this->db->order_by('paxs', 'desc'); 
        //$this->db->limit(10);
        $query = $this->db->get();


        if ($query->num_rows() > 0){
                return $query->result();
        }else{
                return NULL;
        }

	}

}

?>