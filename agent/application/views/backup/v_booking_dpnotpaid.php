<?php include "include_header.php"; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<script type="text/javascript" src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>

<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box red">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-table"></i>
					<small>Booking List - CONFIRMED (DEPOSIT NOT PAID)</small>
				</div>
			</div>
			<div class="portlet-body">
				<div id="view-table">
					<div class="table-toolbar"></div>
					<table class="table table-striped table-bordered table-hover" id="refrensi-table">
						<thead>
							<tr>
								<th data-sortable="false">NO</th>
								<th>BOOKED</th>
								<th>PNR</th>
								<th>AGENT</th>
								<th>PRODUCT</th>
								<th>DEPARTURE</th>
								<th>GUEST</th>
								<th>TIME LIMIT</th>
								<th data-sortable="false">ACTION</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_detil" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">View Payment Slip</h4>
            </div>
        <form role="form" class="form-horizontal" id="form_master" method="post" enctype="multipart/form-data">
        <input type="hidden" name="bookingID" id="bookingID">
            <div class="modal-body"> 
            <fieldset>
            	<div class="form-group">
					<label class="col-md-3 control-label" for="inputSuccess">Bank Name</label>
					<div class="col-md-6">
						<b><span id="ket_bankName"></span></b>
					</div>
				</div>
				<br>
            	<div class="form-group">
					<label class="col-md-3 control-label" for="inputSuccess">Account Name</label>
					<div class="col-md-6">
						<b><span id="ket_accountName"></span></b>
					</div>
				</div>
				<br>
            	<div class="form-group">
					<label class="col-md-3 control-label" for="inputSuccess">Account No</label>
					<div class="col-md-6">
						<b><span id="ket_accountNo"></span></b>
					</div>
				</div>
				<br>
				<div class="form-group">
					<label class="col-md-3 control-label" for="inputSuccess">Transfer Date</label>
					<div class="col-md-6">
						<b><span id="ket_transferDate"></span></b>
					</div>
				</div>
            	<div class="form-group">
					<label class="col-md-3 control-label" for="inputSuccess">Payment Slip</label>
					<div class="col-md-6">
						<b><span id="ket_fileDownload"></span></b>
					</div>
				</div>
            </fieldset>
            </div>
        </form>
            <div class="modal-footer">
                <button type="submit" class="btn dark btn-outline" id="btnSave" onClick="chengestatus()" >Approved Payment & Change Status to Received</button>
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- == Detil Booking Pop up 14/11 == -->
<div class="modal fade" id="modal_detil_booking" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">View Payment Slip</h4>
            </div>
            <div class="modal-body"> 
				<div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">General Information</h3>
                    </div>
                    <div class="panel-body">
                    	<table>
                    		<tr>
                    			<td width="30%">Booking No</td>
                    			<td>-</td>
                    		</tr>
                    	</table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script> 
	var TableAdvanced = function () {
	var initTable1 = function() {

		/*
		* Initialize DataTables, with no sorting on the 'details' column
		*/
		var target='#refrensi-table';
		var oTable = $(target).dataTable( {
			"aoColumnDefs": [
				{
					//"bSortable": false, "aTargets": [ 0,1 ]
				}
			],
			"aoColumns": [
                { "sWidth": "5%" },  
                { "sWidth": "10%" }, 
                { "sWidth": "10%" }, 
                { "sWidth": "15%" }, 
                { "sWidth": "25%" }, 
                { "sWidth": "10%" },
                { "sWidth": "10%" },
                { "sWidth": "10%" },
                { "sWidth": "15%" }
            ],
			"aaSorting": [[1, 'asc']],
			"aLengthMenu": [
				[10, 20, 50, -1],
				[10, 20, 50, "All"] // change per page values here
			],
			// set the initial value
			"iDisplayLength": 10, // default records per page
			"oLanguage": {
				// language settings
				"sLengthMenu": "Display _MENU_ records",
				"sSearch": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
				"sProcessing": '<img src="<?php echo base_url() ?>assets/global/img/loading-spinner-grey.gif"><span>&nbsp;&nbsp;Loading...</span>',
				"sInfoEmpty": "No records found to show",
				"sAjaxRequestGeneralError": "Could not complete request. Please check your internet connection",
				"sEmptyTable":  "No data available in table",
				"sZeroRecords": "No matching records found",
				"oPaginate": {
					"sPrevious": "Prev",
					"sNext": "Next",
					"sPage": "Page",
					"sPageOf": "of"
				}
			},
			"bAutoWidth": true,   // disable fixed width and enable fluid table
			"bSortCellsTop": true, // make sortable only the first row in thead
			"sPaginationType": "bootstrap_full_number", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
			"bProcessing": true, // enable/disable display message box on record load
			"bServerSide": true, // enable/disable server side ajax loading
			"sAjaxSource": "<?=base_url()?>booking_dpnotpaid/get_data", // define ajax source URL
			"sServerMethod": "POST"
		});

		$(target+'_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
		$(target+'_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
		$(target+'_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
		$(target+'_wrapper .dataTables_filter input').unbind();
		$(target+'_wrapper .dataTables_filter input').bind('keyup', function(e) {
			if(e.keyCode == 13) {
				oTable.fnFilter(this.value);
			}
		});
		$(target+'_wrapper .dataTables_filter a').bind('click', function(e) {
			var key=$(target+'_wrapper .dataTables_filter input').val();
			oTable.fnFilter(key);
		});
	}

	return {

		//main function to initiate the module
		init: function () {
			if (!$().dataTable) {
				return;
			}
			initTable1();
		}

	};

}();

$(document).ready(function() {
	TableAdvanced.init();
	
});

function loadTbl(){
	$("#refrensi-table").dataTable().fnDraw();
}

function set_del(data){
	if(confirm("Are you sure to cancel this booking?")){
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url() ?>booking/cancel',
			data: { id : data },
			success: function(data) {
				if(data=='true'){
					//$('#cancel').click();
					loadTbl();
				}
			}
		});
	}
}

function set_view(data){
	var myWindow = window.open("<?php echo base_url('booking_dpnotpaid/detil/') ?>/"+data, "", "width=500,height=700");
}

function chengestatus(){
	if(confirm("Are you sure to Approved Payment & Change Status to Received?")){
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url() ?>booking/chengestatus',
			data: { 'id' : $('#bookingID').val(), 'newstatus' : '2' },
			success: function(data) {
				if(data=='true'){
					//$('#cancel').click();
					$('#modal_detil').modal('hide');
					loadTbl();
				}
			}
		});
	}
}

function set_download(data){
	var isi=data.split('|');
	$('#bookingID').val(isi[0]);
	$('#ket_bankName').html(isi[2]);
	$('#ket_accountName').html(isi[3]);
	$('#ket_accountNo').html(isi[4]);
	$('#ket_transferDate').html(isi[5]);
	if(isi[1]!=''){
		$('#ket_fileDownload').html('<a href="http://kuoni.wuisan.com/booking/uploads/'+isi[1]+'" target="_blank"><i class="fa fa-download"></i> Download</a>');
	}
	$('#modal_detil').modal('show');
}

function view_detil(data){
	$('#modal_detil_booking').modal('show');
}
</script>

<?php include "include_footer.php"; ?>