<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="The Great Holiday - Booking Reservation Application | Travel Agent" name="description" />
        <meta content="Multikreasi Abadi Selaras" name="author" />
        <?php include "additional_plugin_header_login.php"; ?>
    <!-- END HEAD -->

    <body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="<?php echo base_url('login/') ?>">
                <img src="http://thegreatholiday.com/logo.png" alt="" />
            </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <?php
                $attributes = array(
                    'class'         => 'reset-form', 
                    'id'            => 'form_reset', 
                    'autocomplete'  => 'off'
                );
                echo form_open('login/reset_proccess', $attributes);
                echo '<h3 class="form-title font-green">Insert your email here</h3>';
                echo validation_errors('<div class="alert alert-danger display-hide">','</div>');
            ?>
            
            <div class="form-group">
                <?php
                    echo '<label class="control-label visible-ie8 visible-ie9">Email</label>';
                    $data = array(
                        'name'          => 'email',
                        'id'            => 'email',
                        'maxlength'     => '100',
                        'size'          => '50',
                        'class'         => 'form-control form-control-solid placeholder-no-fix',
                        'placeholder'   => 'Email'
                    );
                    echo form_input($data);
                ?>
            </div>
            <a href="<?php echo base_url('login/home') ?>"> << Back to Login area</a>
            <div class="form-actions">
            <?php
                echo form_submit('btn_login','Next','class="btn green uppercase"'); 
            ?>
            </div>
            <?php 
                echo form_close(); 
            ?>
            <!-- END LOGIN FORM -->
        </div>
        <div class="copyright"> 2016 © Multikreasi Abadi Selaras</div>
        <?php include "additional_plugin_footer_login.php"; ?>
    </body>

</html>