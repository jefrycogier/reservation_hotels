<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="The Great Holiday - Booking Reservation Application | Travel Agent" name="description" />
        <meta content="Multikreasi Abadi Selaras" name="author" />
        <?php include "additional_plugin_header_login.php"; ?>
    <!-- END HEAD -->

    <body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="<?php echo base_url('login/') ?>">
                <img src="http://thegreatholiday.com/logo.png" alt="" />
            </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->

        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <?php
            //$query = $this->db->query("select * from hotel_employee where email =".$email."");
            $query = $this->db->query('SELECT * FROM ms_admin WHERE email="'.$email.'"');
            //$result = $query->result();
            //$test = $query->result_array();
            $row = $query->row();
            
                $attributes = array(
                    'class'         => 'login-form', 
                    'id'            => 'form_login', 
                    'autocomplete'  => 'off'
                );
                echo form_open('login/send_email', $attributes);
                echo '<h3 class="form-title font-green">Reset Password</h3>';
                echo validation_errors('<div class="alert alert-danger display-hide">','</div>');
            ?>
            
            
            <div class="form-group">
            <?php
            $data = array(
                    'name'          => 'newpassword',
                    'id'            => 'newpassword',
                    'maxlength'     => '100',
                    'size'          => '50',
                    'class'         => 'form-control form-control-solid placeholder-no-fix',
                    'placeholder'   => 'New Password'
            );

            echo form_password($data);
            ?>
            </div>
            <div class="form-group">
            <?php
            $data = array(
                    'name'          => 'repassword',
                    'id'            => 'repassword',
                    'maxlength'     => '100',
                    'size'          => '50',
                    'class'         => 'form-control form-control-solid placeholder-no-fix',
                    'placeholder'   => 'Re-type Password'
            );

            echo form_password($data);
            echo form_hidden('name',$row->adminName);
            echo form_hidden('email',$row->email);
            ?>
            </div>
            <a href="<?php echo base_url('login/home') ?>"> << Back to Login area</a>
            <div class="form-actions">
            <?php
                echo form_submit('btn_login','Reset','class="btn green uppercase"'); 
            ?>
            </div>
            <?php 
                echo form_close(); 
            ?>
            <!-- END LOGIN FORM -->
        </div>
        <div class="copyright"> 2016 © Multikreasi Abadi Selaras</div>
        <?php include "additional_plugin_footer_login.php"; ?>
    </body>

</html>