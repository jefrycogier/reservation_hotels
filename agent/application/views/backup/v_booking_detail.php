<?php include "include_header.php"; ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<script type="text/javascript" src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bars"></i> <b>Booking Details</b> 
                </div>
                <div class="actions">
                    <a href="<?php echo base_url()?>booking_all" class="btn btn-danger btn-sm">
                        <i class="fa fa-undo"></i> Back </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6 col-sm-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-info-circle"></i> <b>General Information</b> </div>
            </div>
            <div class="portlet-body">
                <div class="row static-info">
                    <div class="col-md-3 name"> PNR / Booked : </div>
                    <div class="col-md-9 value"> <?php echo $data_booking->bookingCode; ?> / <?php echo strtoupper(date("d M", strtotime($data_booking->createdDate))); ?></div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name"> Agent / Staff : </div>
                    <div class="col-md-9 value"> <?php echo strtoupper($data_booking->agentName.' / '.$data_booking->staffName); ?> </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name"> Status : </div>
                    <div class="col-md-9 value">
                    <?php
                        if ($data_booking->bookingStatus == '0') {
                            $status = '<span class="label label-sm label-default">Waiting List</span>';
                        }else if ($data_booking->bookingStatus == '1') {
                            $status = '<span class="label label-sm label-info">Confirmed (Deposit Not Paid)</span>';
                        }else if ($data_booking->bookingStatus == '2') {
                            $status = '<span class="label label-sm label-success">Confirmed (Deposit Received)</span>';
                        }else if ($data_booking->bookingStatus == '3') {
                            $status = '<span class="label label-sm label-warning">Confirmed (Full Payment Not Paid)</span>';
                        }else if ($data_booking->bookingStatus == '4') {
                            $status = '<span class="label label-sm label-primary">Confirmed (Full Payment Received)</span>';
                        }else if ($data_booking->bookingStatus == '5') {
                            $status = '<span class="label label-sm label-danger">Completed</span>';
                        }else{
                            $status = '<span class="label label-sm label-default">Cancel</span>';
                        }
                        echo $status;
                    ?>
                    </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name"> Time Limit :</div>
                    <div class="col-md-9 value"> <?php if($data_booking->bookingStatus==1){ echo strtoupper(date("d M", strtotime($data_booking->timelimit))); ?>
                        <a class="btn btn-xs grey-cascade" onclick="edittimelimit('<?php echo date('m/d/Y', strtotime($data_booking->timelimit)); ?>')"><i class="fa fa-pencil"></i> Edit</a>
                    <?php } else { echo '-'; } ?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-info-circle"></i> <b>Product Information</b> </div>
                <div class="actions">
                <?php if($data_product->fileItinerary!=''){ ?>
                    <a href="http://thegreatholiday.com/booking/admin/product_files/<?php echo $data_product->fileItinerary; ?>" target="_blank" class="btn btn-info btn-sm">
                        <i class="fa fa-download"></i> Download Itinerary </a>
                <?php } ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row static-info">
                    <div class="col-md-3 name"> Product Name : </div>
                    <div class="col-md-9 value"> <?php echo strtoupper($data_product->productName); ?> </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name"> Airline : </div>
                    <div class="col-md-9 value"> <?php echo $data_product->airlineName; ?> </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name"> Departure : </div>
                    <div class="col-md-9 value"> <?php echo $data_product->cityName; ?> </div>
                </div>
                <div class="row static-info">
                    <div class="col-md-3 name"> Days : </div>
                    <div class="col-md-9 value"> <?php echo $data_product->noofdays.'D '.$data_product->noofnights.'N'; ?> </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-info-circle"></i> <b>Guest Information</b> </div>
            </div>
            <div class="portlet-body">
                <div class="row static-info">
                    <div class="col-md-2 name"> Departure Date : </div>
                    <div class="col-md-10 value"> <?php echo strtoupper(date("d M", strtotime($data_depdate->depdate))); ?></div>
                </div>
                <div class="row static-info">
                    <div class="col-md-2 name"> Room Type : </div>
                    <div class="col-md-10 value"> <?php echo $data_room->settingName; ?></div>
                </div>
                <div class="row static-info">
                    <div class="col-md-12">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <th>No</th>
                            <th>Name</th>
                            <th>DOB</th>
                            <th>Passport No</th>
                            <th>Mobile No</th>
                            <th></th>
                        </thead>
                        <tbody>
                            <?php
                            $txt = ''; $no = 1;
                            foreach ($data_guest as $value) {
                                $txt .= '<tr>';
                                $txt .= '   <td>'.$no.'</td>';
                                $txt .= '   <td>'.$value->gfname.' '.$value->glname.', '.$value->gtittle.'</td>';
                                $txt .= '   <td>'.$value->gdob.'</td>';
                                $txt .= '   <td>'.$value->gpassport.'</td>';
                                $txt .= '   <td>'.$value->gmobile.'</td>';
                                $txt .= '   <td><a class="btn btn-xs grey-cascade" onclick="editguest(\''.$value->glname.'|'.$value->gfname.'|'.$value->gtittle.'|'.$value->gdob.'|'.$value->gpassport.'|'.$value->id.'\')"><i class="fa fa-pencil"></i> Edit</a></td>';
                                $txt .= '</tr>';
                                $no++;
                            }
                            echo $txt;
                            ?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6 col-sm-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-info-circle"></i> <b>Action Menu</b> </div>
            </div>
            <div class="portlet-body">
                <?php 
                if($data_booking->bookingStatus==0){ 
                    $dt_kirim = $this->uri->segment(3).'|'.$data_booking->depdateID.'|'.$data_guest_total->TOTAL;
                ?>
                    <a class="btn btn-sm btn-info" onclick="set_confirm_dpnp('<?php echo $dt_kirim; ?>')">Confirmed - Deposit Not Paid</a>
                    <a class="btn btn-sm btn-danger" onclick="set_del('<?php echo $this->uri->segment(3); ?>')">Cancel Booking</a>
                <?php } ?>   

                <?php 
                if($data_booking->bookingStatus==1){ ?>
                    <a class="btn btn-sm btn-info" onclick="set_confirm(<?php echo $this->uri->segment(3); ?>,2)">Confirm Deposit & Change status to Deposit Received</a>
                    <a class="btn btn-sm btn-danger" onclick="set_del('<?php echo $this->uri->segment(3); ?>')">Cancel Booking</a>
                <?php } ?>      

                <?php if($data_booking->bookingStatus==2){ ?>
                    <a class="btn btn-sm btn-info" onclick="set_confirm(<?php echo $this->uri->segment(3); ?>,3)">Confirmed - Full Payment Not Paid</a>
                    <a class="btn btn-sm btn-danger" onclick="set_del('<?php echo $this->uri->segment(3); ?>')">Cancel Booking</a>
                <?php } ?>       

                <?php 
                if($data_booking->bookingStatus==3){ ?>
                    <a class="btn btn-sm btn-info" onclick="set_confirm(<?php echo $this->uri->segment(3); ?>,4)">Confirm Payment & Change status to Full Payment Received</a>
                    <a class="btn btn-sm btn-danger" onclick="set_del('<?php echo $this->uri->segment(3); ?>')">Cancel Booking</a>
                <?php } ?>

                <?php if($data_booking->bookingStatus==4){ ?>
                    <a class="btn btn-sm btn-info" onclick="set_confirm(<?php echo $this->uri->segment(3); ?>,5)">Completed</a>
                    <a class="btn btn-sm btn-danger" onclick="set_del('<?php echo $this->uri->segment(3); ?>')">Cancel Booking</a>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-info-circle"></i> <b>Additional File Download & Upload</b> </div>
            </div>
            <div class="portlet-body">
                <?php 
                if($data_booking->bookingStatus==1){
                    $exp = explode("|", $param_data);
                    if($exp[1]!=''){
                    ?>
                        <a class="btn btn-sm btn-primary" onclick="set_download(0)">Download Deposit Payment</a>
                    <?php }else{ ?>
                        <a class="btn btn-sm btn-primary" onclick="set_download(1)">Download Deposit Payment</a>
                    <?php } ?>
                <?php } ?> 

                <?php if($data_booking->bookingStatus==2){ ?>
                    <a class="btn btn-sm green-meadow" onclick="generate_invoice()">Generate Full / Final Payment Invoice</a>
                    <a class="btn btn-sm btn-warning" onclick="set_upload('<?php echo $param_data3; ?>')">Upload Full / Final Payment Invoice</a>
                <?php } ?>  

                <?php 
                if($data_booking->bookingStatus==3){
                    $exp = explode("|", $param_data2);
                    if($exp[1]!=''){
                    ?>
                        <a class="btn btn-sm btn-primary" onclick="set_download(2)">Download Full Payment Payment</a>
                    <?php }else{ ?>
                        <a class="btn btn-sm btn-primary" onclick="set_download(3)">Download Full Payment Payment</a>
                    <?php } ?>
                <?php } ?>

                <?php if($data_booking->bookingStatus==4){ ?>
                    <a class="btn btn-sm btn-warning" onclick="upload_file(<?php echo $this->uri->segment(3); ?>)">Upload & View File Archive (Tour Final Document)</a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
#gtittle,#gfname,#glname,#gdob,#gpassport {
    text-transform: uppercase;
}
</style>

<div class="modal fade" id="modal_detil" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="form_container">
        <form role="form" class="form-horizontal" id="form_master" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Edit Guest</h4>
            </div>
            <div class="modal-body"> 
                <input type="hidden" name="gid" id="gid">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="inputSuccess">Tittle</label>
                    <div class="col-md-9">
                        <select name="gtittle" class="form-control" id="gtittle">
                            <option value="MR">MR</option>
                            <option value="MRS">MRS</option>
                            <option value="MS">MS</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="inputSuccess">First Name</label>
                    <div class="col-md-9">
                        <input name="gfname" type="text" class="form-control" id="gfname">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="inputSuccess">Last Name</label>
                    <div class="col-md-9">
                        <input name="glname" type="text" class="form-control" id="glname">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="inputSuccess">DOB</label>
                    <div class="col-md-9">
                        <input name="gdob" type="text" class="form-control" id="gdob">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="inputSuccess">Passport No</label>
                    <div class="col-md-9">
                        <input name="gpassport" type="text" class="form-control" id="gpassport">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="save" type="button" class="btn blue" data-loading-text="Loading..."  >Save</button>
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- == EDIT TIME LIMIT ==-->
<div class="modal fade" id="modal_edit_timelimit" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="form_container_timelimit">
        <form role="form" class="form-horizontal" id="form_master_timelimit" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Edit Time Limit</h4>
            </div>
            <div class="modal-body"> 
                <input type="hidden" name="eid" id="eid" value="<?php echo $this->uri->segment(3); ?>">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="inputSuccess">Time Limit</label>
                    <div class="col-md-6">
                        <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" required="true" name="etimelimit" id="etimelimit" /> <small>Format. mm/dd/yyyy</small><!-- mm/dd/yyyy -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="save_timelimit" type="button" class="btn blue" data-loading-text="Loading..."  >Save</button>
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- == UPLOAD PAYMENT ==-->
<div class="modal fade" id="modal_upload" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Upload Full Payment Invoice</h4>
            </div>
        <form role="form" class="form-horizontal" id="form_master_upload" method="post" enctype="multipart/form-data">
            <div class="modal-body"> 
            <fieldset>
            <input type="hidden" name="bookingID" id="bookingID">
            <input type="hidden" name="typeID" id="typeID">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="inputSuccess">Remarks</label>
                    <div class="col-md-6">
                        <input name="note" type="text" class="form-control" id="note">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="inputSuccess">File Invoice <span class="required">*</span></label>
                    <div class="col-md-6">
                        <input name="userfile" type="file" id="userfile"> <span id="fileDownload"></span>
                    </div>
                </div>
            </fieldset>
            </div>
        </form>
            <div class="modal-footer">
                <button type="submit" class="btn dark btn-outline" id="btnSave" onClick="uploadFile2()" >Save</button>
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script type="text/javascript">
    //UPLOAD
    function uploadFile2(){
        if (($("#userfile"))[0].files.length > 0) {
            var $data = $('#form_master_upload').serialize();
            var file = $("#userfile")[0].files[0];
            var formdata = new FormData();
            formdata.append("userfile", file);
            formdata.append("data", $data);
            var ajax = new XMLHttpRequest();
            ajax.upload.addEventListener("progress", progressHandler2, false);
            ajax.addEventListener("load", completeHandler2, false);
            ajax.addEventListener("error", errorHandler2, false);
            ajax.addEventListener("abort", abortHandler2, false);
            ajax.open("POST", "<?php echo base_url()?>booking_all/upload/");
            ajax.send(formdata);
        } else {
            alert("No file chosen!");
        }
    }
    function progressHandler2(event){
        var percent = (event.loaded / event.total) * 100;
        //console.log(percent);
    }
    function completeHandler2(event){
        if(event.target.responseText!=''){
            $('#userfile').val('');
            swal("Error!", event.target.responseText, "error");
        }else{
            $('#form_master_upload').trigger("reset");
            $('#modal_upload').modal('hide');
            swal("Success!", "Your file has been uploaded.", "success");
            window.location = "<?php echo base_url().'booking_all/detils/'.$this->uri->segment(3); ?>";
        }   
    }
    function errorHandler2(event){
        swal("Error!", "Upload Failed!", "error");
    }
    function abortHandler2(event){
        swal("Error!", "Upload Aborted!", "error");
    }
</script>

<!-- == DOWNLOAD DEPOSIT ==-->
<div class="modal fade" id="modal_detil_download" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">View Payment Slip</h4>
            </div>
            <div class="modal-body"> 
            <fieldset>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="inputSuccess">Note from agent</label>
                    <div class="col-md-6">
                        <b><span id="ket_note"></span></b>
                    </div>
                </div>
                <br><p>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="inputSuccess">Transfer Date</label>
                    <div class="col-md-6">
                        <b><span id="ket_transferDate"></span></b>
                    </div>
                </div>
                <br><p>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="inputSuccess">Payment Slip</label>
                    <div class="col-md-6">
                        <b><span id="ket_fileDownload"></span></b>
                    </div>
                </div>
            </fieldset>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- == GENERATE FULL PAYMENT INVOICE 22/11 ==-->
<div class="modal fade" id="modal_generate" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Form generate invoice full payment</h4>
            </div>
            <div class="modal-body"> 
                <table class="table table-bordered table-hover">
                    <thead>
                        <th>NAME</th>
                        <th>CAT</th>
                        <th>TOUR PRICE</th>
                    </thead>
                    <tbody>
                    <?php
                    $txt = '';
                    foreach ($data_guest as $value) {
                        if($value->gtype==0) $cat = 'SGL';
                        if($value->gtype==1) $cat = 'ADL';
                        if($value->gtype==2) $cat = 'CWA';
                        if($value->gtype==3) $cat = 'CWB';
                        if($value->gtype==4) $cat = 'CNB';

                        $txt .= '<tr>';
                        $txt .= '   <td><font color="blue">'.$value->glname.' '.$value->gfname.', '.$value->gtittle.'</font></td>';
                        $txt .= '   <td><font color="blue">'.$cat.'</font></td>';
                        $txt .= '   <td><input type="text" class="tourPrice form-control input-medium" name="tourPrice[]" id="tourPrice_'.$value->id.'" value="0"></td>';
                        $txt .= '</tr>';
                        $no++;
                    }
                    echo $txt;
                    ?>
                        <tr>
                            <td colspan="2">TOTAL TOUR PRICE</td>
                            <td><input type="text" class="form-control input-medium" name="totTourPrice" id="totTourPrice" readonly></td>
                        </tr>
                        <tr>
                            <td colspan="2">DEPOSIT PAID (-)</td>
                            <td><input type="text" class="form-control input-medium" name="totDP" id="totDP" value="<?php echo $data_guest_total->TOTAL*$data_depdate->dpAmount; ?>"></td>
                        </tr>
                        <tr>
                            <td colspan="2"><input type="text" class="form-control input-medium" name="addCharge" id="addCharge" placeholder="Additional Change Description 1"></td>
                            <td><input type="text" class="form-control input-medium" name="addChargePrice" id="addChargePrice" placeholder="Price" value="0"></td>
                        </tr>
                        <tr>
                            <td colspan="2"><input type="text" class="form-control input-medium" name="addCharge2" id="addCharge2" placeholder="Additional Change Description 2"></td>
                            <td><input type="text" class="form-control input-medium" name="addChargePrice2" id="addChargePrice2" placeholder="Price" value="0"></td>
                        </tr>
                        <tr>
                            <td colspan="2"><b>GRAND TOTAL</b><br><small>Total tour price + additional - deposit paid</small></td>
                            <td><input type="text" class="form-control input-medium" name="grandTotal" value="0" id="grandTotal" readonly></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" onClick="generateFile()" >Generate</button>
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- == EVENT HARGA GENERATE INVOICE == -->
<script type="text/javascript">
$(document).ready(function() {
    calculateSum();
    $(".tourPrice").on("keydown keyup", function() {
        calculateSum();
    });

    $("#addChargePrice,#addChargePrice2,#totDP").on("keydown keyup", function() {
        calculateSum();
    }); 

    function calculateSum() {
        var sum = 0;
        //iterate through each textboxes and add the values
        $(".tourPrice").each(function() {
            //add only if the value is number
            if (!isNaN(this.value) && this.value.length != 0) {
                sum += parseFloat(this.value);
                $(this).css("background-color", "white");
            }else if (this.value.length != 0){
                $(this).css("background-color", "red");
            }
        });
     
        $("input#totTourPrice").val(sum);
        calculateTotal(sum);
    } 

    function calculateTotal(data){
        var additional  = parseInt($('#addChargePrice').val());
        var additional2 = parseInt($('#addChargePrice2').val());
        var deposit     = parseInt($('#totDP').val());

        var total = data+additional+additional2-deposit;
        $("input#grandTotal").val(total);
    }
});  

function generateFile(){
    var bookingID       = <?php echo $this->uri->segment(3); ?>;

    var tourPrice = '';
    <?php
    foreach ($data_guest as $value) {
    ?>
        tourPrice += <?php echo $value->id; ?>+'*'+$('#tourPrice_<?php echo $value->id; ?>').val()+'|';
    <?php
    }
    ?>

    var totTourPrice    = $('#totTourPrice').val();
    var totDP           = $('#totDP').val();
    var addCharge       = $('#addCharge').val();
    var addChargePrice  = $('#addChargePrice').val();
    var addCharge2      = $('#addCharge2').val();
    var addChargePrice2 = $('#addChargePrice2').val();
    var grandTotal      = $('#grandTotal').val();

    $.ajax({
        type: 'POST',
        url: '<?php echo base_url() ?>booking/generate',
        data: { 'bookingID' : bookingID, 'tourPrice' : tourPrice, 'totTourPrice' : totTourPrice, 'totDP' : totDP, 'addCharge' : addCharge, 'addChargePrice' : addChargePrice, 'addCharge2' : addCharge2, 'addChargePrice2' : addChargePrice2, 'grandTotal' : grandTotal },
        success: function(data) {
            if(data!='false'){
                window.open("<?php echo base_url().'download/generate/'; ?>"+data, '_blank');
            }else{
                swal("Error!", "Something error with our system.", "error");
            }
        }
    });
} 
</script>

<div class="modal fade" id="modal_files" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" id="form_container">
        <form role="form" class="form-horizontal" id="form_master" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Upload Finall File</h4>
            </div>
            <div class="modal-body"> 
                <input type="hidden" name="file_bookingID" id="file_bookingID">
                <table class="table table-bordered table-hover">
                <?php print_r($data_final_files); ?>
                    <thead>
                        <th>NO</th>
                        <th>DESCRIPTION</th>
                        <th>FILE</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td><input type="text" name="uploadNote1" id="uploadNote1" class="form-control input-medium" value="<?php if(!empty($data_final_files)) { echo $data_final_files->uploadNote1; } else { echo ''; } ?>"></td>
                            <td><input type="file" name="uploadFile1" id="uploadFile1"></td>
                            <td><span id="ketfile1"></span><input type="hidden" name="fileUploaded1" id="fileUploaded1" value="<?php if(!empty($data_final_files)) { echo $data_final_files->uploadFile1; } else { echo ''; } ?>"></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td><input type="text" name="uploadNote2" id="uploadNote2" class="form-control input-medium" value="<?php if(!empty($data_final_files)) { echo $data_final_files->uploadNote2; } else { echo ''; } ?>"></td>
                            <td><input type="file" name="uploadFile2" id="uploadFile2"></td>
                            <td><span id="ketfile2"></span><input type="hidden" name="fileUploaded2" id="fileUploaded2" value="<?php if(!empty($data_final_files)) { echo $data_final_files->uploadFile2; } else { echo ''; } ?>"></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td><input type="text" name="uploadNote3" id="uploadNote3" class="form-control input-medium" value="<?php if(!empty($data_final_files)) { echo $data_final_files->uploadNote3; } else { echo ''; } ?>"></td>
                            <td><input type="file" name="uploadFile3" id="uploadFile3"></td>
                            <td><span id="ketfile3"></span><input type="hidden" name="fileUploaded3" id="fileUploaded3" value="<?php if(!empty($data_final_files)) { echo $data_final_files->uploadFile3; } else { echo ''; } ?>"></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td><input type="text" name="uploadNote4" id="uploadNote4" class="form-control input-medium" value="<?php if(!empty($data_final_files)) { echo $data_final_files->uploadNote4; } else { echo ''; } ?>"></td>
                            <td><input type="file" name="uploadFile4" id="uploadFile4"></td>
                            <td><span id="ketfile4"></span><input type="hidden" name="fileUploaded4" id="fileUploaded4" value="<?php if(!empty($data_final_files)) { echo $data_final_files->uploadFile4; } else { echo ''; } ?>"></td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td><input type="text" name="uploadNote5" id="uploadNote5" class="form-control input-medium" value="<?php if(!empty($data_final_files)) { echo $data_final_files->uploadNote5; } else { echo ''; } ?>"></td>
                            <td><input type="file" name="uploadFile5" id="uploadFile5"></td>
                            <td><span id="ketfile5"></span><input type="hidden" name="fileUploaded5" id="fileUploaded5" value="<?php if(!empty($data_final_files)) { echo $data_final_files->uploadFile5; } else { echo ''; } ?>"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button id="save_file" type="button" class="btn blue" data-loading-text="Loading..."  >Save</button>
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script type="text/javascript">
    /*=== Upload file otomatis saat pilih file ===*/
    $(document).ready(function() {
        $("#uploadFile1").change(function () {
            prosesuploadFile1();
        });
        $("#uploadFile2").change(function () {
            prosesuploadFile2();
        });
        $("#uploadFile3").change(function () {
            prosesuploadFile3();
        });
        $("#uploadFile4").change(function () {
            prosesuploadFile4();
        });
        $("#uploadFile5").change(function () {
            prosesuploadFile5();
        });

        $("#save_file").click(function(){
            save_file();
        });

        if($('#fileUploaded1').val()!=''){
            $('#ketfile1').html('<a class="btn green btn-xs" href="<?php echo base_url()?>archive_files/'+$('#fileUploaded1').val()+'" target="_blank"><i class="fa fa-search"></i> View File</a> <a class="btn red btn-xs" onclick="deletefile1(1)"><i class="fa fa-trash"></i> Delete</a>');
        }
        if($('#fileUploaded2').val()!=''){
            $('#ketfile2').html('<a class="btn green btn-xs" href="<?php echo base_url()?>archive_files/'+$('#fileUploaded2').val()+'" target="_blank"><i class="fa fa-search"></i> View File</a> <a class="btn red btn-xs" onclick="deletefile1(1)"><i class="fa fa-trash"></i> Delete</a>');
        }
        if($('#fileUploaded3').val()!=''){
            $('#ketfile3').html('<a class="btn green btn-xs" href="<?php echo base_url()?>archive_files/'+$('#fileUploaded3').val()+'" target="_blank"><i class="fa fa-search"></i> View File</a> <a class="btn red btn-xs" onclick="deletefile1(1)"><i class="fa fa-trash"></i> Delete</a>');
        }
        if($('#fileUploaded4').val()!=''){
            $('#ketfile4').html('<a class="btn green btn-xs" href="<?php echo base_url()?>archive_files/'+$('#fileUploaded4').val()+'" target="_blank"><i class="fa fa-search"></i> View File</a> <a class="btn red btn-xs" onclick="deletefile1(1)"><i class="fa fa-trash"></i> Delete</a>');
        }
        if($('#fileUploaded5').val()!=''){
            $('#ketfile5').html('<a class="btn green btn-xs" href="<?php echo base_url()?>archive_files/'+$('#fileUploaded5').val()+'" target="_blank"><i class="fa fa-search"></i> View File</a> <a class="btn red btn-xs" onclick="deletefile1(1)"><i class="fa fa-trash"></i> Delete</a>');
        }

    });

/*== 1 ==*/
    function prosesuploadFile1(){
        if ($("#uploadFile1")[0].files.length > 0) {
            var file = $("#uploadFile1")[0].files[0];
            var formdata = new FormData();
            formdata.append("uploadFile1", file);
            var ajax = new XMLHttpRequest();
            //ajax.upload.addEventListener("progress", progressHandler1, false);
            ajax.addEventListener("load", completeHandler1, false);
            ajax.addEventListener("error", errorHandler1, false);
            ajax.addEventListener("abort", abortHandler1, false);
            ajax.open("POST", "<?php echo base_url()?>utils/uploadfile1/");
            ajax.send(formdata);
        } else {
            alert("No file chosen!");
        }
    }
    function completeHandler1(event){
        var data = event.target.responseText.split('*');
        if(data[0]!=''){
            alert(data[1]);
        }else{
            $('#fileUploaded1').val(data[1]);
            $('#uploadFile1').val('');
            $('#ketfile1').html('<a class="btn green btn-xs" href="<?php echo base_url()?>archive_files/'+data[1]+'" target="_blank"><i class="fa fa-search"></i> View File</a> <a class="btn red btn-xs" onclick="deletefile1(1)"><i class="fa fa-trash"></i> Delete</a>');
        }   
    }

/*== 2 ==*/
    function prosesuploadFile2(){
        if ($("#uploadFile2")[0].files.length > 0) {
            var file = $("#uploadFile2")[0].files[0];
            var formdata = new FormData();
            formdata.append("uploadFile2", file);
            var ajax = new XMLHttpRequest();
            //ajax.upload.addEventListener("progress", progressHandler1, false);
            ajax.addEventListener("load", completeHandler2, false);
            ajax.addEventListener("error", errorHandler1, false);
            ajax.addEventListener("abort", abortHandler1, false);
            ajax.open("POST", "<?php echo base_url()?>utils/uploadfile2/");
            ajax.send(formdata);
        } else {
            alert("No file chosen!");
        }
    }
    function completeHandler2(event){
        var data = event.target.responseText.split('*');
        if(data[0]!=''){
            alert(data[1]);
        }else{
            $('#fileUploaded2').val(data[1]);
            $('#uploadFile2').val('');
            $('#ketfile2').html('<a class="btn green btn-xs" href="<?php echo base_url()?>archive_files/'+data[1]+'" target="_blank"><i class="fa fa-search"></i> View File</a> <a class="btn red btn-xs" onclick="deletefile1(2)"><i class="fa fa-trash"></i> Delete</a>');
        }   
    }

/*== 3 ==*/
    function prosesuploadFile3(){
        if ($("#uploadFile3")[0].files.length > 0) {
            var file = $("#uploadFile3")[0].files[0];
            var formdata = new FormData();
            formdata.append("uploadFile3", file);
            var ajax = new XMLHttpRequest();
            //ajax.upload.addEventListener("progress", progressHandler1, false);
            ajax.addEventListener("load", completeHandler3, false);
            ajax.addEventListener("error", errorHandler1, false);
            ajax.addEventListener("abort", abortHandler1, false);
            ajax.open("POST", "<?php echo base_url()?>utils/uploadfile3/");
            ajax.send(formdata);
        } else {
            alert("No file chosen!");
        }
    }
    function completeHandler3(event){
        var data = event.target.responseText.split('*');
        if(data[0]!=''){
            alert(data[1]);
        }else{
            $('#fileUploaded3').val(data[1]);
            $('#uploadFile3').val('');
            $('#ketfile3').html('<a class="btn green btn-xs" href="<?php echo base_url()?>archive_files/'+data[1]+'" target="_blank"><i class="fa fa-search"></i> View File</a> <a class="btn red btn-xs" onclick="deletefile1(3)"><i class="fa fa-trash"></i> Delete</a>');
        }   
    }

/*== 4 ==*/
    function prosesuploadFile4(){
        if ($("#uploadFile4")[0].files.length > 0) {
            var file = $("#uploadFile4")[0].files[0];
            var formdata = new FormData();
            formdata.append("uploadFile4", file);
            var ajax = new XMLHttpRequest();
            //ajax.upload.addEventListener("progress", progressHandler1, false);
            ajax.addEventListener("load", completeHandler4, false);
            ajax.addEventListener("error", errorHandler1, false);
            ajax.addEventListener("abort", abortHandler1, false);
            ajax.open("POST", "<?php echo base_url()?>utils/uploadfile4/");
            ajax.send(formdata);
        } else {
            alert("No file chosen!");
        }
    }
    function completeHandler4(event){
        var data = event.target.responseText.split('*');
        if(data[0]!=''){
            alert(data[1]);
        }else{
            $('#fileUploaded4').val(data[1]);
            $('#uploadFile4').val('');
            $('#ketfile4').html('<a class="btn green btn-xs" href="<?php echo base_url()?>archive_files/'+data[1]+'" target="_blank"><i class="fa fa-search"></i> View File</a> <a class="btn red btn-xs" onclick="deletefile1(4)"><i class="fa fa-trash"></i> Delete</a>');
        }   
    }

/*== 5 ==*/
    function prosesuploadFile5(){
        if ($("#uploadFile5")[0].files.length > 0) {
            var file = $("#uploadFile5")[0].files[0];
            var formdata = new FormData();
            formdata.append("uploadFile5", file);
            var ajax = new XMLHttpRequest();
            //ajax.upload.addEventListener("progress", progressHandler1, false);
            ajax.addEventListener("load", completeHandler5, false);
            ajax.addEventListener("error", errorHandler1, false);
            ajax.addEventListener("abort", abortHandler1, false);
            ajax.open("POST", "<?php echo base_url()?>utils/uploadfile5/");
            ajax.send(formdata);
        } else {
            alert("No file chosen!");
        }
    }
    function completeHandler5(event){
        var data = event.target.responseText.split('*');
        if(data[0]!=''){
            alert(data[1]);
        }else{
            $('#fileUploaded5').val(data[1]);
            $('#uploadFile5').val('');
            $('#ketfile5').html('<a class="btn green btn-xs" href="<?php echo base_url()?>archive_files/'+data[1]+'" target="_blank"><i class="fa fa-search"></i> View File</a> <a class="btn red btn-xs" onclick="deletefile1(5)"><i class="fa fa-trash"></i> Delete</a>');
        }   
    }

    function deletefile1(x){
        if(confirm("Are you sure to Delete this file?")){
            /*== Hapus file dari folder 10/11 (Not working :"(()==*/
             $.ajax({
                url: '<?php echo base_url()?>utils/deletefile/',
                type: "POST",
                data:{'filename':$('#fileUploaded'+x).val()},
                success: function(data){
                    //console.log(data);
                }
            });

            $('#fileUploaded'+x).val('');
            $('#uploadFile'+x).val('');
            $('#ketfile'+x).html('');
            alert('Deleted successfully.');
        }
    }
    function errorHandler1(event){
        alert("Upload Failed");
    }
    function abortHandler1(event){
        alert("Upload Aborted");
    }

    function save_file(){
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url()?>utils/savefile',
            data: {'bookingID':$('#file_bookingID').val(),'uploadNote1':$('#uploadNote1').val(),'uploadFile1':$('#fileUploaded1').val(), 'uploadNote2':$('#uploadNote2').val(),'uploadFile2':$('#fileUploaded2').val(), 'uploadNote3':$('#uploadNote3').val(),'uploadFile3':$('#fileUploaded3').val(), 'uploadNote4':$('#uploadNote4').val(),'uploadFile4':$('#fileUploaded4').val(), 'uploadNote5':$('#uploadNote5').val(),'uploadFile5':$('#fileUploaded5').val()},
            beforeSend: function(){
                $('#save').button('loading');
            },
            complete: function() {
                $('#save').button('reset');
            },
            success: function(data) {
                if(data=='true'){
                    $('#modal_files').modal('hide');
                    window.location = "<?php echo base_url().'booking_all/detils/'.$this->uri->segment(3); ?>";
                }                       
            }
        });
    }
</script>

<!--== GENERAL ==-->
<script type="text/javascript">
$(document).ready(function() {
    /* == EDIT GUEST == */
    $('#save').click(function(){
        $('#form_master').submit(); 
    });

    $('#form_container').find('form').validate({
        errorClass: 'help-block',
        errorElement: 'span',
        ignore: 'input[type=hidden]',
        highlight: function(el, errorClass) {
            $(el).parents('.form-group').first().addClass('has-error');
        },
        unhighlight: function(el, errorClass) {
            var $parent = $(el).parents('.form-group').first();
            $parent.removeClass('has-error');
            $parent.find('.help-block').hide();
        },
        errorPlacement: function(error, el) {
            error.appendTo(el.parents('.form-group').find('div:first'));
        },
        submitHandler: function(form) {
            var $theForm = $(form);
            var $data = $theForm.serialize(); 
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url()?>booking_all/editguest',
                data: $data,
                beforeSend: function(){
                    $('#save').button('loading');
                },
                complete: function() {
                    $('#save').button('reset');
                },
                success: function(data) {
                    if(data=='true'){
                        $('#modal_detil').modal('hide');
                        swal("Success!", "Guest data has been successfully updated.", "success");
                        window.location = "<?php echo base_url().'booking_all/detils/'.$this->uri->segment(3); ?>";
                    }                       
                }
            });
            return false;
        }
    }); 

    /* == EDIT TIMELIMIT ==*/
    $('#save_timelimit').click(function(){
        $('#form_master_timelimit').submit(); 
    });

    $('#form_container_timelimit').find('form').validate({
        errorClass: 'help-block',
        errorElement: 'span',
        ignore: 'input[type=hidden]',
        highlight: function(el, errorClass) {
            $(el).parents('.form-group').first().addClass('has-error');
        },
        unhighlight: function(el, errorClass) {
            var $parent = $(el).parents('.form-group').first();
            $parent.removeClass('has-error');
            $parent.find('.help-block').hide();
        },
        errorPlacement: function(error, el) {
            error.appendTo(el.parents('.form-group').find('div:first'));
        },
        submitHandler: function(form) {
            var $theForm = $(form);
            var $data = $theForm.serialize(); 
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url()?>booking_all/edittimelimit',
                data: $data,
                beforeSend: function(){
                    $('#save_timelimit').button('loading');
                },
                complete: function() {
                    $('#save_timelimit').button('reset');
                },
                success: function(data) {
                    if(data=='true'){
                        $('#modal_edit_timelimit').modal('hide');
                        swal("Success!", "Time Limit has been successfully updated.", "success");
                        window.location = "<?php echo base_url().'booking_all/detils/'.$this->uri->segment(3); ?>";
                    }                       
                }
            });
            return false;
        }
    }); 

});

function editguest(data){
    var dt = data.split("|");
    //$('#gname').html(dt[0]+' '+dt[1]+', '+dt[2]); //last first tittle
    $('#glname').val(dt[0]);
    $('#gfname').val(dt[1]);
    $('#gtittle').val(dt[2]);
    $('#gdob').val(dt[3]);
    $('#gpassport').val(dt[4]);
    $('#gid').val(dt[5]);
    $('#modal_detil').modal('show');
}

/*== EDIT TIME LIMIT 21/11 ==*/
function edittimelimit(data){
    $('#etimelimit').val(data);
    $('#modal_edit_timelimit').modal('show');
}

function set_upload(data){
    var isi=data.split('|');
    $('#typeID').val('0');
    $('#bookingID').val(isi[0]);
    if(isi[1]!=''){
        $('#fileDownload').html('<p><small style="color:red;">Your Invoice has been uploaded. Do you want to re-write it? or view details <a href="<?php echo base_url('uploads/') ?>/'+isi[1]+'" target="_blank">here</a>.</small></p><p><button type="button" class="btn red btn-outline" id="btnSave" onClick="chengestatus()" >Re Confirm Deposit Payment</button></p>');
    }
    $('#modal_upload').modal('show');
}

function set_del(data){
    swal({
        title: "Are you sure to cancel this reservation?",
        text: "Your reservation will be deleted permanently.",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: 'btn-danger',
        confirmButtonText: 'Yes',
        cancelButtonText: "Cancel",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url() ?>booking/cancel',
                data: { id : data },
                success: function(data) {
                    if(data=='true'){
                        swal("Deleted!", "Your reservation has been deleted!", "success");                      
                        window.location = "<?php echo base_url().'booking_all/detils/'.$this->uri->segment(3); ?>";
                    }
                }
            });
        } else {
          swal("Cancelled", "Your data is safe :)", "error");
        }
    });
}

/*== Change status to confirm 17/11 ==*/
function set_confirm(id,data){
    if(data==1){
        var pertanyaan = 'Are you sure you want to change status to DEPOSIT NOT PAID?';
    }else if(data==2){
        var pertanyaan = 'Are you sure you want to change status to DEPOSIT RECEIVED?';
    }else if(data==3){
        var pertanyaan = 'Are you sure you want to change status to FULL PAYMENT NOT PAID?';
    }else if(data==4){
        var pertanyaan = 'Are you sure you want to change status to FULL PAYMENT RECEIVED?';
    }else if(data==5){
        var pertanyaan = 'Are you sure you want to change status to COMPLETED?';
    }else if(data==6){
        var pertanyaan = 'Are you sure you want to change status to CANCEL?';
    }

    swal({
        title: pertanyaan,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: 'btn-danger',
        confirmButtonText: 'Yes',
        cancelButtonText: "Cancel",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url() ?>booking/chengestatus',
                data: { 'id' : id, 'newstatus' : data },
                success: function(data) {
                    if(data=='true'){
                        swal("Success!", "Your reservation status has been changed!", "success");
                        window.location = "<?php echo base_url().'booking_all/detils/'.$this->uri->segment(3); ?>";
                    }
                }
            });
        } else {
          swal("Cancelled", "Your data is safe :)", "error");
        }
    });
}

/*== Change status to DP not PAID 21/11 ==*/
function set_confirm_dpnp(data){
    swal({
        title: "Are you sure to confirm this booking?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: 'btn-danger',
        confirmButtonText: 'Yes',
        cancelButtonText: "Cancel",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function (isConfirm) {
        if (isConfirm) {
            var pisah = data.split('|');
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url() ?>booking/chengestatustodpnp',
                data: { 'id' : pisah[0], 'depdateID' : pisah[1], 'total_guest' : pisah[2] },
                success: function(data) {
                    if(data=='true'){
                        swal("Success!", "Your reservation status has been changed!", "success");
                        window.location = "<?php echo base_url().'booking_all/detils/'.$this->uri->segment(3); ?>";
                    }else if(data=='notavailable'){
                        swal("Oops!", "Your requested Allotment is not available at the moment. Please add your seat allotment at Menu Product.", "warning");
                    }else{
                        swal("Error!", "Something error with out system.", "error");
                    }
                }
            });
        } else {
          swal("Cancelled", "Your data is safe :)", "error");
        }
    });
}

/*== Download payment deposit 17/11 ==*/
function set_download(data){
    if(data==0){
        //Link available
        var isi     = '<?php echo $param_data; ?>';
        var pisah   = isi.split('|');
        $('#ket_note').html(pisah[3]);
        $('#ket_transferDate').html(pisah[2]);
        if(pisah[1]!=''){
            $('#ket_fileDownload').html('<a href="http://kuoni.wuisan.com/booking/uploads/'+pisah[1]+'" target="_blank"><i class="fa fa-download"></i> Download</a>');
        }
        $('#modal_detil_download').modal('show');
    }else if(data==1){
        //Link not available
        swal("Error!", "File not available.", "error");
    }else if(data==2){
        //Link available
        var isi     = '<?php echo $param_data2; ?>';
        var pisah   = isi.split('|');
        $('#ket_note').html(pisah[3]);
        $('#ket_transferDate').html(pisah[2]);
        if(pisah[1]!=''){
            $('#ket_fileDownload').html('<a href="http://kuoni.wuisan.com/booking/uploads/'+pisah[1]+'" target="_blank"><i class="fa fa-download"></i> Download</a>');
        }
        $('#modal_detil_download').modal('show');
    }else if(data==3){
        //Link not available
        swal("Error!", "File not available.", "error");
    }
}

/*== Generate full payment invoice 22/11 ==*/
function generate_invoice(){
    $('#modal_generate').modal('show');
}

/*== Upload file archive 23/11 ==*/
function upload_file(data){
    $('#file_bookingID').val(data);
    $('#modal_files').modal('show');
}
</script>

<?php include "include_footer.php"; ?>