<?php 
    $url = $this->uri->segment(1);
?>
<ul class="nav navbar-nav">
    <!--li class="menu-dropdown classic-menu-dropdown <?php if($url=='dashboard') echo "active"; ?>">
        <a href="<?php echo base_url('dashboard/') ?>"> Dashboard
            <span class="arrow"></span>
        </a>
    </li-->
    <!--li class="menu-dropdown mega-menu-dropdown <?php if(($url=='booking_waitinglist')||($url=='booking_dpnotpaid')||($url=='booking_dpreceived')||($url=='booking_fullnotpaid')||($url=='booking_fullreceived')||($url=='booking_completed')||($url=='booking_cancel')||($url=='booking_all')) echo "active"; ?>">
        <a href="javascript:;"> Booking List
            <span class="arrow"></span>
        </a>
        <ul class="dropdown-menu" style="min-width: 300px">
            <li>
                <div class="mega-menu-content">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="mega-menu-submenu">
                                <li>
                                    <a href="<?php echo base_url('booking_all/') ?>"> All Status </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('booking_waitinglist/') ?>"> Waiting List </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('booking_dpnotpaid/') ?>"> Confirmed (Deposit Not Paid) </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('booking_dpreceived/') ?>"> Confirmed (Deposit Received) </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('booking_fullnotpaid/') ?>"> Confirmed (Full Payment Not Paid) </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('booking_fullreceived/') ?>"> Confirmed (Full Payment Received) </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('booking_completed/') ?>"> Completed </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('booking_cancel/') ?>"> Cancel </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </li-->
    <!--li class="menu-dropdown classic-menu-dropdown <?php if(($url=='seat')||($url=='seatbyagent')||($url=='roomlist')) echo "active"; ?>">
        <a href="javascript:;"> Seat Status
            <span class="arrow"></span>
        </a>
        <ul class="dropdown-menu pull-left">
            <li class=" ">
                <a href="<?php echo base_url('seat/') ?>" class="nav-link"> By Product </a>
            </li>
            <li class=" ">
                <a href="<?php echo base_url('seatbyagent/') ?>" class="nav-link"> By Agent </a>
            </li>
            <li class=" ">
                <a href="<?php echo base_url('roomlist/') ?>" class="nav-link"> Room List </a>
            </li>
        </ul>
    </li-->
    <!--li class="menu-dropdown classic-menu-dropdown <?php if(($url=='product')||($url=='product_add')) echo "active"; ?>">
        <a href="javascript:;"> Product
            <span class="arrow"></span>
        </a>
        <ul class="dropdown-menu pull-left">
            <li class=" ">
                <a href="<?php echo base_url('product/') ?>" class="nav-link"> Tour </a>
            </li>
        </ul>
    </li-->
    <li class="menu-dropdown mega-menu-dropdown  <?php if(($url=='agent')||($url=='user')||($url=='airline')||($url=='departure')||($url=='bank')||($url=='category')||($url=='room')||($url=='term')) echo "active"; ?>">
        <a href="javascript:;"> Master Data
            <span class="arrow"></span>
        </a>
        <ul class="dropdown-menu" style="min-width: 600px">
            <li>
                <div class="mega-menu-content">
                    <div class="row">
                        <div class="col-md-4">
                            <ul class="mega-menu-submenu">
                                <li>
                                    <h3><i><b>User</b></i></h3>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('agent/') ?>"> Agent </a>
                                </li>
                                <!--li>
                                    <a href="<?php echo base_url('user/') ?>"> Administrator </a>
                                </li-->
                            </ul>
                        </div>
                        <!--div class="col-md-4">
                            <ul class="mega-menu-submenu">
                                <li>
                                    <h3><i><b>Product</b></i></h3>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('room/') ?>"> Room Setting </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('airline/') ?>"> Airline </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('departure/') ?>"> Departure City </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('category/') ?>"> Category </a>
                                </li>
                            </ul>
                        </div-->
                        <div class="col-md-4">
                            <ul class="mega-menu-submenu">
                                <li>
                                    <h3><i><b>Others</b></i></h3>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('bank/') ?>"> Bank </a>
                                </li>
                                <!--li>
                                    <a href="<?php echo base_url('term/') ?>"> Term & Condition </a>
                                </li-->
                            </ul>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </li>
</ul>