<?php include "include_header.php"; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<script type="text/javascript" src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>

<div class="row">
	<div class="col-md-12">
	  <div class="portlet box green">
		<div class="portlet-title">
			<div class="caption">
				<small> Master Data <i class="fa fa-angle-right"></i> User <i class="fa fa-angle-right"></i> Agent </small>
			</div>
			<div class="actions">
				 <div class="btn-group btn-group-solid">
				<a class="btn default" href="javascript:;" id="form_act"><i class="fa fa-plus-square"></i> Add New Data</a>
				</div>
			</div>
		</div>
		<div class="portlet-body form" id="form_container" style="display:none;">
			<form role="form" class="form-horizontal" id="form_master" method="post">
			<input type="hidden" name="act" id="act" value="add"/>
			<input type="hidden" name="id" id="id" value=""/>
				<div class="form-body">
				<div id="alert"></div>
					<fieldset>
						<legend>A. GENERAL INFORMATION</legend>
						<div class="form-group">
							<label class="col-md-3 control-label" for="inputSuccess">Agent ID <span class="required">*</span></label>
							<div class="col-md-6">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="User ID (required)" data-container="body"></i>
									<input name="userID" type="text" maxlength="20" required="true" class="form-control" id="userID" onkeyup="this.value = this.value.replace(/[^A-Za-z\.]/g,'');">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="inputSuccess">Agent Name <span class="required">*</span></label>
							<div class="col-md-6">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="Agent Name (required)" data-container="body"></i>
									<input name="agentName" type="text" required="true" class="form-control" id="agentName">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="inputSuccess">Company Name <span class="required">*</span></label>
							<div class="col-md-6">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="Company Name (required)" data-container="body"></i>
									<input name="companyName" type="text" required="true" class="form-control" id="companyName">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="inputSuccess">Address</label>
							<div class="col-md-6">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="Address" data-container="body"></i>
									<input name="address" type="text" class="form-control" id="address">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="inputSuccess">Zip Code</label>
							<div class="col-md-3">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="Zip Code" data-container="body"></i>
									<input name="zipcode" type="text" class="form-control" id="zipcode">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="inputSuccess">City / Country</label>
							<div class="col-md-3">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="City" data-container="body"></i>
									<input name="city" type="text" class="form-control" id="city">
								</div>
							</div>
							<div class="col-md-3">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="Country" data-container="body"></i>
									<input name="country" type="text" class="form-control" id="country">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="inputSuccess">Phone / Fax</label>
							<div class="col-md-3">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="Phone Number" data-container="body"></i>
									<input name="phone" type="text" class="form-control" id="phone">
								</div>
							</div>
							<div class="col-md-3">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="Fax Number" data-container="body"></i>
									<input name="fax" type="text" class="form-control" id="fax">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="inputSuccess">Email</label>
							<div class="col-md-6">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="Email (required)" data-container="body"></i>
									<input name="email" type="email" class="form-control" id="email">
								</div>
							</div>
						</div>
					</fieldset>
					<fieldset>
						<legend>B. CONTACT PERSON</legend>
						<div class="form-group">
							<label class="col-md-3 control-label" for="inputSuccess">Contact Person Name <span class="required">*</span></label>
							<div class="col-md-6">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="Contact Person Name (required)" data-container="body"></i>
									<input name="contactName" type="text" required="true" class="form-control" id="contactName">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="inputSuccess">Position</label>
							<div class="col-md-6">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="Position" data-container="body"></i>
									<input name="contactPosition" type="text" class="form-control" id="contactPosition">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="inputSuccess">Email</label>
							<div class="col-md-6">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="Contact Person Email (required)" data-container="body"></i>
									<input name="contactEmail" type="email" class="form-control" id="contactEmail">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="inputSuccess">Mobile</label>
							<div class="col-md-6">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="Mobile" data-container="body"></i>
									<input name="contactMobile" type="text" class="form-control" id="contactMobile">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="inputSuccess">Whats App Number</label>
							<div class="col-md-6">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="Whats App Number" data-container="body"></i>
									<input name="contactWA" type="text" class="form-control" id="contactWA">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="inputSuccess">BBM</label>
							<div class="col-md-6">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="BBM" data-container="body"></i>
									<input name="contactBBM" type="text" class="form-control" id="contactBBM">
								</div>
							</div>
						</div>
					</fieldset>	
					<fieldset>
						<legend>C. STAFF</legend>
						<div class="form-group">
							<label class="col-md-3 control-label" for="inputSuccess">Staff Name <span class="required">*</span></label>
							<div class="col-md-6">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="Staff Name (required)" data-container="body"></i>
									<input name="staffName" type="text" maxlength="20" required="true" class="form-control" id="staffName" value="SUPER ID" readonly="readonly">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="inputSuccess">Staff ID / Username <span class="required">*</span></label>
							<div class="col-md-6">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="Staff ID / Username (required)" data-container="body"></i>
									<input name="staffUsername" type="text" maxlength="20" required="true" class="form-control" id="staffUsername" onkeyup="this.value = this.value.replace(/[^a-z\.]/g,'');" value="SUPERID" readonly="readonly">
								</div>
							</div>
						</div>
	                    <div class="form-group">
							<label class="col-md-3 control-label" for="inputSuccess">Password <span class="required">*</span></label>
							<div class="col-md-6">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="Password (required, number, 6 digit)" data-container="body"></i>
									<input name="staffPassword" type="text" maxlength="6" minlength="6" required="true" class="form-control" id="staffPassword" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" value="123456" readonly="readonly">
								</div>
							</div>
						</div>
					</fieldset>				
				</div>
				<div class="form-actions center">
					<div class="col-md-offset-4 col-md-8">
						<button id="cancel" type="button" class="btn default">Cancel</button>
						<button id="save" type="button" class="btn blue"  data-loading-text="Loading...">Save</button>
					</div>
				</div>
			</form>
		</div>
	   </div>
	</div>
</div>

<style type="text/css">
	#userID,#agentName,#companyName,#address,#zipcode,#city,#country,#phone,#fax,#contactName,#contactPosition,#contactMobile,#contactWA,#contactBBM{
		text-transform: uppercase;
	}
	#email,#contactEmail{
		text-transform: lowercase;
	}
</style>

<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-table"></i>
					<small>Agent List</small>
				</div>
			</div>
			<div class="portlet-body">
				<div id="view-table">
					<div class="table-toolbar"></div>
					<table class="table table-striped table-bordered table-hover" id="refrensi-table">
						<thead>
							<tr>
								<th data-sortable="false">No</th>
								<th>Agent ID</th>
								<th>Agent Name</th>
								<th>City</th>
								<th>Contact Person</th>
								<th>Status</th>
								<th data-sortable="false">Action</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				<div id="view-tree">
					<div id="list-tree" style="margin:15px; font-size:14px;"></div>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_detil" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="form_setting_container">
        <form role="form" class="form-horizontal" id="form_setting" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Setting</h4>
            </div>
            <div class="modal-body"> 
            <input type="hidden" name="agent_ID" id="agent_ID">
				<div class="form-group">
				    <label class="control-label col-md-4">Agent status</label>
					<div class="mt-radio-inline col-md-6">
					    <label class="mt-radio">
					        <input type="radio" name="isStatus" id="isStatus_0" value="0"> Active
					        <span></span>
					    </label>
					    <label class="mt-radio">
					        <input type="radio" name="isStatus" id="isStatus_1" value="1"> Inactive
					        <span></span>
					    </label>
					</div>
				</div>
				<div class="form-group">
				    <label class="control-label col-md-4">Must edit profile</label>
				    <div class="mt-radio-inline col-md-6">
					    <label class="mt-radio">
					        <input type="radio" name="isProfile" id="isProfile_0" value="0"> On
					        <span></span>
					    </label>
					    <label class="mt-radio">
					        <input type="radio" name="isProfile" id="isProfile_1" value="1"> Off
					        <span></span>
					    </label>
					</div>
				</div>
				<div class="form-group">
				    <label class="control-label col-md-4">Reset password SUPERID</label>
				    <div class="mt-radio-inline col-md-6">
					    <label class="mt-radio">
					        <input type="radio" name="isPassword" id="isPassword_0" value="0"> Yes
					        <span></span>
					    </label>
					    <label class="mt-radio">
					        <input type="radio" name="isPassword" id="isPassword_1" value="1"> No
					        <span></span>
					    </label>
					</div>
				</div>
            </div>
            <div class="modal-footer">
            	<button id="save_setting" type="button" class="btn blue" data-loading-text="Loading..."  >Save</button>
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script> 
	var TableAdvanced = function () {
	var initTable1 = function() {

		/*
		* Initialize DataTables, with no sorting on the 'details' column
		*/
		var target='#refrensi-table';
		var oTable = $(target).dataTable( {
			"aoColumnDefs": [
				{
					//"bSortable": false, "aTargets": [ 0,1 ]
				}
			],
			"aoColumns": [
                { "sWidth": "5%" },  
                { "sWidth": "15%" }, 
                { "sWidth": "25%" }, 
                { "sWidth": "15%" }, 
                { "sWidth": "15%" }, 
                { "sWidth": "10%" }, 
                { "sWidth": "15%" }
            ],
			"aaSorting": [[1, 'asc']],
			"aLengthMenu": [
				[10, 20, 50, -1],
				[10, 20, 50, "All"] // change per page values here
			],
			// set the initial value
			"iDisplayLength": 10, // default records per page
			"oLanguage": {
				// language settings
				"sLengthMenu": "Display _MENU_ records",
				"sSearch": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
				"sProcessing": '<img src="<?php echo base_url() ?>assets/global/img/loading-spinner-grey.gif"><span>&nbsp;&nbsp;Loading...</span>',
				"sInfoEmpty": "No records found to show",
				"sAjaxRequestGeneralError": "Could not complete request. Please check your internet connection",
				"sEmptyTable":  "No data available in table",
				"sZeroRecords": "No matching records found",
				"oPaginate": {
					"sPrevious": "Prev",
					"sNext": "Next",
					"sPage": "Page",
					"sPageOf": "of"
				}
			},
			"bAutoWidth": true,   // disable fixed width and enable fluid table
			"bSortCellsTop": true, // make sortable only the first row in thead
			"sPaginationType": "bootstrap_full_number", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
			"bProcessing": true, // enable/disable display message box on record load
			"bServerSide": true, // enable/disable server side ajax loading
			"sAjaxSource": "<?=base_url()?>agent/get_data", // define ajax source URL
			"sServerMethod": "POST"
		});

		$(target+'_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
		$(target+'_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
		$(target+'_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
		$(target+'_wrapper .dataTables_filter input').unbind();
		$(target+'_wrapper .dataTables_filter input').bind('keyup', function(e) {
			if(e.keyCode == 13) {
				oTable.fnFilter(this.value);
			}
		});
		$(target+'_wrapper .dataTables_filter a').bind('click', function(e) {
			var key=$(target+'_wrapper .dataTables_filter input').val();
			oTable.fnFilter(key);
		});
	}

	return {

		//main function to initiate the module
		init: function () {
			if (!$().dataTable) {
				return;
			}
			initTable1();
		}

	};

}();

$(document).ready(function() {
	TableAdvanced.init();

	$('#form_act').bind('click', function(e) {
		$('#userID').attr('readonly', false);
		$('#form_container').slideToggle(500);
		$('#form_master')[0].reset();
	});

	$('#save').click(function(){
		$('#form_master').submit(); 
	});
	
	$('#cancel').click(function(){
		document.getElementById('form_master').reset();
		$('#id').val('');
		$('#act').val('add');
		$('#form_container').slideUp(500);
		loadTbl();
	});
	
	$('#form_container').find('form').validate({
		errorClass: 'help-block',
		errorElement: 'span',
		ignore: 'input[type=hidden]',
		highlight: function(el, errorClass) {
			$(el).parents('.form-group').first().addClass('has-error');
		},
		unhighlight: function(el, errorClass) {
			var $parent = $(el).parents('.form-group').first();
			$parent.removeClass('has-error');
			$parent.find('.help-block').hide();
		},
		errorPlacement: function(error, el) {
			error.appendTo(el.parents('.form-group').find('div:first'));
		},
		submitHandler: function(form) {
			var $theForm = $(form);
			var $data = $theForm.serialize(); 
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url()?>agent/save',
				data: $data,
				beforeSend: function(){
					$('#save').button('loading');
				},
				complete: function() {
					$('#save').button('reset');
				},
				success: function(data) {
					if(data=='true')
						$('#cancel').click();
					
				}
			});
			return false;
		}
	});	


	/*========= SAVE SETTING ===========*/
	$('#save_setting').click(function(){
		$('#form_setting').submit(); 
	});

	$('#form_setting_container').find('form').validate({
		errorClass: 'help-block',
		errorElement: 'span',
		ignore: 'input[type=hidden]',
		highlight: function(el, errorClass) {
			$(el).parents('.form-group').first().addClass('has-error');
		},
		unhighlight: function(el, errorClass) {
			var $parent = $(el).parents('.form-group').first();
			$parent.removeClass('has-error');
			$parent.find('.help-block').hide();
		},
		errorPlacement: function(error, el) {
			error.appendTo(el.parents('.form-group').find('div:first'));
		},
		submitHandler: function(form) {
			var $theForm = $(form);
			var $data = $theForm.serialize(); 
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url()?>agent/savesetting',
				data: $data,
				beforeSend: function(){
					$('#save_setting').button('loading');
				},
				complete: function() {
					$('#save_setting').button('reset');
				},
				success: function(data) {
					if(data=='true')
						loadTbl();
						$('#modal_detil').modal('hide');
					
				}
			});
			return false;
		}
	});	
});

function loadTbl(){
	$("#refrensi-table").dataTable().fnDraw();
}

//BUAT EDIT
function set_val(data){
	var isi=data.split('|');
	
	document.getElementById('act').value 		='edit';
	document.getElementById('id').value 		=isi[0];
	document.getElementById('agentName').value 	=isi[1];
	document.getElementById('companyName').value 		=isi[2];
	document.getElementById('address').value 	=isi[3];
	document.getElementById('zipcode').value 	=isi[4];
	document.getElementById('city').value 		=isi[5];
	document.getElementById('country').value 	=isi[6];
	document.getElementById('phone').value 		=isi[7];
	document.getElementById('fax').value 		=isi[8];
	document.getElementById('email').value 		=isi[9];
	document.getElementById('userID').value 	=isi[10];
	//document.getElementById('password').value 	=isi[11];
	document.getElementById('contactName').value=isi[12];
	document.getElementById('contactPosition').value 	=isi[13]; 
	document.getElementById('contactEmail').value 		=isi[14]; 
	document.getElementById('contactMobile').value 		=isi[15]; 
	document.getElementById('contactWA').value 	=isi[16]; 
	document.getElementById('contactBBM').value =isi[17]; 

	$('#userID').attr('readonly', true);
	$('#staffName').val('');
	$('#staffUsername').val('');
	$('#staffPassword').val('');
	$('#staffName').attr('readonly', true);
	$('#staffUsername').attr('readonly', true);
	$('#staffPassword').attr('readonly', true);
	$('#staffName').attr('required', false);
	$('#staffUsername').attr('required', false);
	$('#staffPassword').attr('required', false);
 
	$('#form_container').slideDown(500);
	$('html, body').animate({scrollTop: 0}, 500);
}

function set_del(data){
	if(confirm("Are you sure to delete this data ?")){
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url() ?>agent/delete',
			data: { id : data },
			success: function(data) {
				if(data=='true'){
					//$('#cancel').click();
					loadTbl();
				}else if(data=='false'){
					$('#cancel').click();
				}
			}
		});
	}
}

function set_setting(data){
	var isi=data.split('|'); //$row->id|$row->isStatus|$row->isProfile|$row->isPassword
	$('#agent_ID').val(isi[0]);
	if(isi[1]=='0'){
		$('#isStatus_0').prop('checked', true);
		$('#isStatus_1').prop('checked', false);		
	}else{
		$('#isStatus_0').prop('checked', false);
		$('#isStatus_1').prop('checked', true);		
	}

	if(isi[2]=='0'){
		$('#isProfile_0').prop('checked', true);
		$('#isProfile_1').prop('checked', false);		
	}else{
		$('#isProfile_0').prop('checked', false);
		$('#isProfile_1').prop('checked', true);			
	}

	if(isi[3]=='0'){
		$('#isPassword_0').prop('checked', true);
		$('#isPassword_1').prop('checked', false);		
	}else{
		$('#isPassword_0').prop('checked', false);
		$('#isPassword_1').prop('checked', true);			
	}

	/*
	document.getElementById('act').value 		='edit';
	document.getElementById('id').value 		=isi[0];
	document.getElementById('agentName').value 	=isi[1];
	document.getElementById('companyName').value 		=isi[2];
	document.getElementById('address').value 	=isi[3];
	document.getElementById('zipcode').value 	=isi[4];
	document.getElementById('city').value 		=isi[5];
	document.getElementById('country').value 	=isi[6];
	document.getElementById('phone').value 		=isi[7];
	document.getElementById('fax').value 		=isi[8];
	document.getElementById('email').value 		=isi[9];
	document.getElementById('userID').value 	=isi[10];
	//document.getElementById('password').value 	=isi[11];
	document.getElementById('contactName').value=isi[12];
	document.getElementById('contactPosition').value 	=isi[13]; 
	document.getElementById('contactEmail').value 		=isi[14]; 
	document.getElementById('contactMobile').value 		=isi[15]; 
	document.getElementById('contactWA').value 	=isi[16]; 
	document.getElementById('contactBBM').value =isi[17]; 

	$('#userID').attr('readonly', true);
	$('#staffName').val('');
	$('#staffUsername').val('');
	$('#staffPassword').val('');
	$('#staffName').attr('readonly', true);
	$('#staffUsername').attr('readonly', true);
	$('#staffPassword').attr('readonly', true);
	$('#staffName').attr('required', false);
	$('#staffUsername').attr('required', false);
	$('#staffPassword').attr('required', false);
 
	$('#form_container').slideDown(500);
	$('html, body').animate({scrollTop: 0}, 500);
	*/
	$('#modal_detil').modal('show');
}

</script>

<?php include "include_footer.php"; ?>