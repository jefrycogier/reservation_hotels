<?php include "include_header.php"; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<script type="text/javascript" src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>

<!--div class="row">
	<div class="col-md-12">
	  <div class="portlet box green">
		<div class="portlet-title">
			<div class="caption">
				<small> <i class="fa fa-note"></i> </small>
			</div>
			<div class="actions">
				 <div class="btn-group btn-group-solid">
				<a class="btn default" href="javascript:;" id="form_act"><i class="fa fa-plus-square"></i>  Add New Product Tour</a>
				</div>
			</div>
		</div>
	   </div>
	</div>
</div-->

<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-table"></i>
					<small>Booking List - CONFIRMED (DEPOSIT RECEIVED)</small>
				</div>
			</div>
			<div class="portlet-body">
				<div id="view-table">
					<div class="table-toolbar"></div>
					<table class="table table-striped table-bordered table-hover" id="refrensi-table">
						<thead>
							<tr>
								<th data-sortable="false">NO</th>
								<th>BOOKED</th>
								<th>PNR</th>
								<th>AGENT</th>
								<th>PRODUCT</th>
								<th>DEPARTURE</th>
								<th>GUEST</th>
								<th data-sortable="false">ACTION</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_detil" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Upload Invoice</h4>
            </div>
        <form role="form" class="form-horizontal" id="form_master" method="post" enctype="multipart/form-data">
            <div class="modal-body"> 
            <fieldset>
            <input type="hidden" name="bookingID" id="bookingID">
            <input type="hidden" name="typeID" value="0">
            	<div class="form-group">
					<label class="col-md-3 control-label" for="inputSuccess">Remarks</label>
					<div class="col-md-6">
						<textarea name="remarks" id="remarks" class="form-control"></textarea>
					</div>
				</div>
            	<div class="form-group">
					<label class="col-md-3 control-label" for="inputSuccess">Invoice <span class="required">*</span></label>
					<div class="col-md-6">
						<input name="userfile" type="file" id="userfile"> <span id="fileDownload"></span>
					</div>
				</div>
            </fieldset>
            </div>
        </form>
            <div class="modal-footer">
                <button type="submit" class="btn dark btn-outline" id="btnSave" onClick="uploadFile2()" >Save</button>
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script type="text/javascript">
	//UPLOAD
	function uploadFile2(){
		if (($("#userfile"))[0].files.length > 0) {
			var $data = $('#form_master').serialize();
		    var file = $("#userfile")[0].files[0];
			var formdata = new FormData();
			formdata.append("userfile", file);
			formdata.append("data", $data);
			var ajax = new XMLHttpRequest();
			ajax.upload.addEventListener("progress", progressHandler2, false);
			ajax.addEventListener("load", completeHandler2, false);
			ajax.addEventListener("error", errorHandler2, false);
			ajax.addEventListener("abort", abortHandler2, false);
			ajax.open("POST", "<?php echo base_url()?>booking_dpreceived/upload/");
			ajax.send(formdata);
		} else {
		    alert("No file chosen!");
		}
	}
	function progressHandler2(event){
		var percent = (event.loaded / event.total) * 100;
		console.log(percent);
	}
	function completeHandler2(event){
		if(event.target.responseText!=''){
			alert(event.target.responseText);
		}else{
			$('#form_master').trigger("reset");
			$('#modal_detil').modal('hide');
			loadTbl();
		}	
	}
	function errorHandler2(event){
		alert("Upload Failed");
	}
	function abortHandler2(event){
		alert("Upload Aborted");
	}
</script>

<script> 
	var TableAdvanced = function () {
	var initTable1 = function() {

		/*
		* Initialize DataTables, with no sorting on the 'details' column
		*/
		var target='#refrensi-table';
		var oTable = $(target).dataTable( {
			"aoColumnDefs": [
				{
					//"bSortable": false, "aTargets": [ 0,1 ]
				}
			],
			"aoColumns": [
                { "sWidth": "5%" },  
                { "sWidth": "10%" }, 
                { "sWidth": "10%" }, 
                { "sWidth": "15%" }, 
                { "sWidth": "30%" }, 
                { "sWidth": "10%" },
                { "sWidth": "10%" },
                { "sWidth": "15%" }
            ],
			"aaSorting": [[1, 'asc']],
			"aLengthMenu": [
				[10, 20, 50, -1],
				[10, 20, 50, "All"] // change per page values here
			],
			// set the initial value
			"iDisplayLength": 10, // default records per page
			"oLanguage": {
				// language settings
				"sLengthMenu": "Display _MENU_ records",
				"sSearch": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
				"sProcessing": '<img src="<?php echo base_url() ?>assets/global/img/loading-spinner-grey.gif"><span>&nbsp;&nbsp;Loading...</span>',
				"sInfoEmpty": "No records found to show",
				"sAjaxRequestGeneralError": "Could not complete request. Please check your internet connection",
				"sEmptyTable":  "No data available in table",
				"sZeroRecords": "No matching records found",
				"oPaginate": {
					"sPrevious": "Prev",
					"sNext": "Next",
					"sPage": "Page",
					"sPageOf": "of"
				}
			},
			"bAutoWidth": true,   // disable fixed width and enable fluid table
			"bSortCellsTop": true, // make sortable only the first row in thead
			"sPaginationType": "bootstrap_full_number", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
			"bProcessing": true, // enable/disable display message box on record load
			"bServerSide": true, // enable/disable server side ajax loading
			"sAjaxSource": "<?=base_url()?>booking_dpreceived/get_data", // define ajax source URL
			"sServerMethod": "POST"
		});

		$(target+'_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
		$(target+'_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
		$(target+'_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
		$(target+'_wrapper .dataTables_filter input').unbind();
		$(target+'_wrapper .dataTables_filter input').bind('keyup', function(e) {
			if(e.keyCode == 13) {
				oTable.fnFilter(this.value);
			}
		});
		$(target+'_wrapper .dataTables_filter a').bind('click', function(e) {
			var key=$(target+'_wrapper .dataTables_filter input').val();
			oTable.fnFilter(key);
		});
	}

	return {

		//main function to initiate the module
		init: function () {
			if (!$().dataTable) {
				return;
			}
			initTable1();
		}

	};

}();

$(document).ready(function() {
	TableAdvanced.init();

	
});

function loadTbl(){
	$("#refrensi-table").dataTable().fnDraw();
}

//BUAT EDIT
function set_val(data){
	var isi=data.split('|');
	
	document.getElementById('act').value='edit';
	document.getElementById('id').value=isi[0];
	document.getElementById('username').value=isi[1];
	document.getElementById('password').value=isi[2];
	document.getElementById('fullname').value=isi[3];
	document.getElementById('position').value=isi[4]; 
 
	$('#form_container').slideDown(500);
	$('html, body').animate({scrollTop: 0}, 500);
}

function set_del(data){
	if(confirm("Are you sure to cancel this booking?")){
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url() ?>booking/cancel',
			data: { id : data },
			success: function(data) {
				if(data=='true'){
					//$('#cancel').click();
					loadTbl();
				}
			}
		});
	}
}

function set_view(data){
	var myWindow = window.open("<?php echo base_url('booking_dpreceived/detil/') ?>/"+data, "", "width=500,height=700");
}

function set_upload(data){
	var isi=data.split('|');
	$('#bookingID').val(isi[0]);
	if(isi[1]!=''){
		$('#fileDownload').html('<p><small style="color:red;">Your Invoice has been uploaded. Do you want to re-write it? or view details <a href="<?php echo base_url('uploads/') ?>/'+isi[1]+'" target="_blank">here</a>.</small></p><p><button type="button" class="btn red btn-outline" id="btnSave" onClick="chengestatus()" >Re Confirm Deposit Payment</button></p>');
	}
	$('#modal_detil').modal('show');
}

function chengestatus(){
	if(confirm("Are you sure to Re Confirm Deposit Payment & Change Status to Full Payment Not Paid?")){
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url() ?>booking/chengestatus',
			data: { 'id' : $('#bookingID').val(), 'newstatus' : '3' },
			success: function(data) {
				if(data=='true'){
					//$('#cancel').click();
					$('#modal_detil').modal('hide');
					loadTbl();
				}
			}
		});
	}
}
</script>

<?php include "include_footer.php"; ?>