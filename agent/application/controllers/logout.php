<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class logout extends CI_Controller {
	
	public function index(){
		//$this->load->view('welcome_message');
		//session_destroy();
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('staffName');
		$this->session->unset_userdata('staffUsername');
		redirect('login');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */