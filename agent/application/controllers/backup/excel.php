<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class excel extends CI_Controller{
	function __construct() {
		parent::__construct();
		$this->load->model('m_roomlist');
	}

    function download(){
        $this->load->model('m_roomlist');
        $depdateID = $this->uri->segment(3);
        $data['nama']= $this->m_roomlist->alllist_one($depdateID);
        $data['all'] = $this->m_roomlist->alllist($depdateID);
        //echo $this->db->last_query(); exit();
        $this->load->view('v_excel_coba',$data);
    }

}

?>