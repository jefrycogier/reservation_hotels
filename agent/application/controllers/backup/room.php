<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class room extends CI_Controller{
	function __construct() {
		parent::__construct();
		$this->load->model('m_room');
	}

	function index() {
        if($this->session->userdata('id')){
            $session_data           = $this->session->userdata('adminName');
            $data['adminName']      = $session_data;
            $data['title']          = 'Dashboard Administrator';
            $this->load->view('v_room', $data);
        }else{
            //If no session, redirect to login page
            redirect('logout');
        }
	}

	function get_data() {
		$aColumns = array('z.id', 'z.settingName', 'z.as', 'z.att', 'z.cwa', 'z.cwb', 'z.cnb');
        $sSearch =  $this->input->post('sSearch',true); 
        $sWhere = "";
        if (isset($sSearch) && $sSearch != "") {
            $sWhere = "AND (";
            for ( $i = 0 ; $i < count($aColumns) ; $i++ ) {
                if($i==1||$i==2){
                    $sWhere .= " ".$aColumns[$i]." LIKE '%".($sSearch)."%' OR ";
                }
            }
            $sWhere = substr_replace( $sWhere, "", - 3 );
            $sWhere .= ') AND z.flag="0"';
        }else{
            $sWhere = ' AND z.flag="0"';
        }
        //echo $sWhere; exit();
		//filter indovidual create by rizal 14/09/2015
		for ($i=0 ; $i<count($aColumns) ; $i++){
			if ($this->input->post('bSearchable_'.$i) == "true" && $this->input->post('sSearch_'.$i) != '' ){
				$sWhere .= " AND ".$aColumns[$i]." LIKE '%".$this->input->post('sSearch_'.$i)."%' ";
			}
		}
		
        $iTotalRecords  = $this->m_room->get_total($aColumns,$sWhere);
        $iDisplayLength = intval($this->input->post('iDisplayLength',true));
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($this->input->post('iDisplayStart',true));
        
        // $sEcho          = intval($_REQUEST['sEcho']);
        $iSortCol_0     = $this->input->post('iSortCol_0',true);
        
        $records = array();
        $records["aaData"] = array();
        $sOrder = "";
        if (isset($iDisplayStart) && $iDisplayLength != '-1' ) {
            $sLimit = "limit ".intval($iDisplayLength)." OFFSET ".intval( $iDisplayStart );
        }

        if (isset($iSortCol_0)) {
            $sOrder = "ORDER BY  ";
            for ( $i = 0 ; $i < intval($this->input->post('iSortingCols')) ; $i++ ) {
                if ( $this->input->post('bSortable_'.intval($this->input->post('iSortCol_'.$i))) == "true" ) {
                    $sOrder .= "".$aColumns[ intval($this->input->post('iSortCol_'.$i)) ]." ".
                    ($this->input->post('sSortDir_'.$i) === 'asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", - 2 );
            if ( $sOrder == "ORDER BY" ) {
                $sOrder = "";
            }
        }

        $data = $this->m_room->get_data($sLimit,$sWhere,$sOrder,$aColumns); 
		//echo $this->db->last_query();
        $no   = 1 + $iDisplayStart;
        foreach ($data as $row) {
            $pars_data = "$row->id|$row->settingName|$row->as|$row->att|$row->cwa|$row->cwb|$row->cnb";
/*
            if ($row->edu_active == 't') {
                    $status = '<span class="label label-sm label-success">Aktif</span>';
            }
            else {
                    $status = '<span class="label label-sm label-danger">Non Aktif</span>';
            }
*/
            $action = '
            <a href="javascript:void(0)" onclick="set_val(\''.$pars_data.'\')" class="btn btn-xs btn-warning" title="Edit">
                <i class="fa fa-pencil"></i>
            </a>
            <a href="javascript:void(0)" onclick="set_del(\''.$row->id.'\')" class="btn btn-xs btn-danger" title="Delete">
                <i class="fa fa-trash-o"></i>
            </a>';

            $records["aaData"][] = array(
            		$no,
                    strtoupper($row->settingName), 
                    strtoupper($row->as), 
                    strtoupper($row->att), 
                    strtoupper($row->cwa), 
                    strtoupper($row->cwb), 
                    strtoupper($row->cnb), 
                    $action
            );
            $no++;
        }

        //$records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;

        echo json_encode($records);

	}

	public function save(){
        $sql	= true;

        $session= $this->session->userdata('id');
        $act 	= $this->input->post('act', true);

        $data=array(
            //'id'  	        => $this->input->post('id', true),
            'settingName'   => strtoupper($this->input->post('settingName', true)),
            'as'            => $this->input->post('as', true),
            'att'           => $this->input->post('att', true),
            'cwa'           => $this->input->post('cwa', true),
            'cwb'           => $this->input->post('cwb', true),
            'cnb'           => $this->input->post('cnb', true)
        );

        if($act == 'add'){
            $data["flag"]   = '0';
            $sql            = $this->m_room->add($data);
        }elseif($act == 'edit'){
            $data["id"]     = $this->input->post('id', true);
            $sql            = $this->m_room->edit($data);
        }

        if($sql == true){
            echo 'true';
        } 
        else { 
            echo 'false'; 
        }
	}
	
	public function delete(){
        $session        = $this->session->userdata('id');
        $id             = $this->input->post('id', true);

        $data["id"]     = $this->input->post('id', true);
        $data["flag"]   = '1';
        $sql = $this->m_room->del($data);
        //echo $this->db->last_query();
        if($sql == true ){		
            echo 'true';
        }else{
            echo 'false';
        }
	}

}

?>