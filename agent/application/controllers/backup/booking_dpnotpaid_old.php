<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class booking_dpnotpaid extends CI_Controller{
	function __construct() {
		parent::__construct();
		$this->load->model('m_booking');
        $this->load->model('m_product');
	}

	function index() {
        if($this->session->userdata('id')){
            $session_data           = $this->session->userdata('adminName');
            $data['adminName']      = $session_data;
            $data['title']          = 'Dashboard Administrator';
            $this->load->view('v_booking_dpnotpaid', $data);
        }else{
            //If no session, redirect to login page
            redirect('logout');
        }
	}

	function get_data() {
		$aColumns = array('z.id', 'z.bookingCode', 'z.agentName', 'z.productName', 'z.depdate');
        $sSearch =  $this->input->post('sSearch',true); 
        $sWhere = "";
        if (isset($sSearch) && $sSearch != "") {
            $sWhere = "AND (";
            for ( $i = 0 ; $i < count($aColumns) ; $i++ ) {
                if($i==1||$i==2||$i==3||$i==4){
                    $sWhere .= " ".$aColumns[$i]." LIKE '%".($sSearch)."%' OR ";
                }
            }
            $sWhere = substr_replace( $sWhere, "", - 3 );
            $sWhere .= ') AND z.bookingStatus="1"';
        }else{
            $sWhere = ' AND z.bookingStatus="1"';
        }
        //echo $sWhere; exit();
		//filter indovidual create by rizal 14/09/2015
		for ($i=0 ; $i<count($aColumns) ; $i++){
			if ($this->input->post('bSearchable_'.$i) == "true" && $this->input->post('sSearch_'.$i) != '' ){
				$sWhere .= " AND ".$aColumns[$i]." LIKE '%".$this->input->post('sSearch_'.$i)."%' ";
			}
		}
		
        $iTotalRecords  = $this->m_booking->get_total($aColumns,$sWhere);
        $iDisplayLength = intval($this->input->post('iDisplayLength',true));
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($this->input->post('iDisplayStart',true));
        
        // $sEcho          = intval($_REQUEST['sEcho']);
        $iSortCol_0     = $this->input->post('iSortCol_0',true);
        
        $records = array();
        $records["aaData"] = array();
        $sOrder = "";
        if (isset($iDisplayStart) && $iDisplayLength != '-1' ) {
            $sLimit = "limit ".intval($iDisplayLength)." OFFSET ".intval( $iDisplayStart );
        }

        if (isset($iSortCol_0)) {
            $sOrder = "ORDER BY  ";
            for ( $i = 0 ; $i < intval($this->input->post('iSortingCols')) ; $i++ ) {
                if ( $this->input->post('bSortable_'.intval($this->input->post('iSortCol_'.$i))) == "true" ) {
                    $sOrder .= "".$aColumns[ intval($this->input->post('iSortCol_'.$i)) ]." ".
                    ($this->input->post('sSortDir_'.$i) === 'asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", - 2 );
            if ( $sOrder == "ORDER BY" ) {
                $sOrder = "";
            }
        }

        $data = $this->m_booking->get_data($sLimit,$sWhere,$sOrder,$aColumns); 
		//echo $this->db->last_query();
        $no   = 1 + $iDisplayStart;
        foreach ($data as $row) {

            $txt = '';

            $action = '
            <div class="btn-group">
                <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown"> Actions
                    <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu" role="menu">
            ';

            $get_download = $this->m_booking->get_upload_file($row->id,0);
            if(empty($get_download)){
                $param_data = $row->id.'|';
                $action .= '';
            }else{
                $param_data = $row->id.'|'.$get_download->fileUpload.'|'.$get_download->bankName.'|'.$get_download->accountName.'|'.$get_download->accountNo.'|'.strtoupper(date("d M Y", strtotime($get_download->transferDate)));
                $action .= '
                    <li>
                        <a href="javascript:void(0)" onclick="set_download(\''.$param_data.'\')"><i class="fa fa-download"></i> Download Payment Slip</a>
                    </li>';
            }

            $action .= '    
                    <li>
                        <a href="javascript:void(0)" onclick="view_detil(\''.$txt.'\')"> <i class="fa fa-search"></i> Detail</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" onclick="set_del(\''.$row->id.'\')"> <i class="fa fa-ban"></i> Cancel </a>
                    </li>
                </ul>
            </div>
            ';

            

            /*
            $action .= '
            <a href="javascript:void(0)" onclick="set_view(\''.$row->id.'\')" class="btn btn-xs btn-warning" title="View Detil">
                <i class="fa fa-search"></i>
            </a>
            <a href="javascript:void(0)" onclick="set_del(\''.$row->id.'\')" class="btn btn-xs btn-danger" title="Cancel">
                <i class="fa fa-ban"></i>
            </a>';
            */

            $timelimit = strtotime('+2 day', strtotime($row->createdDate));

            $records["aaData"][] = array(
            		$no,
                    strtoupper(date("d M Y", strtotime($row->createdDate))), 
                    strtoupper($row->bookingCode),
                    strtoupper($row->agentName), 
                    strtoupper($row->productName), 
                    strtoupper(date("d M Y", strtotime($row->depdate))), 
                    strtoupper(date("d M Y", $timelimit)),
                    $action
            );
            $no++;
        }

        //$records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;

        echo json_encode($records);

	}

    function detil(){
        if($this->session->userdata('id')){
            $session_data           = $this->session->userdata('adminName');
            $data['adminName']      = $session_data;
            $data['title']          = 'Dashboard Administrator';

            $id                     = $this->uri->segment(3);
            $data['all']            = $this->m_booking->get_booking($id);
            foreach ($data['all'] as $value) {
                $data['product']    = $this->m_product->get_detil_wjoin($value->productID);
            }
            $data['guest']          = $this->m_booking->get_guest($id);
            //print_r($data['product']);
            //$data['product']        = $this->m_product->get_detil($id);
            $this->load->view('v_booking_detail', $data);
        }else{
            //If no session, redirect to login page
            redirect('logout');
        }
    }

}

?>