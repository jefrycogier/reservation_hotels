<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class booking_dpreceived extends CI_Controller{
	function __construct() {
		parent::__construct();
		$this->load->model('m_booking');
        $this->load->model('m_product');
	}

	function index() {
        if($this->session->userdata('id')){
            $session_data           = $this->session->userdata('adminName');
            $data['adminName']      = $session_data;
            $data['title']          = 'Dashboard Administrator';
            $this->load->view('v_booking_dpreceived', $data);
        }else{
            //If no session, redirect to login page
            redirect('logout');
        }
	}

	function get_data() {
		$aColumns = array('z.id', 'z.bookingCode', 'z.agentName', 'z.productName', 'z.depdate');
        $sSearch =  $this->input->post('sSearch',true); 
        $sWhere = "";
        if (isset($sSearch) && $sSearch != "") {
            $sWhere = "AND (";
            for ( $i = 0 ; $i < count($aColumns) ; $i++ ) {
                if($i==1||$i==2||$i==3||$i==4){
                    $sWhere .= " ".$aColumns[$i]." LIKE '%".($sSearch)."%' OR ";
                }
            }
            $sWhere = substr_replace( $sWhere, "", - 3 );
            $sWhere .= ') AND z.bookingStatus="2"';
        }else{
            $sWhere = ' AND z.bookingStatus="2"';
        }
        //echo $sWhere; exit();
		//filter indovidual create by rizal 14/09/2015
		for ($i=0 ; $i<count($aColumns) ; $i++){
			if ($this->input->post('bSearchable_'.$i) == "true" && $this->input->post('sSearch_'.$i) != '' ){
				$sWhere .= " AND ".$aColumns[$i]." LIKE '%".$this->input->post('sSearch_'.$i)."%' ";
			}
		}
		
        $iTotalRecords  = $this->m_booking->get_total($aColumns,$sWhere);
        $iDisplayLength = intval($this->input->post('iDisplayLength',true));
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($this->input->post('iDisplayStart',true));
        
        // $sEcho          = intval($_REQUEST['sEcho']);
        $iSortCol_0     = $this->input->post('iSortCol_0',true);
        
        $records = array();
        $records["aaData"] = array();
        $sOrder = "";
        if (isset($iDisplayStart) && $iDisplayLength != '-1' ) {
            $sLimit = "limit ".intval($iDisplayLength)." OFFSET ".intval( $iDisplayStart );
        }

        if (isset($iSortCol_0)) {
            $sOrder = "ORDER BY  ";
            for ( $i = 0 ; $i < intval($this->input->post('iSortingCols')) ; $i++ ) {
                if ( $this->input->post('bSortable_'.intval($this->input->post('iSortCol_'.$i))) == "true" ) {
                    $sOrder .= "".$aColumns[ intval($this->input->post('iSortCol_'.$i)) ]." ".
                    ($this->input->post('sSortDir_'.$i) === 'asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", - 2 );
            if ( $sOrder == "ORDER BY" ) {
                $sOrder = "";
            }
        }

        $data = $this->m_booking->get_data($sLimit,$sWhere,$sOrder,$aColumns); 
		//echo $this->db->last_query();
        $no   = 1 + $iDisplayStart;
        foreach ($data as $row) {

            $get_upload = $this->m_booking->get_upload_file_invoice($row->id,0);
            if(empty($get_upload)){
                $param_data = $row->id.'|';
            }else{
                $param_data = $row->id.'|'.$get_upload->fileUpload;
            }

            $action = '
            <a href="javascript:void(0)" onclick="set_upload(\''.$param_data.'\')" class="btn btn-xs btn-info" title="Upload Invoice">
                <i class="fa fa-cloud-upload"></i>
            </a>';

            $get_download = $this->m_booking->get_upload_file($row->id,0);
            if(empty($get_download)){
                $action .= '';
            }else{
                $action .= '
                    <a href="http://kuoni.wuisan.com/booking/uploads/'.$get_download->fileUpload.'" class="btn btn-xs btn-info" title="Download Payment Slip" target="_blank">
                        <i class="fa fa-download"></i>
                    </a>
                ';
            }
            
            $action .= '
            <a href="javascript:void(0)" onclick="set_view(\''.$row->id.'\')" class="btn btn-xs btn-warning" title="View Detil">
                <i class="fa fa-search"></i>
            </a>
            <a href="javascript:void(0)" onclick="set_del(\''.$row->id.'\')" class="btn btn-xs btn-danger" title="Cancel">
                <i class="fa fa-ban"></i>
            </a>';

            $timelimit = strtotime('+2 day', strtotime($row->createdDate));

            $records["aaData"][] = array(
            		$no,
                    strtoupper(date("d M Y", strtotime($row->createdDate))), 
                    strtoupper($row->bookingCode),
                    strtoupper($row->agentName), 
                    strtoupper($row->productName), 
                    strtoupper(date("d M Y", strtotime($row->depdate))), 
//                    strtoupper(date("d M Y", $timelimit)),
                    $action
            );
            $no++;
        }

        //$records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;

        echo json_encode($records);

	}

    function detil(){
        if($this->session->userdata('id')){
            $session_data           = $this->session->userdata('adminName');
            $data['adminName']      = $session_data;
            $data['title']          = 'Dashboard Administrator';

            $id                     = $this->uri->segment(3);
            $data['all']            = $this->m_booking->get_booking($id);
            foreach ($data['all'] as $value) {
                $data['product']    = $this->m_product->get_detil_wjoin($value->productID);
            }
            $data['guest']          = $this->m_booking->get_guest($id);
            //print_r($data['product']);
            //$data['product']        = $this->m_product->get_detil($id);
            $this->load->view('v_booking_detail', $data);
        }else{
            //If no session, redirect to login page
            redirect('logout');
        }
    }

    function upload(){
        $status = "";
        $msg    = "";
        $file_element_name = 'userfile';

        if ($status != "error")
        {
            $config['upload_path']      = './uploads/';
            $config['allowed_types']    = 'gif|jpg|png|doc|txt|pdf';
            $config['max_size']         = 1024 * 8;
            $config['encrypt_name']     = TRUE;
      
            $this->load->library('upload', $config);
      
            if (!$this->upload->do_upload($file_element_name)){
                //$status = 'error';
                $status = $this->upload->display_errors('', '');
            }else{
                $img = $this->upload->data();
                $pecah          = explode("&", $this->input->post('data'));
                $p_bookingID    = explode("=", $pecah[0]); 
                $bookingID      = $p_bookingID[1];
                $p_typeID       = explode("=", $pecah[1]); 
                $typeID         = $p_typeID[1];
                $p_remarks      = explode("=", $pecah[2]); 
                $remarks        = strtoupper(str_replace("+", " ", $p_remarks[1]));

                $data = array(
                    'id'            => NULL,
                    'bookingID'     => $bookingID,
                    'typeID'        => $typeID,
                    'remarks'       => $remarks,
                    'fileUpload'    => $img['file_name'],
                    'staffID'       => $this->session->userdata('id')
                );

                $insert_order = $this->m_booking->upload($data);
                
                $status = '';
                $msg = $insert_order;
            }
            @unlink($_FILES[$file_element_name]);
        }
        echo $status;
    }

}

?>