<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class agent extends CI_Controller{
	function __construct() {
		parent::__construct();
		$this->load->model('m_agent');
	}

	function index() {
        if($this->session->userdata('id')){
            $session_data           = $this->session->userdata('adminName');
            $data['adminName']      = $session_data;
            $data['title']          = 'Dashboard Administrator';
            $this->load->view('v_agent', $data);
        }else{
            //If no session, redirect to login page
            redirect('logout');
        }
	}

	function get_data() {
		$aColumns = array('z.id', 'z.userID', 'z.agentName', 'z.city', 'z.contactName');
        $sSearch =  $this->input->post('sSearch',true); 
        $sWhere = "";
        if (isset($sSearch) && $sSearch != "") {
            $sWhere = "AND (";
            for ( $i = 0 ; $i < count($aColumns) ; $i++ ) {
                if($i==1||$i==2||$i==3||$i==4){
                    $sWhere .= " ".$aColumns[$i]." LIKE '%".($sSearch)."%' OR ";
                }
            }
            $sWhere = substr_replace( $sWhere, "", - 3 );
            $sWhere .= ') AND z.flag="0"';
        }else{
            $sWhere = ' AND z.flag="0"';
        }
        //echo $sWhere; exit();
		//filter indovidual create by rizal 14/09/2015
		for ($i=0 ; $i<count($aColumns) ; $i++){
			if ($this->input->post('bSearchable_'.$i) == "true" && $this->input->post('sSearch_'.$i) != '' ){
				$sWhere .= " AND ".$aColumns[$i]." LIKE '%".$this->input->post('sSearch_'.$i)."%' ";
			}
		}
		
        $iTotalRecords  = $this->m_agent->get_total($aColumns,$sWhere);
        $iDisplayLength = intval($this->input->post('iDisplayLength',true));
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($this->input->post('iDisplayStart',true));
        
        // $sEcho          = intval($_REQUEST['sEcho']);
        $iSortCol_0     = $this->input->post('iSortCol_0',true);
        
        $records = array();
        $records["aaData"] = array();
        $sOrder = "";
        if (isset($iDisplayStart) && $iDisplayLength != '-1' ) {
            $sLimit = "limit ".intval($iDisplayLength)." OFFSET ".intval( $iDisplayStart );
        }

        if (isset($iSortCol_0)) {
            $sOrder = "ORDER BY  ";
            for ( $i = 0 ; $i < intval($this->input->post('iSortingCols')) ; $i++ ) {
                if ( $this->input->post('bSortable_'.intval($this->input->post('iSortCol_'.$i))) == "true" ) {
                    $sOrder .= "".$aColumns[ intval($this->input->post('iSortCol_'.$i)) ]." ".
                    ($this->input->post('sSortDir_'.$i) === 'asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", - 2 );
            if ( $sOrder == "ORDER BY" ) {
                $sOrder = "";
            }
        }

        $data = $this->m_agent->get_data($sLimit,$sWhere,$sOrder,$aColumns); 
		//echo $this->db->last_query();
        $no   = 1 + $iDisplayStart;
        foreach ($data as $row) {
            $pars_data = "$row->id|$row->agentName|$row->companyName|$row->address|$row->zipcode|$row->city|$row->country|$row->phone|$row->fax|$row->email|$row->userID|0|$row->contactName|$row->contactPosition|$row->contactEmail|$row->contactMobile|$row->contactWA|$row->contactBBM";

            $pars_setting = "$row->id|$row->isStatus|$row->isProfile|$row->isPassword";

            if($row->isStatus == 0){
                    $status = '<span class="label label-sm label-success">Active</span>';
            }else{
                    $status = '<span class="label label-sm label-danger">Inactive</span>';
            }

            $action = '
            <div class="btn-group">
                <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown"> Action
                    <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a href="javascript:void(0)" onclick="set_val(\''.$pars_data.'\')"> <i class="fa fa-pencil"></i> Edit </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" onclick="set_setting(\''.$pars_setting.'\')"> <i class="fa fa-cogs"></i> Setting </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" onclick="set_del(\''.$row->id.'\')"> <i class="fa fa-trash-o"></i> Delete </a>
                    </li>
                </ul>
            </div>';

            $records["aaData"][] = array(
            		$no,
                    strtolower($row->userID),
                    strtoupper($row->agentName), 
                    strtoupper($row->city), 
                    strtoupper($row->contactName), 
                    $status,
                    $action
            );
            $no++;
        }

        //$records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;

        echo json_encode($records);

	}

	public function save(){
        $sql	= true;

        $session= $this->session->userdata('id');
        $act 	= $this->input->post('act', true);

        $data=array(
                //'id'  	        => $this->input->post('id', true),
                'agentName'         => strtoupper($this->input->post('agentName', true)),
                'companyName'       => strtoupper($this->input->post('companyName', true)),
                'address'           => strtoupper($this->input->post('address', true)),
                'zipcode'           => strtoupper($this->input->post('zipcode', true)),
                'city'              => strtoupper($this->input->post('city', true)),
                'country'           => strtoupper($this->input->post('country', true)),
                'phone'             => strtoupper($this->input->post('phone', true)),
                'fax'               => strtoupper($this->input->post('fax', true)),
                'email'             => strtolower($this->input->post('email', true)),
                'userID'  	        => strtolower($this->input->post('userID', true)),
                'contactName'  	    => strtoupper($this->input->post('contactName', true)), 
                'contactPosition'   => strtoupper($this->input->post('contactPosition', true)), 
                'contactEmail'      => strtolower($this->input->post('contactEmail', true)), 
                'contactMobile'     => strtoupper($this->input->post('contactMobile', true)),
                'contactWA'         => strtoupper($this->input->post('contactWA', true)),
                'contactBBM'        => strtoupper($this->input->post('contactBBM', true)),
                'isStatus'          => 0,
                'isProfile'         => 1,
                'isPassword'        => 1
        );

        if($act == 'add'){
            // $data['created_by'] = $session['user_id']; 
            //$data["createdDate"]    = date('Y-m-d H:i:s');
            //$data["createdBy"]      = $session;
            $data["flag"]   = '0';
            $sql            = $this->m_agent->add($data);
            if($sql != false){
                $agentID = $sql;
                $data=array(
                    //'id'              => $this->input->post('id', true),
                    'agentID'           => $sql,
                    'staffName'         => strtoupper($this->input->post('staffName', true)),
                    'staffUsername'     => strtolower($this->input->post('staffUsername', true)),
                    'staffPassword'     => $this->input->post('staffPassword', true),
                    'flag'              => 0
                );
                $sql            = $this->m_agent->add_staff($data);
                echo 'true';
            } 
            else { 
                echo 'false'; 
            }
        }elseif($act == 'edit'){
            $data["id"]     = $this->input->post('id', true);
            $sql            = $this->m_agent->edit($data);
            if($sql != false){
                echo 'true';
            } 
            else { 
                echo 'false'; 
            }
        }
	}
	
	public function delete(){
        $session        = $this->session->userdata('id');
        $id             = $this->input->post('id', true);

        $data["id"]     = $this->input->post('id', true);
        $data["flag"]   = '1';
        $sql = $this->m_agent->del($data);
        //echo $this->db->last_query();
        if($sql == true ){		
            echo 'true';
        }else{
            echo 'false';
        }
	}

    public function savesetting(){
        $sql    = true;
        $data=array(
            'id'            => $this->input->post('agent_ID', true),
            'isStatus'      => $this->input->post('isStatus', true),
            'isProfile'     => $this->input->post('isProfile', true),
            'isPassword'    => $this->input->post('isPassword', true)
        );
        $sql   = $this->m_agent->edit_setting($data);
        if($sql == true){
            echo 'true';
        } 
        else { 
            echo 'false'; 
        }
    }

}

?>