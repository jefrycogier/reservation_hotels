<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class booking_all extends CI_Controller{
	function __construct() {
		parent::__construct();
		$this->load->model('m_booking');
        $this->load->model('m_product');
	}

	function index() {
        if($this->session->userdata('id')){
            $session_data           = $this->session->userdata('adminName');
            $data['adminName']      = $session_data;
            $data['title']          = 'Dashboard Administrator';
            $this->load->view('v_booking_all', $data);
        }else{
            //If no session, redirect to login page
            redirect('logout');
        }
	}

	function get_data() {
		$aColumns = array('z.id', 'z.createdDate', 'z.agentName', 'z.productName', 'z.depdate', 'z.guestName', 'z.timelimit', 'z.ketStatus');
        $sSearch =  $this->input->post('sSearch',true); 
        $sWhere = "";
        if (isset($sSearch) && $sSearch != "") {
            $sWhere = "AND (";
            for ( $i = 0 ; $i < count($aColumns) ; $i++ ) {
                if($i==2||$i==3||$i==5||$i==7){
                    $sWhere .= " ".$aColumns[$i]." LIKE '%".($sSearch)."%' OR ";
                }
            }
            $sWhere = substr_replace( $sWhere, "", - 3 );
            $sWhere .= ') AND z.bookingStatus!="0" AND z.bookingStatus!="6"';
        }else{
            $sWhere = ' AND z.bookingStatus!="0" AND z.bookingStatus!="6"';
        }
        //echo $sWhere; exit();
		//filter indovidual create by rizal 14/09/2015
		for ($i=0 ; $i<count($aColumns) ; $i++){
			if ($this->input->post('bSearchable_'.$i) == "true" && $this->input->post('sSearch_'.$i) != '' ){
				$sWhere .= " AND ".$aColumns[$i]." LIKE '%".$this->input->post('sSearch_'.$i)."%' ";
			}
		}
		
        $iTotalRecords  = $this->m_booking->get_total($aColumns,$sWhere);
        $iDisplayLength = intval($this->input->post('iDisplayLength',true));
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($this->input->post('iDisplayStart',true));
        
        // $sEcho          = intval($_REQUEST['sEcho']);
        $iSortCol_0     = $this->input->post('iSortCol_0',true);
        
        $records = array();
        $records["aaData"] = array();
        $sOrder = "";
        if (isset($iDisplayStart) && $iDisplayLength != '-1' ) {
            $sLimit = "limit ".intval($iDisplayLength)." OFFSET ".intval( $iDisplayStart );
        }

        if (isset($iSortCol_0)) {
            $sOrder = "ORDER BY  ";
            for ( $i = 0 ; $i < intval($this->input->post('iSortingCols')) ; $i++ ) {
                if ( $this->input->post('bSortable_'.intval($this->input->post('iSortCol_'.$i))) == "true" ) {
                    $sOrder .= "".$aColumns[ intval($this->input->post('iSortCol_'.$i)) ]." ".
                    ($this->input->post('sSortDir_'.$i) === 'asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", - 2 );
            if ( $sOrder == "ORDER BY" ) {
                $sOrder = "ORDER BY z.id desc";
            }
        }

        $data = $this->m_booking->get_data($sLimit,$sWhere,$sOrder,$aColumns); 
		//echo $this->db->last_query();
        $no   = 1 + $iDisplayStart;
        foreach ($data as $row) {

            $action = '
            <a href="'.base_url().'booking_all/detils/'.$row->id.'" class="btn btn-xs btn-info" title="Detail"> Detail <i class="fa fa-play-circle"></i></a>
            ';

            if($row->bookingStatus==1){
                $timelimit = strtoupper(date("d M", strtotime($row->timelimit)));
            }else{
                $timelimit = '-';
            }

            $records["aaData"][] = array(
            		$no,
                    strtoupper(date("d M", strtotime($row->createdDate)).'<br>'.$row->bookingCode), 
                    strtoupper($row->agentName),
                    //strtoupper($row->agentName), 
                    strtoupper($row->productName), 
                    strtoupper(date("d M", strtotime($row->depdate))), 
                    strtoupper($row->guestName),
                    $timelimit,
                    strtoupper($row->ketStatus),
                    $action
            );
            $no++;
        }

        //$records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;

        echo json_encode($records);

	}

    function upload(){
        $status = "";
        $msg    = "";
        $file_element_name = 'userfile';

        if ($status != "error")
        {
            $config['upload_path']      = './uploads/';
            $config['allowed_types']    = 'gif|jpg|png|doc|txt|pdf';
            $config['max_size']         = 1024 * 8;
            $config['encrypt_name']     = TRUE;
      
            $this->load->library('upload', $config);
      
            if (!$this->upload->do_upload($file_element_name)){
                //$status = 'error';
                $status = $this->upload->display_errors('', '');
            }else{
                $img = $this->upload->data();
                //echo $this->input->post('data'); exit();
                //bookingID=6&typeID=0&note=asdasdasdasdsad+sadsadsadsad
                $pecah          = explode("&", $this->input->post('data'));
                $p_bookingID    = explode("=", $pecah[0]); 
                $bookingID      = $p_bookingID[1];
                $p_typeID       = explode("=", $pecah[1]); 
                $typeID         = $p_typeID[1];
                $p_note         = explode("=", $pecah[2]); 
                $note           = strtoupper(str_replace("+", " ", $p_note[1]));

                $data = array(
                    'id'            => NULL,
                    'bookingID'     => $bookingID,
                    'typeID'        => $typeID,
                    'remarks'       => $note,
                    'fileUpload'    => $img['file_name'],
                    'staffID'       => $this->session->userdata('id')
                );

                $insert_order = $this->m_booking->upload($data);
                
                $status = '';
                $msg = $insert_order;
            }
            @unlink($_FILES[$file_element_name]);
        }
        echo $status;
    }

    function detils(){
        if($this->session->userdata('id')){
            /*== Cek Session 15/11 ==*/
            $session_data           = $this->session->userdata('adminName');
            $data['adminName']      = $session_data;
            $data['title']          = 'Dashboard Administrator';
            //$this->load->view('v_booking_all', $data);

            $id                     = $this->uri->segment(3);
            $data['data_booking']   = $this->m_booking->get_booking_detil($id);
            $data['data_product']   = $this->m_booking->get_detil($data['data_booking']->productID);
            $data['data_depdate']   = $this->m_booking->get_depdate_detil($data['data_booking']->depdateID);
            $data['data_room']      = $this->m_booking->get_room_detil($data['data_booking']->roomID);
            $data['data_guest']     = $this->m_booking->get_guest_detil($id);
            $data['data_guest_total'] = $this->m_booking->get_guest_total($id);

            $data['data_final_files'] = $this->m_booking->get_final_files($id);
            //echo $this->db->last_query();
            /*== File upload deposit 16/11 ==*/
            $get_upload = $this->m_booking->get_upload_file($id,0);
            if(empty($get_upload)){
                $data['param_data'] = $id.'|';
            }else{
                $data['param_data'] = $id.'|'.$get_upload->fileUpload.'|'.strtoupper(date("d M Y", strtotime($get_upload->transferDate))).'|'.$get_upload->note.'|';
            }

            /*== File upload full payment 16/11 ==*/
            $get_upload2 = $this->m_booking->get_upload_file($id,1);
            if(empty($get_upload2)){
                $data['param_data2'] = $id.'|';
            }else{
                $data['param_data2'] = $id.'|'.$get_upload2->fileUpload.'|'.strtoupper(date("d M Y", strtotime($get_upload2->transferDate))).'|'.$get_upload2->note.'|';
            }

            /*== File upload full payment invoice 22/11 ==*/
            $get_upload3 = $this->m_booking->get_upload_file_invoice($id);
            if(empty($get_upload3)){
                $data['param_data3'] = $id.'|';
            }else{
                $data['param_data3'] = $id.'|'.$get_upload3->fileUpload.'|'.$get_upload3->remarks.'|';
            }

            $this->load->view('v_booking_detail', $data);
        }else{
            //If no session, redirect to login page
            redirect('logout');
        }
    }

    function editguest(){
        $sql    = true;
        //gid=7&gdob=13+SEP+1993&gpassport=123789456
        $data = array(
            'id'        => $this->input->post('gid', true),
            'gtittle'   => strtoupper($this->input->post('gtittle', true)),
            'gfname'    => strtoupper($this->input->post('gfname', true)),
            'glname'    => strtoupper($this->input->post('glname', true)),
            'gdob'      => strtoupper($this->input->post('gdob', true)),
            'gpassport' => strtoupper($this->input->post('gpassport', true))
        );

        $sql   = $this->m_booking->edit_guest($data);
        if($sql == true ){      
            echo 'true';
        }else{
            echo 'false';
        }
    }

    function edittimelimit(){
        $sql    = true;
        $pisah  = explode("/", $this->input->post('etimelimit', true));
        $timelimit = $pisah[2].'-'.$pisah[0].'-'.$pisah[1];

        $data = array(
            'id'        => $this->input->post('eid', true),
            'timelimit' => $timelimit
        );

        $sql   = $this->m_booking->edit_timelimit($data);
        if($sql == true ){      
            echo 'true';
        }else{
            echo 'false';
        }
    }
}

?>