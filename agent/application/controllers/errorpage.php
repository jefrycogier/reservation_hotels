<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class errorpage extends CI_Controller {
	public function index(){
		$data['title']			= '404 Page';
		$this->load->view('v_errorpage',$data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */