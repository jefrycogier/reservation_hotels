<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class dashboard extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('m_dashboard');
	}

	
	public function index(){
		$this->home();
	}

	public function home(){
		$this->load->view('v_dashboard');
		/*
		if($this->session->userdata('id')){
			$this->all();
		}else{
			//If no session, redirect to login page
			redirect('logout');
		}
		*/
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */