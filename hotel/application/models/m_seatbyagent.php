<?php

class m_seatbyagent extends CI_Model {
	
	private $table = 'products', $id = 'id' ;

	public function get_total($aColumns,$sWhere){

		$result = $this->db->query("SELECT COUNT(*) TOTAL FROM(
				SELECT `a`.*, COUNT(c.id) AS paxs 
				FROM (`ms_agent` a) 
				  JOIN `booking` b ON `b`.`agentID` = `a`.`id` 
				  JOIN `booking_detail` c ON `c`.`bookingID` = `b`.`id` 
				WHERE `a`.`flag` = '0' AND `b`.`bookingStatus` < '6' 
				GROUP BY `a`.`id` 
				HAVING COUNT(c.id) > 0 
				ORDER BY `paxs` DESC ) z WHERE 0=0 $sWhere");
		foreach ($result->result() as $row) {
			$total = $row->TOTAL;
		}
		return $total;
	}

	public function get_data($sLimit,$sWhere,$sOrder,$aColumns){
		$sql = "SELECT * FROM(
				SELECT `a`.*, COUNT(c.id) AS paxs 
				FROM (`ms_agent` a) 
				  JOIN `booking` b ON `b`.`agentID` = `a`.`id` 
				  JOIN `booking_detail` c ON `c`.`bookingID` = `b`.`id` 
				WHERE `a`.`flag` = '0' AND `b`.`bookingStatus` < '6' 
				GROUP BY `a`.`id` 
				HAVING COUNT(c.id) > 0 
				ORDER BY `paxs` DESC ) z
				WHERE 0=0 $sWhere $sOrder $sLimit";

		//echo $sql; exit;
		$result = $this->db->query($sql);
		$result = $result->result();
		return $result;
	}
}

?>