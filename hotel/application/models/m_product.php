<?php

class M_product extends CI_Model {
	
	private $table = 'products', $id = 'id' ;

	public function get_total($aColumns,$sWhere){

		$result= $this->db->query("SELECT COUNT(*) TOTAL FROM (
		SELECT p.*, a.`airlineName`, b.`cityName`, c.`categoryName`, CONCAT(p.`noofdays`,'D',p.`noofnights`,'N') AS days 
		FROM `products` p
		INNER JOIN `ms_airline` a ON a.`id`=p.`airlineID`
		INNER JOIN `ms_departure` b ON b.`id`=p.`departureID`
		INNER JOIN `ms_category` c ON c.`id`=p.`categoryID`) z WHERE 0=0 $sWhere");
		foreach ($result->result() as $row) {
			$total = $row->TOTAL;
		}
		return $total;
	}

	public function get_data($sLimit,$sWhere,$sOrder,$aColumns){
		$sql = "SELECT * FROM (
		SELECT p.*, a.`airlineName`, b.`cityName`, c.`categoryName`, CONCAT(p.`noofdays`,'D',p.`noofnights`,'N') AS days 
		FROM `products` p
		INNER JOIN `ms_airline` a ON a.`id`=p.`airlineID`
		INNER JOIN `ms_departure` b ON b.`id`=p.`departureID`
		INNER JOIN `ms_category` c ON c.`id`=p.`categoryID`) z
		WHERE 0=0 $sWhere $sOrder $sLimit";

		//echo $sql; exit;
		$result = $this->db->query($sql);
		$result = $result->result();
		return $result;
	}

	function del($data){
		$this->db->where($this->id,$data);
        $this->db->update($this->table, array('flag' => 1));
        if ($this->db->affected_rows() == '1') {
		    return TRUE;
		} else {
		    // any trans error?
		    if ($this->db->trans_status() === FALSE) {
		        return false;
		    }
		    return true;
		}
	}

	function add($data){
		$this->db->insert($this->table, $data);
		if($this->db->affected_rows()){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}

	function add_depdate($data,$id){
		$array =array();
		for($i=0;$i<count($data);$i++){
			if($data[$i]!=''){
				//depdate|allotment|remarks|dpAmount|patt|pas|pcwa|pcwb|pcnb|surcharge|surcharge2|discount|discount2
				$pecah_data = explode("|", $data[$i]);
				$pecah_depdate = explode("-", $pecah_data[0]); //dd-mm-yyyy
				$array['depdate'] 		= $pecah_depdate[2].'-'.$pecah_depdate[1].'-'.$pecah_depdate[0];
				$array['allotment'] 	= $pecah_data[1];
				$array['remarks']		= mysql_real_escape_string($pecah_data[2]);
				$array['dpAmount']		= $pecah_data[3];
				$array['patt']			= $pecah_data[4];
				$array['pas']			= $pecah_data[5];
				$array['pcwa']			= $pecah_data[6];
				$array['pcwb']			= $pecah_data[7];
				$array['pcnb']			= $pecah_data[8];
				$array['surcharge']		= $pecah_data[9];
				$array['surcharge2']	= $pecah_data[10];
				$array['discount']		= $pecah_data[11];
				$array['discount2']		= $pecah_data[12];
				$array['flag']			= '0';

				$array['productID']		= $id;
				$this->db->insert('products_date', $array);
			}
		}

	}

	/*== Edit depdate 11/11==*/
	function edit_depdate($data,$id,$data_hapus){
		$array = array();
		for($j=0;$j<count($data_hapus);$j++){
			if($data_hapus[$j]!=''){
				$array['flag']	= '1';
				$this->db->where('id',$data_hapus[$j]);
        		$this->db->update('products_date', $array);
			}
		}

		$array =array();
		for($i=0;$i<count($data);$i++){
			if($data[$i]!=''){
				$pecah_data = explode("|", $data[$i]);
				if($pecah_data[13]!=0){
					//Jika id bukan 0 berarti update
					$pecah_depdate = explode("-", $pecah_data[0]); //dd-mm-yyyy
					$array['depdate'] 		= $pecah_depdate[2].'-'.$pecah_depdate[1].'-'.$pecah_depdate[0];
					$array['allotment'] 	= $pecah_data[1];
					$array['remarks']		= mysql_real_escape_string($pecah_data[2]);
					$array['dpAmount']		= $pecah_data[3];
					$array['patt']			= $pecah_data[4];
					$array['pas']			= $pecah_data[5];
					$array['pcwa']			= $pecah_data[6];
					$array['pcwb']			= $pecah_data[7];
					$array['pcnb']			= $pecah_data[8];
					$array['surcharge']		= $pecah_data[9];
					$array['surcharge2']	= $pecah_data[10];
					$array['discount']		= $pecah_data[11];
					$array['discount2']		= $pecah_data[12];
					$array['flag']			= '0';

					$this->db->where('id',$pecah_data[13]);
        			$this->db->update('products_date', $array);
				}else{
					//Jika id 0 maka insert baru
					$pecah_depdate = explode("-", $pecah_data[0]); //dd-mm-yyyy
					$array['depdate'] 		= $pecah_depdate[2].'-'.$pecah_depdate[1].'-'.$pecah_depdate[0];
					$array['allotment'] 	= $pecah_data[1];
					$array['remarks']		= $pecah_data[2];
					$array['dpAmount']		= $pecah_data[3];
					$array['patt']			= $pecah_data[4];
					$array['pas']			= $pecah_data[5];
					$array['pcwa']			= $pecah_data[6];
					$array['pcwb']			= $pecah_data[7];
					$array['pcnb']			= $pecah_data[8];
					$array['surcharge']		= $pecah_data[9];
					$array['surcharge2']	= $pecah_data[10];
					$array['discount']		= $pecah_data[11];
					$array['discount2']		= $pecah_data[12];
					$array['flag']			= '0';

					$array['productID']		= $id;
					$this->db->insert('products_date', $array);
				}
			}
		}

	}

	function add_room($data,$id){
		$array =array();
		for($i=0;$i<count($data);$i++){
			$array['roomID']		= $data[$i];
			$array['productID']		= $id;
			$this->db->insert('products_room', $array);
		}
	}

	/*== Edit room 11/11==*/
	function edit_room($data,$id){
		$this->db->where('productID', $id);
   		$this->db->delete('products_room'); 

		$array =array();
		for($i=0;$i<count($data);$i++){
			$array['roomID']	= $data[$i];
			$array['productID']	= $id;
			$this->db->insert('products_room', $array);
		}
	}

	/*== Edit product general 11/11==*/
	function edit($data){
		$this->db->trans_start();
		$this->db->where($this->id,$data[$this->id]);
        $this->db->update($this->table, $data);
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            return false;
        }else{
            return $data[$this->id];
        }
	}

	function get_departure(){
		$this->db->select('*');
        $this->db->from('ms_departure');
        $this->db->where('flag', '0'); 
        $query = $this->db->get();


        if ($query->num_rows() > 0){
        	return $query->result();
        }else{
        	return NULL;
        }
	}

	function get_airline(){
		$this->db->select('*');
        $this->db->from('ms_airline');
        $this->db->where('flag', '0'); 
        $query = $this->db->get();


        if ($query->num_rows() > 0){
        	return $query->result();
        }else{
        	return NULL;
        }		
	}
	
	function get_category(){
		$this->db->select('*');
        $this->db->from('ms_category');
        $this->db->where('flag', '0'); 
        $query = $this->db->get();


        if ($query->num_rows() > 0){
        	return $query->result();
        }else{
        	return NULL;
        }		
	}

	function get_room(){
		$this->db->select('*');
        $this->db->from('ms_room_setting');
        $this->db->where('flag', '0'); 
        $this->db->order_by("id","asc");
        $query = $this->db->get();


        if ($query->num_rows() > 0){
        	return $query->result();
        }else{
        	return NULL;
        }		
	}

	function get_depdate($id){
		$this->db->select('*');
        $this->db->from('products_date');
        $this->db->where('productID', $id); 
        $this->db->where('flag', '0'); 
        $query = $this->db->get();


        if ($query->num_rows() > 0){
        	return $query->result();
        }else{
        	return NULL;
        }
	}

	function get_product_room($id){
		$this->db->select('roomID');
        $this->db->from('products_room');
        $this->db->where('productID', $id); 
        $query = $this->db->get();

        if ($query->num_rows() > 0){
        	return $query->result();
        }else{
        	return NULL;
        }
	}

	function get_detil($id){
		$this->db->select('*');
        $this->db->from('products');
        $this->db->where('id', $id); 
        $query = $this->db->get();


        if ($query->num_rows() > 0){
        	return $query->row(); //Only 1 data
        }else{
        	return NULL;
        }
	}

	function get_detil_wjoin($id){
		$this->db->select('products.*,ms_departure.cityName,ms_airline.airlineName,ms_category.categoryName');
        $this->db->from('products');
        $this->db->join('ms_departure', 'products.departureID = ms_departure.id');
        $this->db->join('ms_category', 'products.categoryID = ms_category.id');
        $this->db->join('ms_airline', 'products.airlineID = ms_airline.id');
        $this->db->where('products.id', $id); 
        $query = $this->db->get();

        if ($query->num_rows() > 0){
        	return $query->row(); //Only 1 data
        }else{
        	return NULL;
        }
	}

	function edit_seat($data){
		$this->db->where('id',$data[$this->id]);
        $this->db->update('products_date', $data);
        if ($this->db->affected_rows() == '1') {
		    return TRUE;
		} else {
		    // any trans error?
		    if ($this->db->trans_status() === FALSE) {
		        return false;
		    }
		    return true;
		}
	}

	/*== GET ALL PRODUCT BY AGENT (SEAT AGENT) 23/11 ==*/
	function allproductbyagent($agentID){
        $this->db->select('booking.id, booking.createdDate, booking.bookingCode, products.productName, ms_departure.cityName, ms_agent.userID as agentName, products_date.depdate, booking_detail.glname, booking_detail.gfname, booking_detail.gtittle, COUNT(booking_detail.`id`) AS guesttotal, booking.bookingStatus, ms_agent_staff.staffUsername');
        $this->db->from('booking');
        $this->db->join('booking_detail','booking_detail.bookingID = booking.id');
        $this->db->join('products','products.id = booking.productID');
        $this->db->join('products_date','products_date.id = booking.depdateID');
        $this->db->join('ms_departure','ms_departure.id = products.departureID');
        $this->db->join('ms_agent','ms_agent.id = booking.agentID');
        $this->db->join('ms_agent_staff','ms_agent_staff.id = booking.createdBy');
        $this->db->where('booking.agentID', $agentID); 
        $this->db->group_by('booking.id'); 
        $this->db->order_by('products.productName', 'asc'); 
        $this->db->order_by('products_date.depdate', 'desc'); 
        $query = $this->db->get();

        if ($query->num_rows() > 0){
        	return $query->result(); //Only 1 data
        }else{
        	return NULL;
        }
	}
}

?>