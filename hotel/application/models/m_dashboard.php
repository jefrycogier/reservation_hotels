<?php

class m_dashboard extends CI_Model {
	
	function get_lastbooking(){
        $this->db->select('booking.id, booking.createdDate, booking.bookingCode, products.productName, ms_departure.cityName, ms_agent.userID as agentName, products_date.depdate, booking_detail.glname, booking_detail.gfname, booking_detail.gtittle, COUNT(booking_detail.`id`) AS guesttotal, booking.bookingStatus, ms_agent_staff.staffUsername');
        $this->db->from('booking');
        $this->db->join('booking_detail','booking_detail.bookingID = booking.id');
        $this->db->join('products','products.id = booking.productID');
        $this->db->join('products_date','products_date.id = booking.depdateID');
        $this->db->join('ms_departure','ms_departure.id = products.departureID');
        $this->db->join('ms_agent','ms_agent.id = booking.agentID');
        $this->db->join('ms_agent_staff','ms_agent_staff.id = booking.createdBy');
        $this->db->group_by('booking.id'); 
        $this->db->order_by('booking.id', 'desc'); 
        $this->db->limit(10);

        $query = $this->db->get();
        if ($query->num_rows() > 0){
                return $query->result();
        }else{
                return NULL;
        }

	}

}

?>