<?php

class m_user extends CI_Model {
	
	private $table = 'ms_admin', $id = 'id' ;

	public function get_total($aColumns,$sWhere){
		$result= $this->db->query("SELECT COUNT(*) TOTAL FROM $this->table z WHERE 0=0 $sWhere");
		foreach ($result->result() as $row) {
			$total = $row->TOTAL;
		}
		return $total;
	}
        
        function email_valid($email){
            $query= $this->db->query("SELECT * FROM ms_admin WHERE email = '".$email."'");
            //$result = $query->row();
            return $query;
            
	}

	public function get_data($sLimit,$sWhere,$sOrder,$aColumns){
		/*
		$sql    = "SELECT ".implode($aColumns,',').", (SELECT COUNT(*) FROM `m_foto_a` b WHERE a.`id`=b.`aktifitasID`) AS jml FROM $this->table 
			INNER JOIN `users` c ON a.`staffID`=c.`id` 
			WHERE 0=0 $sWhere $sOrder $sLimit";
		*/

		$sql = "SELECT * FROM $this->table z WHERE 0=0 $sWhere $sOrder $sLimit";

		//echo $sql; exit;
		$result = $this->db->query($sql);
		$result = $result->result();
		return $result;
	}

	function del($data){
		/*
		$this->db->where($this->id, $data);
		$this->db->delete($this->table);
		if($this->db->affected_rows()){ 
			return true;
		}else{
			return false;
		}
		*/
		$this->db->where($this->id,$data[$this->id]);
        $this->db->update($this->table, $data);
        if ($this->db->affected_rows() == '1') {
		    return TRUE;
		} else {
		    // any trans error?
		    if ($this->db->trans_status() === FALSE) {
		        return false;
		    }
		    return true;
		}
	}

	function add($data){
		/*
		$last_id = $this->db->select($this->id)->limit("1")->order_by($this->id,"desc")->get($this->table)->result_array();
        
        if ($last_id) {
        	$data[$this->id] =$last_id[0][$this->id]+1;
        }else{
        	$data[$this->id] =1;
        }
        */

		$this->db->insert($this->table, $data);
		if($this->db->affected_rows()){
			return true;
		}else{
			return false;
		}
	}

	function edit($data){
		$this->db->where($this->id,$data[$this->id]);
        $this->db->update($this->table, $data);
        if ($this->db->affected_rows() == '1') {
		    return TRUE;
		} else {
		    // any trans error?
		    if ($this->db->trans_status() === FALSE) {
		        return false;
		    }
		    return true;
		}
		/*
        if($this->db->affected_rows()){
            return true;
        }
        else{
            return false;
        }
        */
	}
}

?>