<?php

class m_roomlist extends CI_Model {
	
	private $table = 'products', $id = 'id' ;

	public function get_total($aColumns,$sWhere){

		$result = $this->db->query("SELECT COUNT(*) TOTAL FROM(
				SELECT products.`productName`, products_date.`depdate`, products_date.`id`, COUNT(booking_detail.id) AS paxs 
				FROM products_date
				INNER JOIN products ON products_date.`productID`=products.`id`
				INNER JOIN `booking` ON `booking`.`depdateID` = `products_date`.`id`
				INNER JOIN `booking_detail` ON `booking_detail`.`bookingID` = `booking`.`id`
				WHERE products.`flag`=0 AND `booking`.`bookingStatus` < '6' 
				GROUP BY `products_date`.`id` 
				HAVING COUNT(booking_detail.id) > 0 
				ORDER BY `paxs` DESC
				) z WHERE 0=0 $sWhere");

		foreach ($result->result() as $row) {
			$total = $row->TOTAL;
		}
		return $total;
	}

	public function get_data($sLimit,$sWhere,$sOrder,$aColumns){
		$sql = "SELECT * FROM(
				SELECT products.`productName`, products_date.`depdate`, products_date.`id`, COUNT(booking_detail.id) AS paxs 
				FROM products_date
				INNER JOIN products ON products_date.`productID`=products.`id`
				INNER JOIN `booking` ON `booking`.`depdateID` = `products_date`.`id`
				INNER JOIN `booking_detail` ON `booking_detail`.`bookingID` = `booking`.`id`
				WHERE products.`flag`=0 AND `booking`.`bookingStatus` < '6' 
				GROUP BY `products_date`.`id` 
				HAVING COUNT(booking_detail.id) > 0 
				ORDER BY `paxs` DESC
				) z WHERE 0=0 $sWhere $sOrder $sLimit";

		//echo $sql; exit;
		$result = $this->db->query($sql);
		$result = $result->result();
		return $result;
	}

	function alllist($depdateID){
		$this->db->select('*');
        $this->db->from('booking_detail');
        $this->db->join('booking','booking.id=booking_detail.bookingID');
        $this->db->join('ms_agent','booking.agentID=ms_agent.id');
        $this->db->where('booking.depdateID', $depdateID); 
        $this->db->where('booking.bookingStatus <', '6'); 
        $this->db->order_by('booking.createdDate','asc');
        $query = $this->db->get();

        if ($query->num_rows() > 0){
        	return $query->result();
        }else{
        	return NULL;
        }
	}

	function alllist_one($depdateID){
		$this->db->select('products.productName,products_date.depdate');
        $this->db->from('booking_detail');
        $this->db->join('booking','booking.id=booking_detail.bookingID');
        $this->db->join('products','products.id=booking.productID');
        $this->db->join('products_date','products.id=products_date.productID');
        $this->db->where('booking.depdateID', $depdateID); 
        $this->db->where('booking.bookingStatus <', '6'); 
        $this->db->order_by('booking.createdDate','asc');
        $query = $this->db->get();

        if ($query->num_rows() > 0){
        	return $query->row();
        }else{
        	return NULL;
        }
	}
}

?>