<?php

class m_term extends CI_Model {
	
	private $table = 'ms_term_confirmation', $id = 'id' ;

	public function get_total($aColumns,$sWhere){
		$result= $this->db->query("SELECT COUNT(*) TOTAL FROM $this->table z WHERE 0=0 $sWhere");
		foreach ($result->result() as $row) {
			$total = $row->TOTAL;
		}
		return $total;
	}

	public function get_data($sLimit,$sWhere,$sOrder,$aColumns){
		$sql = "SELECT * FROM $this->table z WHERE 0=0 $sWhere $sOrder $sLimit";

		$result = $this->db->query($sql);
		$result = $result->result();
		return $result;
	}

	function del($data){
		$this->db->where($this->id,$data[$this->id]);
        $this->db->update($this->table, $data);
        if ($this->db->affected_rows() == '1') {
		    return TRUE;
		} else {
		    // any trans error?
		    if ($this->db->trans_status() === FALSE) {
		        return false;
		    }
		    return true;
		}
	}

	function add($data){
		$this->db->insert($this->table, $data);
		if($this->db->affected_rows()){
			return true;
		}else{
			return false;
		}
	}

	function edit($data){
		$this->db->where($this->id,$data[$this->id]);
        $this->db->update($this->table, $data);
        if ($this->db->affected_rows() == '1') {
		    return TRUE;
		} else {
		    // any trans error?
		    if ($this->db->trans_status() === FALSE) {
		        return false;
		    }
		    return true;
		}
	}

	function sortno(){
		$this->db->select('sortby');
		$this->db->from('ms_term_confirmation');
		$this->db->where('flag','0');
		$this->db->order_by('sortby', 'desc');
		$this->db->limit('1');
		$query = $this->db->get(); 
		return $query->row();
	}

	function idsortno($id){
		$this->db->select('sortby');
		$this->db->from('ms_term_confirmation');
		$this->db->where('id',$id);
		$query = $this->db->get(); 
		return $query->row();
	}

	function idsortno2($sortno){
		$this->db->select('id,sortby');
		$this->db->from('ms_term_confirmation');
		$this->db->where('sortby >',$sortno);
		$query = $this->db->get(); 
		return $query->result();
	}

	function updatesortno($id,$sortno){
		$this->db->where('id',$id);
        $this->db->update($this->table, array('sortby' => $sortno));
        if ($this->db->affected_rows() == '1') {
		    return TRUE;
		} else {
		    // any trans error?
		    if ($this->db->trans_status() === FALSE) {
		        return false;
		    }
		    return true;
		}
	}

}

?>