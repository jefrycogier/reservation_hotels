<?php

class m_room extends CI_Model {
	
	private $table = 'ms_room_type', $id = 'id' ;

	public function get_total($aColumns,$sWhere){
		$result= $this->db->query("SELECT COUNT(*) TOTAL FROM $this->table z WHERE 0=0 $sWhere");
		foreach ($result->result() as $row) {
			$total = $row->TOTAL;
		}
		return $total;
	}

	public function get_data($sLimit,$sWhere,$sOrder,$aColumns){
		$sql = "SELECT * FROM $this->table z WHERE 0=0 $sWhere $sOrder $sLimit";

		$result = $this->db->query($sql);
		$result = $result->result();
		return $result;
	}

	function del($data){
		$this->db->where($this->id,$data[$this->id]);
        $this->db->update($this->table, $data);
        if ($this->db->affected_rows() == '1') {
		    return TRUE;
		} else {
		    // any trans error?
		    if ($this->db->trans_status() === FALSE) {
		        return false;
		    }
		    return true;
		}
	}

	function add($data,$data_detail){
		$this->db->insert($this->table, $data);
                $room_type_id = $this->db->insert_id();
                $start_date = $data_detail['startdate'];
                $end_date = $data_detail['enddate'];
                $start_ts = strtotime($start_date); 
                $end_ts = strtotime($end_date); 
                while ($start_ts <= $end_ts) 
                { 
                    $date_allotment = date('Y-m-d', $start_ts); 
                    $start_ts = strtotime(date('Y-m-d', $start_ts) . ' +1 day'); 
                    $sql = "INSERT INTO allotment_room (roomtypeID,allotmentdate) " .
                    "VALUES (" .
                    $this->db->escape($room_type_id) .
                    "," .
                    $this->db->escape($date_allotment) .
    //                "," .
    //                $this->db->escape($date_loop) .
                    ")";
                    $result = $this->db->query($sql);
                }  
                return $result;
		if($this->db->affected_rows()){
			return true;
		}else{
			return false;
		}
	}

	function edit($data){
		$this->db->where($this->id,$data[$this->id]);
        $this->db->update($this->table, $data);
        if ($this->db->affected_rows() == '1') {
		    return TRUE;
		} else {
		    // any trans error?
		    if ($this->db->trans_status() === FALSE) {
		        return false;
		    }
		    return true;
		}
	}
        
        function add_room_type_detail($data){
		$this->db->insert('ms_room_type_detail', $data);
		if($this->db->affected_rows()){
			return true;
		}else{
			return false;
		}
	}
        
        function insert_allotment($data,$start_date,$end_date) {        
            $id_room_type = $data[$this->id];
            $start_ts = strtotime($start_date); 
            $end_ts = strtotime($end_date); 

            var_dump($id_room_type);
            var_dump($start_ts);
            var_dump($end_ts);
            die;
            
            
            while ($start_ts <= $end_ts) 
            { 
                $date_loop = date('Y-m-d', $start_ts); 
                $start_ts = strtotime(date('Y-m-d', $start_ts) . ' +1 day'); 
                $sql = "INSERT INTO allotment_room (allotmentdate) " .
                "VALUES (" .
                $this->db->escape($date_loop) .
//                "," .
//                $this->db->escape(1) .
//                "," .
//                $this->db->escape($date_loop) .
                ")";
                $result = $this->db->query($sql);
            }  
            
            
            return $result;
        }
        
}

?>