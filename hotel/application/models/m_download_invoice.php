<?php

class m_download_invoice extends CI_Model {

        function get_booking($id){
                $this->db->select('booking.*,products_date.depdate,ms_room_setting.settingName,ms_agent.*');
                $this->db->from('booking');
                $this->db->join('products_date', 'products_date.id=booking.depdateID');
                $this->db->join('ms_room_setting', 'ms_room_setting.id=booking.roomID');
                $this->db->join('ms_agent', 'ms_agent.id=booking.agentID');
                $this->db->where('booking.id', $id); 
                $query = $this->db->get();


                if ($query->num_rows() > 0){
                        return $query->row();
                }else{
                        return NULL;
                }
        }

        function get_generate($id){
                $this->db->select('*');
                $this->db->from('generate');
                $this->db->where('id', $id); 
                $query = $this->db->get();

                if ($query->num_rows() > 0){
                        return $query->row();
                }else{
                        return NULL;
                }
        }

        function get_total_guest($id){
                $this->db->select('COUNT(*) as total');
                $this->db->from('booking_detail');
                $this->db->where('bookingID', $id); 
                $query = $this->db->get();


                if ($query->num_rows() > 0){
                        return $query->row();
                }else{
                        return NULL;
                }
        }

        function get_guest($id){
                $this->db->select('*');
                $this->db->from('booking_detail');
                $this->db->where('bookingID', $id); 
                $query = $this->db->get();


                if ($query->num_rows() > 0){
                        return $query->result();
                }else{
                        return NULL;
                }
        }

        function get_product($id){
                $this->db->select('products.*,ms_departure.cityName,ms_airline.airlineName,ms_category.categoryName');
                $this->db->from('products');
                $this->db->join('ms_departure', 'products.departureID = ms_departure.id');
                $this->db->join('ms_category', 'products.categoryID = ms_category.id');
                $this->db->join('ms_airline', 'products.airlineID = ms_airline.id');
                $this->db->where('products.id', $id); 
                $query = $this->db->get();

                if ($query->num_rows() > 0){
                        return $query->row(); //Only 1 data
                }else{
                        return NULL;
                }
        }

        function get_product_date($id){
                $this->db->select('*');
                $this->db->from('products_date');
                $this->db->where('id', $id); 
                $query = $this->db->get();

                if ($query->num_rows() > 0){
                        return $query->row(); //Only 1 data
                }else{
                        return NULL;
                }
        }
        
        function get_total_adult($id){
                $this->db->select('COUNT(*) as TOTAL');
                $this->db->from('booking_detail');
                $this->db->where('bookingID', $id);
                $this->db->where('gtype <', '2');
                $query = $this->db->get();

                if ($query->num_rows() > 0){
                        return $query->row(); //Only 1 data
                }else{
                        return NULL;
                }
        }

        function get_total_child($id){
                $this->db->select('COUNT(*) as TOTAL');
                $this->db->from('booking_detail');
                $this->db->where('bookingID', $id);
                $this->db->where('gtype >', '1');
                $query = $this->db->get();

                if ($query->num_rows() > 0){
                        return $query->row(); //Only 1 data
                }else{
                        return NULL;
                }
        }
        
        function get_term(){
                $this->db->select('*');
                $this->db->from('ms_term_confirmation');
                $this->db->where('flag', '0');
                        $this->db->order_by('sortby','asc');
                $query = $this->db->get();

                if ($query->num_rows() > 0){
                        return $query->result();
                }else{
                        return NULL;
                }
        }

        function get_bank($id){
                $this->db->select('*');
                $this->db->from('ms_bank');
                $this->db->where('ms_bank.id', $id); 
                $query = $this->db->get();

                if ($query->num_rows() > 0){
                        return $query->row();
                }else{
                        return NULL;
                }
        }
}

?>