<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class term extends CI_Controller{
	function __construct() {
		parent::__construct();
		$this->load->model('m_term');
	}

	function index() {
        if($this->session->userdata('id')){
            $session_data           = $this->session->userdata('adminName');
            $data['adminName']      = $session_data;
            $data['title']          = 'Dashboard Administrator';
            $this->load->view('v_term', $data);
        }else{
            //If no session, redirect to login page
            redirect('logout');
        }
	}

	function get_data() {
		$aColumns = array('z.id', 'z.sortby', 'z.content');
        $sSearch =  $this->input->post('sSearch',true); 
        $sWhere = "";
        if (isset($sSearch) && $sSearch != "") {
            $sWhere = "AND (";
            for ( $i = 0 ; $i < count($aColumns) ; $i++ ) {
                if($i==1||$i==2){
                    $sWhere .= " ".$aColumns[$i]." LIKE '%".($sSearch)."%' OR ";
                }
            }
            $sWhere = substr_replace( $sWhere, "", - 3 );
            $sWhere .= ') AND z.flag="0"';
        }else{
            $sWhere = ' AND z.flag="0"';
        }
        //echo $sWhere; exit();
		//filter indovidual create by rizal 14/09/2015
		for ($i=0 ; $i<count($aColumns) ; $i++){
			if ($this->input->post('bSearchable_'.$i) == "true" && $this->input->post('sSearch_'.$i) != '' ){
				$sWhere .= " AND ".$aColumns[$i]." LIKE '%".$this->input->post('sSearch_'.$i)."%' ";
			}
		}
		
        $iTotalRecords  = $this->m_term->get_total($aColumns,$sWhere);
        $iDisplayLength = intval($this->input->post('iDisplayLength',true));
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($this->input->post('iDisplayStart',true));
        
        // $sEcho          = intval($_REQUEST['sEcho']);
        $iSortCol_0     = $this->input->post('iSortCol_0',true);
        
        $records = array();
        $records["aaData"] = array();
        $sOrder = "";
        if (isset($iDisplayStart) && $iDisplayLength != '-1' ) {
            $sLimit = "limit ".intval($iDisplayLength)." OFFSET ".intval( $iDisplayStart );
        }

        if (isset($iSortCol_0)) {
            $sOrder = "ORDER BY  ";
            for ( $i = 0 ; $i < intval($this->input->post('iSortingCols')) ; $i++ ) {
                if ( $this->input->post('bSortable_'.intval($this->input->post('iSortCol_'.$i))) == "true" ) {
                    $sOrder .= "".$aColumns[ intval($this->input->post('iSortCol_'.$i)) ]." ".
                    ($this->input->post('sSortDir_'.$i) === 'asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", - 2 );
            if ( $sOrder == "ORDER BY" ) {
                $sOrder = "";
            }
        }

        $data = $this->m_term->get_data($sLimit,$sWhere,$sOrder,$aColumns); 
		//echo $this->db->last_query();
        $no   = 1 + $iDisplayStart;
        foreach ($data as $row) {
            $pars_data = "$row->id|$row->content|$row->sortby";

            $action = '
            <a href="javascript:void(0)" onclick="set_val(\''.$pars_data.'\')" class="btn btn-xs btn-warning" title="Edit">
                <i class="fa fa-pencil"></i>
            </a>
            <a href="javascript:void(0)" onclick="set_del(\''.$row->id.'\')" class="btn btn-xs btn-danger" title="Delete">
                <i class="fa fa-trash-o"></i>
            </a>';

            $records["aaData"][] = array(
            		$no,
                    $row->sortby, 
                    $row->content, 
                    $action
            );
            $no++;
        }

        //$records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;

        echo json_encode($records);

	}

	public function save(){
        $sql	= true;

        $session= $this->session->userdata('id');
        $act 	= $this->input->post('act', true);

        $sortno = $this->m_term->sortno();
        if($sortno->sortby==null){
            $sortno = 1;
        }else{
            $sortno = $sortno->sortby+1;
        }

        $data=array(
            //'id'  	        => $this->input->post('id', true),
            'content'      => $this->input->post('content', true)
        );

        if($act == 'add'){
            $data["flag"]   = '0';
            $data["sortby"] = $sortno;
            $sql            = $this->m_term->add($data);
        }elseif($act == 'edit'){
            $data["id"]     = $this->input->post('id', true);
            $sql            = $this->m_term->edit($data);
        }

        if($sql == true){
            echo 'true';
        } 
        else { 
            echo 'false'; 
        }
	}
	
	public function delete(){
        $session        = $this->session->userdata('id');
        $id             = $this->input->post('id', true);

        $data["id"]     = $this->input->post('id', true);
        $data["flag"]   = '1';

        $idsortno = $this->m_term->idsortno($id); //Get sortno id yang akan di hapus
        $idsortno2 = $this->m_term->idsortno2($idsortno->sortby); //Get semua id yang memiliki sortno lebih besar dari $idsortno
        if(count($idsortno2)>0){
            foreach ($idsortno2 as $value) {
                $updateafter    = $value->sortby-1;
                $updatesortno   = $this->m_term->updatesortno($value->id,$updateafter);
            }
        }

        $sql = $this->m_term->del($data);
        //echo $this->db->last_query();
        if($sql == true ){		
            echo 'true';
        }else{
            echo 'false';
        }
	}

}

?>