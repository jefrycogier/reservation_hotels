<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class login extends CI_Controller {
	
	public function index(){
		$this->home();
	}

	public function home(){
		if ($this->session->userdata('id')==''){
			$data['title']			= 'Login Page';
			$this->load->view('v_login',$data);
		}else{					
			redirect('room_type');
		}
	}

	public function login_proccess(){
		$this->load->helper(array('form', 'url', 'array'));
		$this->load->library('form_validation');
		$this->load->model('m_login');

		$this->form_validation->set_rules('userID', 'Username', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[4]');

		if($this->form_validation->run()==FALSE){
			$data['title']		= 'Login Page';
			$this->load->view('v_login',$data);
		}else{
			$data = array(
				'userID'		=> $this->input->post('userID'),
				'password'		=> $this->input->post('password'),
				'flag'			=> 0
			);
			$login_authentication = $this->m_login->login_authentication($data);
//echo $this->db->last_query(); exit;

			if ($login_authentication->num_rows() == 1) {
		        foreach ($login_authentication->result() as $sess) {
		        	$sess_data['id'] 			= $sess->id;
		        	$sess_data['adminName'] 	= $sess->adminName;
		        	$sess_data['userID'] 		= $sess->userID;
                                $sess_data['position'] 		= $sess->position;
		        	$this->session->set_userdata($sess_data);          
		    	}
		        redirect('login');    
		    }else {
		        echo "<script>alert('Gagal login: Cek username, password!');history.go(-1);</script>";
		    }

			/*
			$data['title'] = 'Dashboard';
			$data['page_header'] = 'Order Success!';
			$this->load->view('v_home', $data);
			if ($this->session->userdata('logged_in')==''){
				$data['title']			= 'Login Page';
				$data['page_header'] 	= 'Login Page';
				$this->load->view('v_login',$data);
			}else{
				$data['title'] = 'Dashboard';
				$data['page_header'] = 'Order Success!';
				$this->load->view('v_home', $data);
			}
			*/
		}
	}
        
        public function reset_proccess(){
            $this->load->helper(array('form', 'url', 'array'));
            $this->load->library('form_validation');
            $this->load->model('m_login');

            $data = array(
                            'email' => $this->input->post('email'),				
                    );
            
            $email = $this->input->post('email');
            $reset_authentication = $this->m_login->reset_authentication($email);

            if ($reset_authentication->num_rows() == 1) {
                $this->load->view('v_form_password',$data);         
            }else {
                echo "<script>alert('email dont match in data.');history.go(-1);</script>";
            }
		
	}
        
        public function reset_pass(){
            $this->load->helper(array('form', 'url', 'array'));
            $this->load->library('form_validation');
            $this->load->model('m_login');

            $email = $this->input->post('email');
            $query = $this->db->query('SELECT * FROM ms_admin WHERE email="'.$email.'"');
            $row = $query->row();
            $name = $row->adminName;
            $data = array(
                            'name' => $name,				
                            'email' => $this->input->post('email'),				
                    );
            
            $reset_authentication = $this->m_login->reset_authentication($email);
            
            if ($reset_authentication->num_rows() == 1) {
                $subject = 'Konfirmasi perubahan kata sandi untuk '.$name.'';

                $message = 'Kepada Pelanggan Yth '.$name.','."\n";
                $message .= 'Kami mendapatkan permintaan untuk mereset ulang kata sandi akun anda.'."\n";
                $message .= 'klik link berikut '.site_url() . 'login/form_password?email=' . $email .' untuk mengganti password baru anda'."\n";
                $message .= 'salam hangat, '."\n";
                $message .= 'Hotel reservation'."\n";
                $sender_email = 'jefrymiftakhul@gmail.com';
                $sender_password = 'peterpan7';

                            // Configure email library
                $config['protocol'] = 'smtp';
                $config['smtp_host'] = 'ssl://smtp.googlemail.com';
                $config['smtp_port'] = 465;
                $config['smtp_user'] = $sender_email;
                $config['smtp_pass'] = $sender_password;

                // Load email library and passing configured values to email library 
                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");

                // Sender email address
                $this->email->from("noreply@hotelreservation.com","Hotel Reservation");
                //$this->email->from($sender_email);
                // Receiver email address
                $this->email->to($email);
                // Subject of email
                $this->email->subject($subject);
                // Message in email
                $this->email->message($message);

                if ($this->email->send()) {
                    echo "<script>
                        alert('Konfirmasi perubahan password sudah dikirim ke email anda');
                        window.location.href = 'login';
                    </script>";
                } else {
                    echo "<script>alert('Wrong email. Please try again.');history.go(-1);</script>";
                }
                //$this->load->view('v_form_password',$data);         
            }else {
                echo "<script>alert('email dont match in data.');history.go(-1);</script>";
            }
		
	}
        
        public function reset_password(){
		$this->load->view('v_reset');
	}
        public function form_password(){
           
            $data = array(
                    'email' => $_GET['email'],				
                    );
            $this->load->view('v_form_password',$data);  
	}
        public function save_password(){
           $email =  $this->input->post('email');
           $password =  $this->input->post('newpassword');
           
           $updateData=array("password"=>$password);

            $this->db->where("email",$email);
            $this->db->update("ms_admin",$updateData);
            echo "<script>
                    alert('Password anda berhasil diubah ... silakan login dengan password baru anda');
                    window.location.href = 'login';
                </script>";
//            echo "<script>alert('Password anda berhasil diubah ... silakan login dengan password baru anda');</script>";
//            redirect('login');
          
	}

	public function send_email(){//not used
            $name =  $this->input->post('name');
            $email =  $this->input->post('email');
            $password =  $this->input->post('newpassword');
            $subject = 'Konfirmasi perubahan kata sandi untuk '.$name.'';

            $message = 'Kepada Pelanggan Yth '.$name.','."\n";
            $message .= 'Kami mendapatkan permintaan untuk mereset ulang kata sandi akun anda.'."\n";
            $message .= 'Password baru anda adalah : '.$password.''."\n";
            $message .= 'salam hangat, '."\n";
            $message .= 'Hotel reservation'."\n";
            $message .= 'klik link berikut '.site_url() . 'login/form_password?email=' . $email;
            $sender_email = 'jefrymiftakhul@gmail.com';
            $sender_password = 'peterpan7';
		
			// Configure email library
            $config['protocol'] = 'smtp';
            $config['smtp_host'] = 'ssl://smtp.googlemail.com';
            $config['smtp_port'] = 465;
            $config['smtp_user'] = $sender_email;
            $config['smtp_pass'] = $sender_password;

            // Load email library and passing configured values to email library 
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            
            // Sender email address
            $this->email->from("noreply@hotelreservation.com","Hotel Reservation");
            //$this->email->from($sender_email);
            // Receiver email address
            $this->email->to($email);
            // Subject of email
            $this->email->subject($subject);
            // Message in email
            $this->email->message($message);

            if ($this->email->send()) {
                $updateData=array("password"=>$password);

                $this->db->where("email",$email);
                $this->db->update("ms_admin",$updateData);
                redirect('login');
            } else {
                echo "<script>alert('Wrong email. Please try again.');history.go(-1);</script>";
            }

	}
        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */