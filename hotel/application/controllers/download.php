<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class download extends CI_Controller{
    function __construct() {
        parent::__construct();
        $this->load->model('m_download_invoice');
    }

    public function invoicedeposit(){ 
		$this->load->model('m_download_invoice');
		$id = $this->uri->segment(3);

		$data['data_booking'] 	= $this->m_download_invoice->get_booking($id);
		$productID 				= $data['data_booking']->productID;
		$data['data_product'] 	= $this->m_download_invoice->get_product($productID);
		$data['data_total_guest'] = $this->m_download_invoice->get_total_guest($id);
		$data['data_guest'] 	= $this->m_download_invoice->get_guest($id);
		$data['data_adult'] 	= $this->m_download_invoice->get_total_adult($id);
		$data['data_child'] 	= $this->m_download_invoice->get_total_child($id);
		$depdateID 				= $data['data_booking']->depdateID;
		$data['data_product_date'] 	= $this->m_download_invoice->get_product_date($depdateID);
		$data['data_term']		= $this->m_download_invoice->get_term();
		//load mPDF library
		$this->load->library('m_pdf');
		
		$html=$this->load->view('v_download',$data, true); //load the pdf_output.php by passing our data and get all data in $html varriable.
 	 
		//this the the PDF filename that user will get to download
		$pdfFilePath = 'INVOICE-DEPOSIT-'.$data['data_booking']->bookingCode.'.pdf';

		//actually, you can pass mPDF parameter on this load() function
		$pdf = $this->m_pdf->load();
		//generate the PDF!
		$pdf->WriteHTML($html,2);
		//offer it to user via browser download! (The PDF won't be saved on your server HDD)
		$pdf->Output($pdfFilePath, "D");

	} 
	
	public function confirmation(){ 
		$this->load->model('m_download_invoice');
		$id = $this->uri->segment(3);

		$data['data_booking'] 	= $this->m_download_invoice->get_booking($id);
		$productID 				= $data['data_booking']->productID;
		$data['data_product'] 	= $this->m_download_invoice->get_product($productID);
		$data['data_total_guest'] = $this->m_download_invoice->get_total_guest($id);
		$data['data_guest'] 	= $this->m_download_invoice->get_guest($id);
		$data['data_adult'] 	= $this->m_download_invoice->get_total_adult($id);
		$data['data_child'] 	= $this->m_download_invoice->get_total_child($id);
		$depdateID 				= $data['data_booking']->depdateID;
		$data['data_product_date'] 	= $this->m_download_invoice->get_product_date($depdateID);
		$data['data_term']		= $this->m_download_invoice->get_term();
		//load mPDF library
		$this->load->library('m_pdf');
		
		$html=$this->load->view('v_download',$data, true); //load the pdf_output.php by passing our data and get all data in $html varriable.
 	 
		//this the the PDF filename that user will get to download
		$pdfFilePath = 'TOUR-CONFIRMATION-'.$data['data_booking']->bookingCode.'.pdf';

		//actually, you can pass mPDF parameter on this load() function
		$pdf = $this->m_pdf->load();
		//generate the PDF!
		$pdf->WriteHTML($html,2);
		//offer it to user via browser download! (The PDF won't be saved on your server HDD)
		$pdf->Output($pdfFilePath, "D");

	} 

	public function generate(){ 
		$this->load->model('m_download_invoice');
		$id = $this->uri->segment(3);

		$data['data_generate']	= $this->m_download_invoice->get_generate($id);
		$bookingID 				= $data['data_generate']->bookingID;
		$data['data_booking'] 	= $this->m_download_invoice->get_booking($bookingID);
		$productID 				= $data['data_booking']->productID;
		$data['data_product'] 	= $this->m_download_invoice->get_product($productID);
		$data['data_total_guest'] = $this->m_download_invoice->get_total_guest($bookingID);
		$data['data_guest'] 	= $this->m_download_invoice->get_guest($bookingID);
		$data['data_adult'] 	= $this->m_download_invoice->get_total_adult($bookingID);
		$data['data_child'] 	= $this->m_download_invoice->get_total_child($bookingID);
		$depdateID 				= $data['data_booking']->depdateID;
		$data['data_product_date'] 	= $this->m_download_invoice->get_product_date($depdateID);
		$data['data_term']		= $this->m_download_invoice->get_term();
		$data['data_bank']		= $this->m_download_invoice->get_bank(1);
		
		//load mPDF library
		$this->load->library('m_pdf');
		
		$html=$this->load->view('v_download_invoice',$data, true); //load the pdf_output.php by passing our data and get all data in $html varriable.
 	 
		//this the the PDF filename that user will get to download
		$pdfFilePath = 'FINAL-PAYMENT-INVOICE-'.$data['data_booking']->bookingCode.'.pdf';

		//actually, you can pass mPDF parameter on this load() function
		$pdf = $this->m_pdf->load();
		//generate the PDF!
		$pdf->WriteHTML($html,2);
		//offer it to user via browser download! (The PDF won't be saved on your server HDD)
		$pdf->Output($pdfFilePath, "D");

	} 

}

?>