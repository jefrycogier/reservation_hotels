<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class dashboard extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('m_dashboard');
	}

	
	public function index(){
		$this->home();
	}

	public function home(){
		if($this->session->userdata('id')){
			$this->all();
		}else{
			//If no session, redirect to login page
			redirect('logout');
		}
	}

	public function all(){
		$session_data = $this->session->userdata('adminName');
		$data['adminName'] 		= $session_data;
		$data['title'] 			= 'Dashboard Administrator';
		$data['list_booking'] = $this->m_dashboard->get_lastbooking();
		//echo $this->db->last_query();
		$this->load->view('v_dashboard', $data);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */