<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class booking extends CI_Controller{
    function __construct() {
        parent::__construct();
        $this->load->model('m_product');
        $this->load->model('m_booking');
        $this->load->model('m_download_invoice');
    }

    function cancel(){
        $id     = $this->input->post('id', true);
        $sql    = $this->m_booking->cancel($id);
        if($sql == true ){      
            echo 'true';
        }else{
            echo 'false';
        }
    }

    function chengestatus(){
        $id         = $this->input->post('id', true);
        $newstatus  = $this->input->post('newstatus', true);
        $sql    = $this->m_booking->chengestatus($id,$newstatus);
        //echo $this->db->last_query(); exit;
        if($sql == true ){      
            echo 'true';
        }else{
            echo 'false';
        }
    }

    function chengestatustodpnp(){
        $id         = $this->input->post('id', true);
        $depdateID  = $this->input->post('depdateID', true);
        $sts_all   = $this->m_booking->all_allotment($depdateID);
        $sts_wl    = $this->m_booking->stock_allotment($depdateID,0);
        $sts_dpnp  = $this->m_booking->stock_allotment($depdateID,1);
        $sts_dpr   = $this->m_booking->stock_allotment($depdateID,2);
        $sts_fpnp  = $this->m_booking->stock_allotment($depdateID,3);
        $sts_fpr   = $this->m_booking->stock_allotment($depdateID,4);
        $sts_comp  = $this->m_booking->stock_allotment($depdateID,5);

        $stok_yang_akan_booking = $this->input->post('total_guest', true);
        $stok_balance = $sts_all->TOTAL-($sts_dpnp->TOTAL+$sts_dpr->TOTAL+$sts_fpnp->TOTAL+$sts_fpr->TOTAL+$sts_comp->TOTAL);
        $stok_sementara = $stok_balance-$stok_yang_akan_booking;
        
        if($stok_sementara>=0){
            //echo "STOK AVAILABLE";
            $bookingStatus = '1';
            $sql    = $this->m_booking->chengestatus($id,1);
            if($sql == true ){      
                echo 'true';
            }else{
                echo 'false';
            }
        }else{
            //echo "STOK NOT AVAILABLE";
            $bookingStatus = '0';
            echo 'notavailable';
        }
    }

    function generate(){ 
        $sql    = true;
        $data = array(
            'bookingID'     => $this->input->post('bookingID', true),
            'tourPrice'     => $this->input->post('tourPrice', true),
            'totTourPrice'  => $this->input->post('totTourPrice', true),
            'totDP'         => $this->input->post('totDP', true),
            'addCharge'     => $this->input->post('addCharge', true),
            'addChargePrice'    => $this->input->post('addChargePrice', true),
            'addCharge2'    => $this->input->post('addCharge2', true),
            'addChargePrice2'   => $this->input->post('addChargePrice2', true),
            'grandTotal'    => $this->input->post('grandTotal', true)
        );

        $sql   = $this->m_booking->add_generate($data);
        if($sql != false ){      
            echo $sql;
        }else{
            echo 'false';
        }
    }
}

?>