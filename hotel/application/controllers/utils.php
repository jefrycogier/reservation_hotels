<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class utils extends CI_Controller{
	function __construct() {
		parent::__construct();
		//$this->load->model('m_booking');
        $this->load->model('m_utils');
        $this->load->helper("url");
	}

    function uploadproductfile(){
        $status = "";
        $msg    = "";
        $file_element_name = 'userfile';

        if ($status != "error")
        {
            $config['upload_path']      = './product_files/';
            $config['allowed_types']    = 'gif|jpg|png|doc|txt|pdf';
            $config['max_size']         = 1024 * 8;
            $config['encrypt_name']     = TRUE;
      
            $this->load->library('upload', $config);
      
            if (!$this->upload->do_upload($file_element_name)){
                $status = 'error';
                $msg    = $this->upload->display_errors('', '');
            }else{
                $img    = $this->upload->data();
                $status = '';
                $msg    = $img['file_name'];
            }
            @unlink($_FILES[$file_element_name]);
        }
        echo $status.'*'.$msg;
    }

    function deleteproductfile(){
        $this->load->helper("url");
        $this->load->helper("file");
        $filename = $this->input->post('filename', true);
        @unlink(base_url("product_files/".$filename));
    }

/*== THUMBNAIL 25/11 ==*/
    function uploadproductimage(){
        $status = "";
        $msg    = "";
        $file_element_name = 'userfile2';

        if ($status != "error")
        {
            $config['upload_path']      = './product_image/';
            $config['allowed_types']    = 'gif|jpg|png';
            $config['max_size']         = 1024 * 8;
            $config['encrypt_name']     = TRUE;
      
            $this->load->library('upload', $config);
      
            if (!$this->upload->do_upload($file_element_name)){
                $status = 'error';
                $msg    = $this->upload->display_errors('', '');
            }else{
                $img    = $this->upload->data();
                $status = '';
                $msg    = $img['file_name'];
            }
            @unlink($_FILES[$file_element_name]);
        }
        echo $status.'*'.$msg;
    }

    function deleteproductimage(){
        $this->load->helper("url");
        $this->load->helper("file");
        $filename = $this->input->post('filename', true);
        @unlink(base_url("product_image/".$filename));
    }

    function uploadfile1(){
        $status = "";
        $msg    = "";
        $file_element_name = 'uploadFile1';

        if ($status != "error")
        {
            $config['upload_path']      = './archive_files/';
            $config['allowed_types']    = 'gif|jpg|png|doc|txt|pdf|xls|xlsx';
            $config['max_size']         = 1024 * 8;
            $config['encrypt_name']     = TRUE;
      
            $this->load->library('upload', $config);
      
            if (!$this->upload->do_upload($file_element_name)){
                $status = 'error';
                $msg    = $this->upload->display_errors('', '');
            }else{
                $img    = $this->upload->data();
                $status = '';
                $msg    = $img['file_name'];
            }
            @unlink($_FILES[$file_element_name]);
        }
        echo $status.'*'.$msg;
    }

    function uploadfile2(){
        $status = "";
        $msg    = "";
        $file_element_name = 'uploadFile2';

        if ($status != "error")
        {
            $config['upload_path']      = './archive_files/';
            $config['allowed_types']    = 'gif|jpg|png|doc|txt|pdf|xls|xlsx';
            $config['max_size']         = 1024 * 8;
            $config['encrypt_name']     = TRUE;
      
            $this->load->library('upload', $config);
      
            if (!$this->upload->do_upload($file_element_name)){
                $status = 'error';
                $msg    = $this->upload->display_errors('', '');
            }else{
                $img    = $this->upload->data();
                $status = '';
                $msg    = $img['file_name'];
            }
            @unlink($_FILES[$file_element_name]);
        }
        echo $status.'*'.$msg;
    }

    function uploadfile3(){
        $status = "";
        $msg    = "";
        $file_element_name = 'uploadFile3';

        if ($status != "error")
        {
            $config['upload_path']      = './archive_files/';
            $config['allowed_types']    = 'gif|jpg|png|doc|txt|pdf|xls|xlsx';
            $config['max_size']         = 1024 * 8;
            $config['encrypt_name']     = TRUE;
      
            $this->load->library('upload', $config);
      
            if (!$this->upload->do_upload($file_element_name)){
                $status = 'error';
                $msg    = $this->upload->display_errors('', '');
            }else{
                $img    = $this->upload->data();
                $status = '';
                $msg    = $img['file_name'];
            }
            @unlink($_FILES[$file_element_name]);
        }
        echo $status.'*'.$msg;
    }

    function uploadfile4(){
        $status = "";
        $msg    = "";
        $file_element_name = 'uploadFile4';

        if ($status != "error")
        {
            $config['upload_path']      = './archive_files/';
            $config['allowed_types']    = 'gif|jpg|png|doc|txt|pdf|xls|xlsx';
            $config['max_size']         = 1024 * 8;
            $config['encrypt_name']     = TRUE;
      
            $this->load->library('upload', $config);
      
            if (!$this->upload->do_upload($file_element_name)){
                $status = 'error';
                $msg    = $this->upload->display_errors('', '');
            }else{
                $img    = $this->upload->data();
                $status = '';
                $msg    = $img['file_name'];
            }
            @unlink($_FILES[$file_element_name]);
        }
        echo $status.'*'.$msg;
    }

    function uploadfile5(){
        $status = "";
        $msg    = "";
        $file_element_name = 'uploadFile5';

        if ($status != "error")
        {
            $config['upload_path']      = './archive_files/';
            $config['allowed_types']    = 'gif|jpg|png|doc|txt|pdf|xls|xlsx';
            $config['max_size']         = 1024 * 8;
            $config['encrypt_name']     = TRUE;
      
            $this->load->library('upload', $config);
      
            if (!$this->upload->do_upload($file_element_name)){
                $status = 'error';
                $msg    = $this->upload->display_errors('', '');
            }else{
                $img    = $this->upload->data();
                $status = '';
                $msg    = $img['file_name'];
            }
            @unlink($_FILES[$file_element_name]);
        }
        echo $status.'*'.$msg;
    }

    function deletefile(){
        $this->load->helper("url");
        $this->load->helper("file");
        //@unlink(base_url("archive_files/".$filename));
        @unlink(FCPATH .'archive_files/'.$filename);
        //@unlink(base_url("product_files/".$filename));
    }

    function savefile(){
        $this->load->model('m_utils');
        $sql    = true;

        $bookingID     = $this->input->post('bookingID', true);
        $uploadNote1   = $this->input->post('uploadNote1', true);
        $uploadFile1   = $this->input->post('uploadFile1', true);

        $uploadNote2   = $this->input->post('uploadNote2', true);
        $uploadFile2   = $this->input->post('uploadFile2', true);

        $uploadNote3   = $this->input->post('uploadNote3', true);
        $uploadFile3   = $this->input->post('uploadFile3', true);

        $uploadNote4   = $this->input->post('uploadNote4', true);
        $uploadFile4   = $this->input->post('uploadFile4', true);

        $uploadNote5   = $this->input->post('uploadNote5', true);
        $uploadFile5   = $this->input->post('uploadFile5', true);

        $data=array(
            //'id'              => $this->input->post('id', true),
            'bookingID'     => $bookingID,
            'uploadNote1'   => $uploadNote1,
            'uploadFile1'   => $uploadFile1,
            'uploadNote2'   => $uploadNote2,
            'uploadFile2'   => $uploadFile2,
            'uploadNote3'   => $uploadNote3,
            'uploadFile3'   => $uploadFile3,
            'uploadNote4'   => $uploadNote4,
            'uploadFile4'   => $uploadFile4,
            'uploadNote5'   => $uploadNote5,
            'uploadFile5'   => $uploadFile5
        );

        $sql = $this->m_utils->add($data);
        if($sql != false){
            echo 'true';
        }else{ 
            echo 'false'; 
        }
    }
}

?>