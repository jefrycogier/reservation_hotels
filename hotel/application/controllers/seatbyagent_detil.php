<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class seatbyagent_detil extends CI_Controller{
	function __construct() {
		parent::__construct();
		$this->load->model('m_product');
	}

	function view() {
        $this->load->model('m_product');
        $agentID = $this->uri->segment(3);
        $data['all'] = $this->m_product->allproductbyagent($agentID);
        $this->load->view('v_detil_seatbyagent',$data);
    }

}

?>