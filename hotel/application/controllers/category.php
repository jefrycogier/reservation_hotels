<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class category extends CI_Controller{
	function __construct() {
		parent::__construct();
        $this->load->model('m_category');
		$this->load->model('m_seat');
	}

	function index() {
        if($this->session->userdata('id')){
            $session_data           = $this->session->userdata('adminName');
            $data['adminName']      = $session_data;
            $data['title']          = 'Dashboard Administrator';
            $this->load->view('v_category', $data);
        }else{
            //If no session, redirect to login page
            redirect('logout');
        }
	}

	function get_data() {
		$aColumns = array('z.id', 'z.categoryCode', 'z.categoryName');
        $sSearch =  $this->input->post('sSearch',true); 
        $sWhere = "";
        if (isset($sSearch) && $sSearch != "") {
            $sWhere = "AND (";
            for ( $i = 0 ; $i < count($aColumns) ; $i++ ) {
                if($i==1||$i==2){
                    $sWhere .= " ".$aColumns[$i]." LIKE '%".($sSearch)."%' OR ";
                }
            }
            $sWhere = substr_replace( $sWhere, "", - 3 );
            $sWhere .= ') AND z.flag="0"';
        }else{
            $sWhere = ' AND z.flag="0"';
        }
        //echo $sWhere; exit();
		//filter indovidual create by rizal 14/09/2015
		for ($i=0 ; $i<count($aColumns) ; $i++){
			if ($this->input->post('bSearchable_'.$i) == "true" && $this->input->post('sSearch_'.$i) != '' ){
				$sWhere .= " AND ".$aColumns[$i]." LIKE '%".$this->input->post('sSearch_'.$i)."%' ";
			}
		}
		
        $iTotalRecords  = $this->m_category->get_total($aColumns,$sWhere);
        $iDisplayLength = intval($this->input->post('iDisplayLength',true));
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($this->input->post('iDisplayStart',true));
        
        // $sEcho          = intval($_REQUEST['sEcho']);
        $iSortCol_0     = $this->input->post('iSortCol_0',true);
        
        $records = array();
        $records["aaData"] = array();
        $sOrder = "";
        if (isset($iDisplayStart) && $iDisplayLength != '-1' ) {
            $sLimit = "limit ".intval($iDisplayLength)." OFFSET ".intval( $iDisplayStart );
        }

        if (isset($iSortCol_0)) {
            $sOrder = "ORDER BY  ";
            for ( $i = 0 ; $i < intval($this->input->post('iSortingCols')) ; $i++ ) {
                if ( $this->input->post('bSortable_'.intval($this->input->post('iSortCol_'.$i))) == "true" ) {
                    $sOrder .= "".$aColumns[ intval($this->input->post('iSortCol_'.$i)) ]." ".
                    ($this->input->post('sSortDir_'.$i) === 'asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", - 2 );
            if ( $sOrder == "ORDER BY" ) {
                $sOrder = "";
            }
        }

        $data = $this->m_category->get_data($sLimit,$sWhere,$sOrder,$aColumns); 
		//echo $this->db->last_query();
        $no   = 1 + $iDisplayStart;
        foreach ($data as $row) {
            $pars_data = "$row->id|$row->categoryCode|$row->categoryName";

            $action = '
            <a href="javascript:void(0)" onclick="set_val(\''.$pars_data.'\')" class="btn btn-xs btn-warning" title="Edit">
                <i class="fa fa-pencil"></i>
            </a>
            <a href="javascript:void(0)" onclick="set_del(\''.$row->id.'\')" class="btn btn-xs btn-danger" title="Delete">
                <i class="fa fa-trash-o"></i>
            </a>
            <a href="javascript:void(0)" onclick="set_mail(\''.$row->id.'\')" class="btn btn-xs btn-info" title="Email">
                <i class="fa fa-envelope"></i>
            </a>';

            $records["aaData"][] = array(
            		$no,
                    strtoupper($row->categoryCode), 
                    strtoupper($row->categoryName), 
                    $action
            );
            $no++;
        }

        //$records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;

        echo json_encode($records);

	}

	public function save(){
        $sql	= true;

        $session= $this->session->userdata('id');
        $act 	= $this->input->post('act', true);

        $data=array(
            //'id'  	        => $this->input->post('id', true),
            'categoryCode'       => $this->input->post('categoryCode', true),
            'categoryName'       => $this->input->post('categoryName', true)
        );

        if($act == 'add'){
            $data["flag"]   = '0';
            $sql            = $this->m_category->add($data);
        }elseif($act == 'edit'){
            $data["id"]     = $this->input->post('id', true);
            $sql            = $this->m_category->edit($data);
        }

        if($sql == true){
            echo 'true';
        } 
        else { 
            echo 'false'; 
        }
	}
	
	public function delete(){
        $session        = $this->session->userdata('id');
        $id             = $this->input->post('id', true);

        $data["id"]     = $this->input->post('id', true);
        $data["flag"]   = '1';
        $sql = $this->m_category->del($data);
        //echo $this->db->last_query();
        if($sql == true ){		
            echo 'true';
        }else{
            echo 'false';
        }
	}

    public function email(){
        $email = strtolower($this->input->post('email', true));
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            echo 'false';
        }else{

            $categoryID = $this->input->post('categoryID', true);
            $data_cat = $this->m_category->get_cat($categoryID); 

            $pesan = '
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <title>Oxygen Invitation</title>

      <style type="text/css">
        img { max-width: 600px; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}
        a img { border: none; }
        table { border-collapse: collapse !important;}
        #outlook a { padding:0; }
        .ReadMsgBody { width: 100%; }
        .ExternalClass { width: 100%; }
        .backgroundTable { margin: 0 auto; padding: 0; width: 100% !important; }
        table td { border-collapse: collapse; }
        .ExternalClass * { line-height: 115%; }
        .container-for-gmail-android { min-width: 600px; }

        * {
          font-family: Helvetica, Arial, sans-serif;
        }

        body {
          -webkit-font-smoothing: antialiased;
          -webkit-text-size-adjust: none;
          width: 100% !important;
          margin: 0 !important;
          height: 100%;
          color: #676767;
        }

        td {
          font-family: Helvetica, Arial, sans-serif;
          font-size: 14px;
          color: #777777;
          text-align: center;
          line-height: 21px;
        }

        a {
          color: #ff6f6f;
          font-weight: bold;
          text-decoration: none !important;
        }

        .pull-left {
          text-align: left;
        }

        .pull-right {
          text-align: right;
        }

        .header-lg,
        .header-md,
        .header-sm {
          font-size: 32px;
          font-weight: 700;
          line-height: normal;
          padding: 35px 0 0;
          color: #4d4d4d;
        }

        .header-md {
          font-size: 24px;
        }

        .header-sm {
          padding: 5px 0;
          font-size: 18px;
          line-height: 1.3;
        }

        .content-padding {
          padding: 5px 0 5px;
        }

        .mobile-header-padding-right {
          width: 290px;
          text-align: right;
          padding-left: 10px;
        }

        .mobile-header-padding-left {
          width: 290px;
          text-align: left;
          padding-left: 10px;
        }

        .free-text {
          width: 100% !important;
          padding: 10px 60px 0px;
        }

        .block-rounded {
          border-radius: 5px;
          border: 1px solid #e5e5e5;
          vertical-align: top;
        }

        .button {
          padding: 30px 0 0;
        }

        .info-block {
          padding: 0 20px;
          width: 260px;
        }

        .mini-block-container {
          padding: 20px 20px;
          width: 900px;
        }

        .mini-block {
          background-color: #ffffff;
          width: 900px;
          border: 1px solid #cccccc;
          border-radius: 5px;
          padding: 25px 25px;
        }

        .block-rounded {
          width: 260px;
        }

        .info-img {
          width: 258px;
          border-radius: 5px 5px 0 0;
        }

        .force-width-img {
          width: 480px;
          height: 1px !important;
        }

        .force-width-full {
          width: 600px;
          height: 1px !important;
        }

        .user-img img {
          width: 130px;
          border-radius: 5px;
          border: 1px solid #cccccc;
        }

        .user-img {
          text-align: center;
          border-radius: 100px;
          color: #ff6f6f;
          font-weight: 700;
        }

        .user-msg {
          padding-top: 5px;
          font-size: 12px;
          text-align: center;
        }

        .mini-img {
          padding: 5px;
          width: 140px;
        }

        .mini-img img {
          border-radius: 5px;
          width: 140px;
        }

        .force-width-gmail {
          min-width:600px;
          height: 0px !important;
          line-height: 1px !important;
          font-size: 1px !important;
        }

        .mini-imgs {
          padding: 25px 0 30px;
        }
      </style>

      <style type="text/css" media="screen">
        @import url(http://fonts.googleapis.com/css?family=Oxygen:400,700);
      </style>

      <style type="text/css" media="screen">
        @media screen {
          * {
            font-family: "Oxygen", "Helvetica Neue", "Arial", "sans-serif" !important;
          }
        }
      </style>

      <style type="text/css" media="only screen and (max-width: 480px)">
        @media only screen and (max-width: 480px) {

          table[class*="container-for-gmail-android"] {
            min-width: 290px !important;
            width: 100% !important;
          }

          table[class="w320"] {
            width: 320px !important;
          }

          img[class="force-width-gmail"] {
            display: none !important;
            width: 0 !important;
            height: 0 !important;
          }

          td[class*="mobile-header-padding-left"] {
            width: 160px !important;
            padding-left: 0 !important;
          }

          td[class*="mobile-header-padding-right"] {
            width: 160px !important;
            padding-right: 0 !important;
          }

          td[class="mobile-block"] {
            display: block !important;
          }

          td[class="mini-img"],
          td[class="mini-img"] img{
            width: 150px !important;
          }

          td[class="header-lg"] {
            font-size: 24px !important;
            padding-bottom: 5px !important;
          }

          td[class="header-md"] {
            font-size: 18px !important;
            padding-bottom: 5px !important;
          }

          td[class="content-padding"] {
            padding: 5px 0 30px !important;
          }

          td[class="button"] {
            padding: 5px !important;
          }

          td[class*="free-text"] {
            padding: 10px 18px 30px !important;
          }

          img[class="force-width-img"],
          img[class="force-width-full"] {
            display: none !important;
          }

          td[class="info-block"] {
            display: block !important;
            width: 280px !important;
            padding-bottom: 40px !important;
          }

          td[class="info-img"],
          img[class="info-img"] {
            width: 278px !important;
          }

          td[class="mini-block-container"] {
            padding: 8px 20px !important;
            width: 280px !important;
          }

          td[class="mini-block"] {
            padding: 20px !important;
          }

          td[class="user-img"] {
            display: block !important;
            text-align: center !important;
            width: 100% !important;
            padding-bottom: 10px;
          }

          td[class="user-msg"] {
            display: block !important;
            padding-bottom: 5px;
          }
        }
      </style>
    </head>

    <body bgcolor="#f7f7f7">
    <table align="center" cellpadding="0" cellspacing="0" class="container-for-gmail-android" width="100%">
    ';

    $data_tour = $this->m_category->get_product($categoryID); 
    if(!empty($data_tour)){
      foreach ($data_tour as $dt_tour) {
        if($dt_tour->TOTAL>0){
          $pesan .= '
            <tr>
              <td align="center" valign="top" width="100%" style="background-color: #f7f7f7;" class="content-padding">
                <center>
                  <table cellspacing="0" cellpadding="0" width="600" class="w320">
                    <tr>
                      <td class="header-lg">
                        '.strtoupper($dt_tour->productName).'
                      </td>
                    </tr>
                    <tr>
                      <td class="free-text">
                        <span>'.strtoupper($dt_tour->highlight).'</span>
                      </td>
                    </tr>
                    <tr>
                      <td class="mini-block-container">
                        <table cellspacing="0" cellpadding="0" width="100%"  style="border-collapse:separate !important;">
                          <tr>
                            <td class="mini-block">
                              <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                  <td>
                                    <table cellspacing="0" cellpadding="0" width="100%">
                                       <tr>
                                        <td class="user-msg">
          ';
              $data = $this->m_category->get_allproduct($dt_tour->id); 
              if(!empty($data)){
                $txt = '';
                $txt .= '<table width="100%" border="1">';
                $txt .= '<thead>';
                //$txt .= '   <th>NO</th>';
                //$txt .= '   <th>PRODUCT NAME</th>';
                $txt .= '   <th>DEP</th>';
                $txt .= '   <th width="10%">STOCK</th>';
                $txt .= '   <th width="10%">BALANCE</th>';
                $txt .= '   <th width="10%">WL</th>';
                $txt .= '   <th width="10%">DP-NP</th>';
                $txt .= '   <th width="10%">DP-R</th>';
                $txt .= '   <th width="10%">FP-NP</th>';
                $txt .= '   <th width="10%">FP-R</th>';
                $txt .= '   <th width="10%">DONE</th>';
                $txt .= '</thead>';
                $txt .= '<tbody>';
                foreach ($data as $value) {
                    $sts_wl    = $this->m_seat->stock_allotment($value->id,0);
                    $sts_dpnp  = $this->m_seat->stock_allotment($value->id,1);
                    $sts_dpr   = $this->m_seat->stock_allotment($value->id,2);
                    $sts_fpnp  = $this->m_seat->stock_allotment($value->id,3);
                    $sts_fpr   = $this->m_seat->stock_allotment($value->id,4);
                    $sts_comp  = $this->m_seat->stock_allotment($value->id,5);
                    $sts_canc  = $this->m_seat->stock_allotment($value->id,6);
                    $balance = $value->allotment-($sts_dpnp->TOTAL+$sts_dpr->TOTAL+$sts_fpnp->TOTAL+$sts_fpr->TOTAL);
                    $txt .= '<tr>';
                    //$txt .= '   <td>'.($key+1).'</td>';
                    //$txt .= '   <td>'.strtoupper($value->productName).'</td>';
                    $txt .= '   <td>'.strtoupper(date("d M", strtotime($value->depdate))).'</td>';
                    $txt .= '   <td>'.$value->allotment.'</td>';
                    $txt .= '   <td><b>'.$balance.'</b></td>';
                    $txt .= '   <td><font style="color:red;">'.$sts_wl->TOTAL.'</font></td>';
                    $txt .= '   <td>'.$sts_dpnp->TOTAL.'</td>';
                    $txt .= '   <td>'.$sts_dpr->TOTAL.'</td>';
                    $txt .= '   <td>'.$sts_fpnp->TOTAL.'</td>';
                    $txt .= '   <td>'.$sts_fpr->TOTAL.'</td>';
                    $txt .= '   <td>'.$sts_comp->TOTAL.'</td>';
                    $txt .= '</tr>';
                    //$txt .= strtoupper(date("d M Y", strtotime($value->depdate))).'|'.$value->allotment.'|'.$sts_wl->TOTAL.'|'.$sts_dpnp->TOTAL.'|'.$sts_dpr->TOTAL.'|'.$sts_fpnp->TOTAL.'|'.$sts_fpr->TOTAL.'|'.$sts_comp->TOTAL.'|'.$sts_canc->TOTAL.'*';
                }
                $txt .= '</tbody>';
                $txt .= '</table>';
              }else{
                $txt = '';
              }
          $pesan .='
                                          '.$txt.'
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>  
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </center>
              </td>
            </tr>
          ';
        }
      }
    }

    $pesan .= '
    </table>
    </body>
    </html>';

            $this->load->library('email');

            $config['protocol'] = 'CodeIgniter';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['charset'] = 'iso-8859-1';
            $config['wordwrap'] = TRUE;
            $config['mailtype']= "html";

            $this->email->initialize($config);

            $this->email->to($email);
            $this->email->from('info@thegreatholiday.com','THE GREAT HOLIDAY');
            $this->email->cc('itcreatrix@gmail.com');
            $this->email->subject('SEAT STATUS - ' .$data_cat->categoryName);
            $this->email->message($pesan);

            $this->email->send();
            echo "true";
        }
    }

}

?>