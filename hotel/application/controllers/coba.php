<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class coba extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('m_coba');
	}

	public function all(){
		$session_data = $this->session->userdata('adminName');
		$data['adminName'] 		= $session_data;
		$data['title'] 			= 'Dashboard Administrator';
		$data['list_agent'] 	= $this->m_coba->get_agentlist();
		echo $this->db->last_query();
		$this->load->view('v_coba', $data);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */