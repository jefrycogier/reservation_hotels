<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class room_type extends CI_Controller{
	function __construct() {
		parent::__construct();
		$this->load->model('m_room');
	}

	function index() {
        if($this->session->userdata('id')){
            $session_data           = $this->session->userdata('adminName');
            $data['adminName']      = $session_data;
            $data['title']          = 'Dashboard Administrator';
            $this->load->view('v_room_type', $data);
        }else{
            //If no session, redirect to login page
            redirect('logout');
        }
	}

	function get_data() {
		$aColumns = array('z.id', 'z.roomType', 'z.size', 'z.bedType', 'z.extraBed');
        $sSearch =  $this->input->post('sSearch',true); 
        $sWhere = "";
        if (isset($sSearch) && $sSearch != "") {
            $sWhere = "AND (";
            for ( $i = 0 ; $i < count($aColumns) ; $i++ ) {
                if($i==1||$i==2||$i==3||$i==4){
                    $sWhere .= " ".$aColumns[$i]." LIKE '%".($sSearch)."%' OR ";
                }
            }
            $sWhere = substr_replace( $sWhere, "", - 3 );
            $sWhere .= ') AND z.flag="0"';
        }else{
            $sWhere = ' AND z.flag="0"';
        }
        //echo $sWhere; exit();
		//filter indovidual create by rizal 14/09/2015
		for ($i=0 ; $i<count($aColumns) ; $i++){
			if ($this->input->post('bSearchable_'.$i) == "true" && $this->input->post('sSearch_'.$i) != '' ){
				$sWhere .= " AND ".$aColumns[$i]." LIKE '%".$this->input->post('sSearch_'.$i)."%' ";
			}
		}
		
        $iTotalRecords  = $this->m_room->get_total($aColumns,$sWhere);
        $iDisplayLength = intval($this->input->post('iDisplayLength',true));
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($this->input->post('iDisplayStart',true));
        
        // $sEcho          = intval($_REQUEST['sEcho']);
        $iSortCol_0     = $this->input->post('iSortCol_0',true);
        
        $records = array();
        $records["aaData"] = array();
        $sOrder = "";
        if (isset($iDisplayStart) && $iDisplayLength != '-1' ) {
            $sLimit = "limit ".intval($iDisplayLength)." OFFSET ".intval( $iDisplayStart );
        }

        if (isset($iSortCol_0)) {
            $sOrder = "ORDER BY  ";
            for ( $i = 0 ; $i < intval($this->input->post('iSortingCols')) ; $i++ ) {
                if ( $this->input->post('bSortable_'.intval($this->input->post('iSortCol_'.$i))) == "true" ) {
                    $sOrder .= "".$aColumns[ intval($this->input->post('iSortCol_'.$i)) ]." ".
                    ($this->input->post('sSortDir_'.$i) === 'asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", - 2 );
            if ( $sOrder == "ORDER BY" ) {
                $sOrder = "";
            }
        }

        $data = $this->m_room->get_data($sLimit,$sWhere,$sOrder,$aColumns); 
		//echo $this->db->last_query();
        $no   = 1 + $iDisplayStart;
        foreach ($data as $row) {
            $pars_data = "$row->id|$row->roomType|$row->size|$row->bedType|$row->extraBed|$row->breakfast|$row->priceA|$row->priceB|$row->priceC|$row->priceD|$row->priceE|$row->surcharge|$row->surcharge2";
            $pars_data_roomprice = $row->id."|".$row->roomType."|".$row->priceA."|".$row->priceB."|".$row->priceC."|".$row->priceD."|".$row->priceE."|".$row->surcharge."|".$row->surcharge2;
            //$pars_setting = "$row->id|$row->isStatus|$row->isProfile|$row->isPassword";

            if($row->availability == 0){
                    $status = '<span class="label label-sm label-success">Active</span>';
            }else{
                    $status = '<span class="label label-sm label-danger">Inactive</span>';
            }

            $action = '
            <div class="btn-group">
                <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown"> Action
                    <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a href="javascript:void(0)" onclick="set_val(\''.$pars_data.'\')"> <i class="fa fa-pencil"></i> Edit </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" onclick="set_update_detail(\''.$pars_data_roomprice.'\')"> <i class="fa fa-search"></i> Detail </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" onclick="set_del(\''.$row->id.'\')"> <i class="fa fa-trash-o"></i> Delete </a>
                    </li>
                </ul>
            </div>';

            $records["aaData"][] = array(
            		$no,
                    strtoupper($row->roomType), 
                    strtoupper($row->size), 
                    strtoupper($row->bedType), 
                    strtoupper($row->extraBed), 
                    number_format($row->priceA, 0, '.', '.'), 
                    $status,
                    $action
            );
            $no++;
        }

        //$records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;

        echo json_encode($records);

	}

	public function save(){
        $sql	= true;

        $session= $this->session->userdata('id');
        $act 	= $this->input->post('act', true);

        $data=array(
                //'id'  	        => $this->input->post('id', true),
                'roomType'      => strtoupper($this->input->post('roomType', true)),
                'size'          => strtoupper($this->input->post('size', true)),
                'bedType'       => strtoupper($this->input->post('bedType', true)),
                'extraBed'      => strtoupper($this->input->post('extraBed', true)),
                'breakfast'     => strtoupper($this->input->post('breakfast', true)),
                'availability'  => 0,
                'priceA'          => strtoupper($this->input->post('priceA', true)),
                'priceB'          => strtolower($this->input->post('priceB', true)),
                'priceC'          => strtolower($this->input->post('priceC', true)),
                'priceD'          => strtolower($this->input->post('priceD', true)),
                'priceE'          => strtolower($this->input->post('priceE', true)),
                'surcharge'       => strtolower($this->input->post('surcharge', true)),
                'surcharge2'      => strtolower($this->input->post('surcharge2', true)),
                //'startdate'      => strtolower($this->input->post('startdate', true)),
                //'enddate'      => strtolower($this->input->post('enddate', true)),
    //                  
                //'isStatus'      => 0,
        );

        if($act == 'add'){
            $start_date = date($this->input->post('startdate', true));
            $end_date = date($this->input->post('enddate', true));
            $start_date = date("Y-m-d", strtotime($start_date));
            $end_date = date("Y-m-d", strtotime($end_date));
            $data_detail["startdate"]      = $start_date;
            $data_detail["enddate"]      = $end_date;
            //$data_date = array_merge($start_date, $end_date);
            //$data['created_by'] = $session['user_id']; 
            //$data["createdDate"]    = date('Y-m-d H:i:s');
            //$data["flag"]   = '0';
            $sql            = $this->m_room->add($data,$data_detail);
            
            //$sql_allotment            = $this->m_room->insert_allotment($data,$start_date,$end_date);
           
                if($sql != false){
                    echo 'true';
                } 
                else { 
                    echo 'false'; 
                }
            }elseif($act == 'edit'){
                $data["id"]     = $this->input->post('id', true);
                $sql            = $this->m_room->edit($data);
                if($sql != false){
                    echo 'true';
                } 
                else { 
                    echo 'false'; 
                }
            }
	}
	
	public function delete(){
            $session        = $this->session->userdata('id');
            $id             = $this->input->post('id', true);

            $data["id"]     = $this->input->post('id', true);
            $data["flag"]   = '1';
            $sql = $this->m_room->del($data);
            //echo $this->db->last_query();
            if($sql == true ){		
                echo 'true';
            }else{
                echo 'false';
            }
	}
        
        public function update_detail(){
            
            $data=array(
                'id'  	        => $_POST["id"],
                'priceA'          => $_POST["priceA"],
                'priceB'          => $_POST["priceB"],
                'priceC'          => $_POST["priceC"],
                'priceD'          => $_POST["priceD"],
                'priceE'          => $_POST["priceE"],
                'surcharge'       => $_POST["surcharge"],
                'surcharge2'      => $_POST["surcharge2"],
              );
            
            $sql = $this->m_room->edit($data);
            if($sql == true ){		
                echo 'true';
            }else{
                echo 'false';
            }
            
        }


}

?>