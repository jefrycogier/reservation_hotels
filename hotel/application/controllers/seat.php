<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class seat extends CI_Controller{
	function __construct() {
		parent::__construct();
		$this->load->model('m_seat');
	}

	function index() {
        if($this->session->userdata('id')){
            $session_data           = $this->session->userdata('adminName');
            $data['adminName']      = $session_data;
            $data['title']          = 'Dashboard Administrator';
            $this->load->view('v_seat', $data);
        }else{
            //If no session, redirect to login page
            redirect('logout');
        }
	}

	function get_data() {
		$aColumns = array('z.id', 'z.airlineName', 'z.cityName', 'z.categoryName', 'z.days', 'z.productName');
        $sSearch =  $this->input->post('sSearch',true); 
        $sWhere = "";
        if (isset($sSearch) && $sSearch != "") {
            $sWhere = "AND (";
            for ( $i = 0 ; $i < count($aColumns) ; $i++ ) {
                if($i==1||$i==2||$i==3||$i==4||$i==5){
                    $sWhere .= " ".$aColumns[$i]." LIKE '%".($sSearch)."%' OR ";
                }
            }
            $sWhere = substr_replace( $sWhere, "", - 3 );
            $sWhere .= ') AND z.flag="0"';
        }else{
            $sWhere = ' AND z.flag="0"';
        }
        //echo $sWhere; exit();
		//filter indovidual create by rizal 14/09/2015
		for ($i=0 ; $i<count($aColumns) ; $i++){
			if ($this->input->post('bSearchable_'.$i) == "true" && $this->input->post('sSearch_'.$i) != '' ){
				$sWhere .= " AND ".$aColumns[$i]." LIKE '%".$this->input->post('sSearch_'.$i)."%' ";
			}
		}
		
        $iTotalRecords  = $this->m_seat->get_total($aColumns,$sWhere);
        $iDisplayLength = intval($this->input->post('iDisplayLength',true));
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($this->input->post('iDisplayStart',true));
        
        // $sEcho          = intval($_REQUEST['sEcho']);
        $iSortCol_0     = $this->input->post('iSortCol_0',true);
        
        $records = array();
        $records["aaData"] = array();
        $sOrder = "";
        if (isset($iDisplayStart) && $iDisplayLength != '-1' ) {
            $sLimit = "limit ".intval($iDisplayLength)." OFFSET ".intval( $iDisplayStart );
        }

        if (isset($iSortCol_0)) {
            $sOrder = "ORDER BY  ";
            for ( $i = 0 ; $i < intval($this->input->post('iSortingCols')) ; $i++ ) {
                if ( $this->input->post('bSortable_'.intval($this->input->post('iSortCol_'.$i))) == "true" ) {
                    $sOrder .= "".$aColumns[ intval($this->input->post('iSortCol_'.$i)) ]." ".
                    ($this->input->post('sSortDir_'.$i) === 'asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", - 2 );
            if ( $sOrder == "ORDER BY" ) {
                $sOrder = "";
            }
        }

        $data = $this->m_seat->get_data($sLimit,$sWhere,$sOrder,$aColumns); 
		//echo $this->db->last_query();
        $no   = 1 + $iDisplayStart;
        foreach ($data as $row) {
            $pars_depdate = $this->m_seat->get_depdate($row->id);
            $txt = '';
            foreach ($pars_depdate as $value) {
                $sts_wl    = $this->m_seat->stock_allotment($value->id,0);
                $sts_dpnp  = $this->m_seat->stock_allotment($value->id,1);
                $sts_dpr   = $this->m_seat->stock_allotment($value->id,2);
                $sts_fpnp  = $this->m_seat->stock_allotment($value->id,3);
                $sts_fpr   = $this->m_seat->stock_allotment($value->id,4);
                $sts_comp  = $this->m_seat->stock_allotment($value->id,5);
                $sts_canc  = $this->m_seat->stock_allotment($value->id,6);
                $txt .= strtoupper(date("d M Y", strtotime($value->depdate))).'|'.$value->allotment.'|'.$sts_wl->TOTAL.'|'.$sts_dpnp->TOTAL.'|'.$sts_dpr->TOTAL.'|'.$sts_fpnp->TOTAL.'|'.$sts_fpr->TOTAL.'|'.$sts_comp->TOTAL.'|'.$sts_canc->TOTAL.'*';
            }
/*
            if ($row->edu_active == 't') {
                    $status = '<span class="label label-sm label-success">Aktif</span>';
            }
            else {
                    $status = '<span class="label label-sm label-danger">Non Aktif</span>';
            }
*/
            $action = '
            <a href="javascript:void(0)" onclick="view_detil(\''.$txt.'\')" class="btn btn-xs btn-info" title="View seat allotment">
                <i class="fa fa-search"></i>
            </a>';
            /*
            <a href="javascript:void(0)" onclick="set_del(\''.$row->id.'\')" class="btn btn-xs btn-danger" title="Delete">
                <i class="fa fa-trash-o"></i>
            </a>';
            */

            $records["aaData"][] = array(
            		$no,
                    strtoupper($row->categoryName),
                    strtoupper($row->cityName), 
                    strtoupper($row->airlineName), 
                    strtoupper($row->productName), 
                    strtoupper($row->days), 
                    $action
            );
            $no++;
        }

        //$records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;

        echo json_encode($records);

	}
	
	public function delete(){
		// print_r($_POST["id"]);
        $id  	= $this->input->post('id', true);
        $sql 	= $this->m_seat->del($id);
        //echo $this->db->last_query();
        if($sql == true ){		
            echo 'true';
        }else{
            echo 'false';
        }
	}

    public function viewdetil(){
        $productID = $this->uri->segment(3);
        $pars_depdate = $data = $this->m_seat->get_depdate($productID);
        $this->load->view('v_product_detil', $pars_depdate);
    }

}

?>