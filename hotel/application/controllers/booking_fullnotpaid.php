<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class booking_fullnotpaid extends CI_Controller{
	function __construct() {
		parent::__construct();
		$this->load->model('m_booking');
        $this->load->model('m_product');
	}

	function index() {
        if($this->session->userdata('id')){
            $session_data           = $this->session->userdata('adminName');
            $data['adminName']      = $session_data;
            $data['title']          = 'Dashboard Administrator';
            $this->load->view('v_booking_fullnotpaid', $data);
        }else{
            //If no session, redirect to login page
            redirect('logout');
        }
	}

    function get_data() {
        $aColumns = array('z.id', 'z.bookingCode', 'z.agentName', 'z.productName', 'z.depdate', 'z.guestName');
        $sSearch =  $this->input->post('sSearch',true); 
        $sWhere = "";
        if (isset($sSearch) && $sSearch != "") {
            $sWhere = "AND (";
            for ( $i = 0 ; $i < count($aColumns) ; $i++ ) {
                if($i==1||$i==2||$i==3||$i==5){
                    $sWhere .= " ".$aColumns[$i]." LIKE '%".($sSearch)."%' OR ";
                }
            }
            $sWhere = substr_replace( $sWhere, "", - 3 );
            $sWhere .= ') AND z.bookingStatus="3"';
        }else{
            $sWhere = ' AND z.bookingStatus="3"';
        }
        //echo $sWhere; exit();
        //filter indovidual create by rizal 14/09/2015
        for ($i=0 ; $i<count($aColumns) ; $i++){
            if ($this->input->post('bSearchable_'.$i) == "true" && $this->input->post('sSearch_'.$i) != '' ){
                $sWhere .= " AND ".$aColumns[$i]." LIKE '%".$this->input->post('sSearch_'.$i)."%' ";
            }
        }
        
        $iTotalRecords  = $this->m_booking->get_total($aColumns,$sWhere);
        $iDisplayLength = intval($this->input->post('iDisplayLength',true));
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($this->input->post('iDisplayStart',true));
        
        // $sEcho          = intval($_REQUEST['sEcho']);
        $iSortCol_0     = $this->input->post('iSortCol_0',true);
        
        $records = array();
        $records["aaData"] = array();
        $sOrder = "";
        if (isset($iDisplayStart) && $iDisplayLength != '-1' ) {
            $sLimit = "limit ".intval($iDisplayLength)." OFFSET ".intval( $iDisplayStart );
        }

        if (isset($iSortCol_0)) {
            $sOrder = "ORDER BY  ";
            for ( $i = 0 ; $i < intval($this->input->post('iSortingCols')) ; $i++ ) {
                if ( $this->input->post('bSortable_'.intval($this->input->post('iSortCol_'.$i))) == "true" ) {
                    $sOrder .= "".$aColumns[ intval($this->input->post('iSortCol_'.$i)) ]." ".
                    ($this->input->post('sSortDir_'.$i) === 'asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", - 2 );
            if ( $sOrder == "ORDER BY" ) {
                $sOrder = "ORDER BY z.createdDate desc";
            }
        }

        $data = $this->m_booking->get_data($sLimit,$sWhere,$sOrder,$aColumns); 
        //echo $this->db->last_query();
        $no   = 1 + $iDisplayStart;
        foreach ($data as $row) {

            $action = '
            <a href="'.base_url().'booking_all/detils/'.$row->id.'" class="btn btn-xs btn-info" title="Detail"> Detail <i class="fa fa-play-circle"></i></a>
            ';

            $records["aaData"][] = array(
                    $no,
                    strtoupper(date("d M", strtotime($row->createdDate))), 
                    strtoupper($row->bookingCode),
                    strtoupper($row->agentName), 
                    strtoupper($row->productName), 
                    strtoupper(date("d M", strtotime($row->depdate))), 
                    strtoupper($row->guestName), 
                    $action
            );
            $no++;
        }

        //$records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;

        echo json_encode($records);
    }

    function detil(){
        if($this->session->userdata('id')){
            $session_data           = $this->session->userdata('adminName');
            $data['adminName']      = $session_data;
            $data['title']          = 'Dashboard Administrator';

            $id                     = $this->uri->segment(3);
            $data['all']            = $this->m_booking->get_booking($id);
            foreach ($data['all'] as $value) {
                $data['product']    = $this->m_product->get_detil_wjoin($value->productID);
            }
            $data['guest']          = $this->m_booking->get_guest($id);
            //print_r($data['product']);
            //$data['product']        = $this->m_product->get_detil($id);
            $this->load->view('v_booking_detail', $data);
        }else{
            //If no session, redirect to login page
            redirect('logout');
        }
    }

}

?>