<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class product_view extends CI_Controller{
	function __construct() {
		parent::__construct();
		$this->load->model('m_product');
	}

    function load_data(){
        if($this->session->userdata('id')){
            $session_data           = $this->session->userdata('adminName');
            $data['adminName']      = $session_data;
            $data['title']          = 'Dashboard Administrator';

            $data['list_departure'] = $this->m_product->get_departure();
            //echo $this->db->last_query();
            $data['list_airline']   = $this->m_product->get_airline();
            $data['list_category']  = $this->m_product->get_category();
            $data['list_room']      = $this->m_product->get_room();
            //echo $this->db->last_query();

            $id                     = $this->uri->segment(3);
            $data['all']            = $this->m_product->get_detil($id);
            $data['list_depdate']   = $this->m_product->get_depdate($id);
            //depdate|allotment|remarks|dpAmount|patt|pas|pcwa|pcwb|pcnb|surcharge|surcharge2|discount|discount2
            $txtd = '';
            foreach ($data['list_depdate'] as $value) {
                $pisah = explode("-", $value->depdate); //y-m-d
                $txtd .= $pisah[2].'-'.$pisah[1].'-'.$pisah[0].'|'.$value->allotment.'|'.$value->remarks.'|'.$value->dpAmount.'|'.$value->patt.'|'.$value->pas.'|'.$value->pcwa.'|'.$value->pcwb.'|'.$value->pcnb.'|'.$value->surcharge.'|'.$value->surcharge2.'|'.$value->discount.'|'.$value->discount2.'|'.$value->id.'*';
            }
            $data['list_depdate']  = $txtd;

            $data['list_product_room']  = $this->m_product->get_product_room($id);
            $txt = '';
            foreach ($data['list_product_room'] as $value) {
                $txt .= $value->roomID.'|';
            }
            $data['list_product_room']  = $txt;

            $this->load->view('v_product_view', $data);
        }else{
            //If no session, redirect to login page
            redirect('logout');
        }
    }

    function save(){
        $sql    = true;

        $session= $this->session->userdata('id');
        $act    = $this->input->post('act', true);

        $data = array(
            //'id'              => $this->input->post('id', true),
            'airlineID'     => $this->input->post('airlineID', true),
            'departureID'   => $this->input->post('departureID', true),
            'categoryID'    => $this->input->post('categoryID', true),
            'noofdays'      => $this->input->post('noofdays', true),
            'noofnights'    => $this->input->post('noofnights', true),
            'productName'   => $this->input->post('productName', true),
            'highlight'     => $this->input->post('highlight', true),
            'generalRemarks'=> $this->input->post('generalRemarks', true),
            'timelimit'     => '0',
            'flag'          => '0'
        );
print_r($data);
        $sql = $this->m_product->add($data);
        if($sql != false){
            //DEP. DATE
            $dt_room    = $this->input->post('isi', true);
            $exp        = array();
            $pisah      = explode("*",$dt_room);
            $data_depdate = array_merge($exp, $pisah);//array_push($pisah);
            $sql_detil = $this->m_product->add_depdate($data_depdate,$sql);
            //ROOM
            $roomID     = $this->input->post('roomID');
            $sql_room   = $this->m_product->add_room($roomID,$sql);
            echo 'true';
        } 
        else { 
            echo 'false'; 
        }
    }

}

?>