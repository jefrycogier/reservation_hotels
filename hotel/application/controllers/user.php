<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user extends CI_Controller{
	function __construct() {
		parent::__construct();
		$this->load->model('m_user');
	}

	function index() {
        if($this->session->userdata('id')){
            $session_data           = $this->session->userdata('adminName');
            $data['adminName']      = $session_data;
            $data['title']          = 'Dashboard Administrator';
            $this->load->view('v_user', $data);
        }else{
            //If no session, redirect to login page
            redirect('logout');
        }
	}

	function get_data() {
		$aColumns = array('z.id', 'z.userID', 'z.adminName', 'z.email');
        $sSearch =  $this->input->post('sSearch',true); 
        $sWhere = "";
        if (isset($sSearch) && $sSearch != "") {
            $sWhere = "AND (";
            for ( $i = 0 ; $i < count($aColumns) ; $i++ ) {
                if($i==1||$i==2||$i==3){
                    $sWhere .= " ".$aColumns[$i]." LIKE '%".($sSearch)."%' OR ";
                }
            }
            $sWhere = substr_replace( $sWhere, "", - 3 );
            $sWhere .= ') AND z.flag="0"';
        }else{
            $sWhere = ' AND z.flag="0"';
        }
        //echo $sWhere; exit();
		//filter indovidual create by rizal 14/09/2015
		for ($i=0 ; $i<count($aColumns) ; $i++){
			if ($this->input->post('bSearchable_'.$i) == "true" && $this->input->post('sSearch_'.$i) != '' ){
				$sWhere .= " AND ".$aColumns[$i]." LIKE '%".$this->input->post('sSearch_'.$i)."%' ";
			}
		}
		
        $iTotalRecords  = $this->m_user->get_total($aColumns,$sWhere);
        $iDisplayLength = intval($this->input->post('iDisplayLength',true));
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($this->input->post('iDisplayStart',true));
        
        // $sEcho          = intval($_REQUEST['sEcho']);
        $iSortCol_0     = $this->input->post('iSortCol_0',true);
        
        $records = array();
        $records["aaData"] = array();
        $sOrder = "";
        if (isset($iDisplayStart) && $iDisplayLength != '-1' ) {
            $sLimit = "limit ".intval($iDisplayLength)." OFFSET ".intval( $iDisplayStart );
        }

        if (isset($iSortCol_0)) {
            $sOrder = "ORDER BY  ";
            for ( $i = 0 ; $i < intval($this->input->post('iSortingCols')) ; $i++ ) {
                if ( $this->input->post('bSortable_'.intval($this->input->post('iSortCol_'.$i))) == "true" ) {
                    $sOrder .= "".$aColumns[ intval($this->input->post('iSortCol_'.$i)) ]." ".
                    ($this->input->post('sSortDir_'.$i) === 'asc' ? 'asc' : 'desc') .", ";
                }
            }

            $sOrder = substr_replace( $sOrder, "", - 2 );
            if ( $sOrder == "ORDER BY" ) {
                $sOrder = "";
            }
        }

        $data = $this->m_user->get_data($sLimit,$sWhere,$sOrder,$aColumns); 
		//echo $this->db->last_query();
        $no   = 1 + $iDisplayStart;
        foreach ($data as $row) {
            $pars_data = "$row->id|$row->userID|$row->password|$row->adminName|$row->email|$row->mobile|$row->WA|$row->BBM";
/*
            if ($row->edu_active == 't') {
                    $status = '<span class="label label-sm label-success">Aktif</span>';
            }
            else {
                    $status = '<span class="label label-sm label-danger">Non Aktif</span>';
            }
*/
            $action = '
            <a href="javascript:void(0)" onclick="set_val(\''.$pars_data.'\')" class="btn btn-xs btn-warning" title="Edit">
                <i class="fa fa-pencil"></i>
            </a>
            <a href="javascript:void(0)" onclick="set_del(\''.$row->id.'\')" class="btn btn-xs btn-danger" title="Delete">
                <i class="fa fa-trash-o"></i>
            </a>';

            $records["aaData"][] = array(
            		$no,
                    strtolower($row->userID),
                    strtoupper($row->adminName), 
                    strtolower($row->email), 
                    $action
            );
            $no++;
        }

        //$records["sEcho"] = $sEcho;
        $records["iTotalRecords"] = $iTotalRecords;
        $records["iTotalDisplayRecords"] = $iTotalRecords;

        echo json_encode($records);

	}

	public function save(){
        $sql	= true;

        $session= $this->session->userdata('id');
        $act 	= $this->input->post('act', true);

        $data=array(
                //'id'  	        => $this->input->post('id', true),
                'userID'  	=> $this->input->post('userID', true),
                'password'      => $this->input->post('password', true),
                'adminName'  	=> $this->input->post('adminName', true), 
                'email'         => $this->input->post('email', true), 
                'mobile'        => $this->input->post('mobile', true), 
                'WA'            => $this->input->post('WA', true),
                'BBM'           => $this->input->post('BBM', true),
                'position'      => $this->input->post('position', true)
        );

        if($act == 'add'){
            // $data['created_by'] = $session['user_id']; 
            //$data["createdDate"]    = date('Y-m-d H:i:s');
            //$data["createdBy"]      = $session;
//            $email = $this->input->post('email');
//            $data["email"]   = $email;
            $data["flag"]   = '0';
            $email_authentication = $this->m_user->email_valid($data["email"]);
            if ($email_authentication->num_rows() == 1) {
		echo "<script>alert('email already registered !! Check this out');history.go(-1);</script>";
                redirect('user');
                
            }else {
//                $email = $this->input->post('email');
//                $data["email"]   = $email;
                $sql = $this->m_user->add($data);
            }
        }elseif($act == 'edit'){
            $data["id"]     = $this->input->post('id', true);
            $sql            = $this->m_user->edit($data);
        }

        if($sql == true){
            echo 'true';
        } 
        else { 
            echo 'false'; 
        }
	}
	
	public function delete(){
        $session['id']  = $this->session->userdata('id');
        $id             = $this->input->post('id', true);
        if($session['id']==$this->input->post('id', true)){
            echo 'error';
        }else{
            $data["id"]     = $this->input->post('id', true);
            $data["flag"]   = '1';
            $sql = $this->m_user->del($data);
            //echo $this->db->last_query();
            if($sql == true ){		
                echo 'true';
            }else{
                echo 'false';
            }
        }
	}

}

?>