<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class booking_add extends CI_Controller{
	function __construct() {
		parent::__construct();
		$this->load->model('m_booking');
	}

	function index() {
        if($this->session->userdata('staffID')){
            $session_data           = $this->session->userdata('staffName');
            $data['staffName']      = $session_data;
            $data['title']          = 'Dashboard Administrator';

            /*
            $data['list_departure'] = $this->m_product->get_departure();
            //echo $this->db->last_query();
            $data['list_airline']   = $this->m_product->get_airline();
            $data['list_category']  = $this->m_product->get_category();
            $data['list_room']      = $this->m_product->get_room();
            */
            $this->load->view('v_booking_add', $data);
        }else{
            //If no session, redirect to login page
            redirect('logout');
        }
	}

    function newbooking(){
        if($this->session->userdata('staffID')){
            $session_data           = $this->session->userdata('staffName');
            $data['staffName']      = $session_data;
            $data['title']          = 'Dashboard Administrator';

            //$data['list_departure'] = $this->m_product->get_departure();
            //echo $this->db->last_query();
            //$data['list_airline']   = $this->m_product->get_airline();
            //$data['list_category']  = $this->m_product->get_category();
            //$data['list_room']      = $this->m_product->get_room();

            $id                     = $this->uri->segment(3);
            $data['all']            = $this->m_booking->get_detil($id);
            //echo $this->db->last_query();
            $data['list_depdate']   = $this->m_booking->get_depdate($id);
            $data['list_room']      = $this->m_booking->get_room($id);
            $this->load->view('v_booking_add', $data);
        }else{
            //If no session, redirect to login page
            redirect('logout');
        }
    }

    function save(){
        $sql    = true;

        $session        = $this->session->userdata('agentID');
        $sessionstaff   = $this->session->userdata('staffID');
        $act            = $this->input->post('act', true);

        //$cek_allotment = $this->m_booking->allotment_used($this->input->post('productID', true), $this->input->post('depdateID', true));
        //$cek_allotment2 = $this->m_booking->allotment($this->input->post('productID', true), $this->input->post('depdateID', true));

        //print_r($cek_allotment); exit();

        $data=array(
            //'id'              => $this->input->post('id', true),
            'bookingCode'   => date('ymdhis'),
            'agentID'       => $session,
            'productID'     => $this->input->post('productID', true),
            'depdateID'     => $this->input->post('depdateID', true),
            'roomID'        => $this->input->post('roomID', true),
            'bookingStatus' => '1',
            'createdDate'   => date('Y-m-d H:i:s')
        );

        $sql = $this->m_booking->add($data);
        if($sql != false){
            //LOG
            $data_log=array(
                //'id'              => $this->input->post('id', true),
                'bookingID'     => $sql,
                'agentID'       => $session,
                'staffID'       => $sessionstaff,
                'adminID'       => 0,
                'lastStatus'    => '',
                'newStatus'     => 'Confirmed (Deposit Not Paid)',
                'createdDate'   => date('Y-m-d H:i:s')
            );
            $sql_log   = $this->m_booking->add_log($data_log);

            //ROOM
            $dt['gtype']    = $this->input->post('gtype', true);
            $dt['gtittle']  = $this->input->post('gtittle', true);
            $dt['gfname']   = $this->input->post('gfname', true);
            $dt['glname']   = $this->input->post('glname', true);
            $dt['gdob']     = $this->input->post('gdob', true);
            $dt['gpassport'] = $this->input->post('gpassport', true);
            foreach ( $dt['gtype'] as $key => $value) {
                $data = array(
                    //'id'              => $this->input->post('id', true),
                    'bookingID'    => $sql,
                    'gtype'        => $dt['gtype'][$key],
                    'gtittle'      => $dt['gtittle'][$key],
                    'gfname'       => $dt['gfname'][$key],
                    'glname'       => $dt['glname'][$key],
                    'gdob'         => $dt['gdob'][$key],
                    'gpassport'    => $dt['gpassport'][$key]
                );
                $sql_room   = $this->m_booking->add_detil($data);
            }
/*
            $roomID     = $this->input->post('roomID');
            $sql_room   = $this->m_product->add_room($roomID,$sql);
*/
            echo 'true';
        } 
        else { 
            echo 'false'; 
        }
    }

}

?>