<?php include "include_header.php"; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<script type="text/javascript" src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>


<div class="row">
	<div class="col-md-12">
	  <div class="portlet box green">
		<div class="portlet-title">
			<div class="caption">
				<small> Master Data <i class="fa fa-angle-right"></i> User <i class="fa fa-angle-right"></i> Room Type </small>
			</div>
			<div class="actions">
				 <div class="btn-group btn-group-solid">
				<a class="btn default" href="javascript:;" id="form_act"><i class="fa fa-plus-square"></i> Add New Data</a>
				</div>
			</div>
		</div>
		<div class="portlet-body form" id="form_container" style="display:none;">
			<form role="form" class="form-horizontal" id="form_master" method="post">
			<input type="hidden" name="act" id="act" value="add"/>
			<input type="hidden" name="id" id="id" value=""/>
                        
				<div class="form-body">
				<div id="alert"></div>
					<fieldset>
						<legend>A. GENERAL INFORMATION</legend>
						<div class="form-group">
							<label class="col-md-3 control-label" for="inputSuccess">Room Type<span class="required">*</span></label>
							<div class="col-md-6">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="Room Type (required)" data-container="body"></i>
									<input name="roomType" type="text" required="true" class="form-control" id="roomType">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label" for="inputSuccess">Size<span class="required">*</span></label>
							<div class="col-md-6">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="Size (required)" data-container="body"></i>
									<input name="size" type="text" required="true" class="form-control" id="size">
								</div>
							</div>
						</div>
						<div class="form-group">
                                                        <label class="col-md-3 control-label" for="inputSuccess">Bed Type<span class="required">*</span></label>
							<div class="col-md-6">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="Bed Type (required)" data-container="body"></i>
									<input name="bedType" type="text" required="true" class="form-control" id="bedType">
								</div>
							</div>
						</div>
						<div class="form-group">
                                                        <label class="col-md-3 control-label" for="inputSuccess">Extra Bed<span class="required">*</span></label>
							<div class="col-md-6">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="Extra Bed (required)" data-container="body"></i>
									<input name="extraBed" type="text" required="true" class="form-control" id="extraBed">
								</div>
							</div>
						</div>
						<div class="form-group">
                                                        <label class="col-md-3 control-label" for="inputSuccess">Breakfast<span class="required">*</span></label>
							<div class="col-md-6">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="Breakfast (required)" data-container="body"></i>
									<input name="breakfast" type="text" required="true" class="form-control" id="breakfast">
								</div>
							</div>
						</div>
					</fieldset>
				</div>
				<div class="form-actions center">
					<div class="col-md-offset-4 col-md-8">
						<button id="cancel" type="button" class="btn default">Cancel</button>
						<button id="save" type="button" class="btn blue"  data-loading-text="Loading...">Save</button>
					</div>
				</div>
			</form>
		</div>
	   </div>
	</div>
</div>

<style type="text/css">
    	#roomType,#size,#bedType,#extraBed,#breakfast,#priceA,#priceB,#priceC,#priceD,#priceE,#surcharge,#surcharge2{
		text-transform: uppercase;
	}
/*	#email,#contactEmail{
		text-transform: lowercase;
	}*/
</style>

<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-table"></i>
					<small>Room Type List</small>
				</div>
			</div>
			<div class="portlet-body">
				<div id="view-table">
					<div class="table-toolbar"></div>
					<table class="table table-striped table-bordered table-hover" id="refrensi-table">
						<thead>
							<tr>
								<th data-sortable="false">No</th>
								<th>Room Type</th>
								<th>Size</th>
								<th>Bed Type</th>
								<th>Extra Bed</th>
								<th>Availability</th>
								<th data-sortable="false">Action</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				<div id="view-tree">
					<div id="list-tree" style="margin:15px; font-size:14px;"></div>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_detil" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="form_setting_container">
        <form role="form" class="form-horizontal" id="form_setting" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Setting</h4>
            </div>
            <div class="modal-footer">
            	<button id="save_setting" type="button" class="btn blue" data-loading-text="Loading..."  >Save</button>
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script> 
	var TableAdvanced = function () {
	var initTable1 = function() {

		/*
		* Initialize DataTables, with no sorting on the 'details' column
		*/
		var target='#refrensi-table';
		var oTable = $(target).dataTable( {
			"aoColumnDefs": [
				{
					//"bSortable": false, "aTargets": [ 0,1 ]
				}
			],
			"aoColumns": [
                { "sWidth": "5%" },  
                { "sWidth": "15%" }, 
                { "sWidth": "25%" }, 
                { "sWidth": "15%" }, 
                { "sWidth": "15%" }, 
                { "sWidth": "10%" }, 
                { "sWidth": "15%" }
            ],
			"aaSorting": [[1, 'asc']],
			"aLengthMenu": [
				[10, 20, 50, -1],
				[10, 20, 50, "All"] // change per page values here
			],
			// set the initial value
			"iDisplayLength": 10, // default records per page
			"oLanguage": {
				// language settings
				"sLengthMenu": "Display _MENU_ records",
				"sSearch": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
				"sProcessing": '<img src="<?php echo base_url() ?>assets/global/img/loading-spinner-grey.gif"><span>&nbsp;&nbsp;Loading...</span>',
				"sInfoEmpty": "No records found to show",
				"sAjaxRequestGeneralError": "Could not complete request. Please check your internet connection",
				"sEmptyTable":  "No data available in table",
				"sZeroRecords": "No matching records found",
				"oPaginate": {
					"sPrevious": "Prev",
					"sNext": "Next",
					"sPage": "Page",
					"sPageOf": "of"
				}
			},
			"bAutoWidth": true,   // disable fixed width and enable fluid table
			"bSortCellsTop": true, // make sortable only the first row in thead
			"sPaginationType": "bootstrap_full_number", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
			"bProcessing": true, // enable/disable display message box on record load
			"bServerSide": true, // enable/disable server side ajax loading
			"sAjaxSource": "<?=base_url()?>room_type/get_data", // define ajax source URL
			"sServerMethod": "POST"
		});

		$(target+'_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
		$(target+'_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
		$(target+'_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
		$(target+'_wrapper .dataTables_filter input').unbind();
		$(target+'_wrapper .dataTables_filter input').bind('keyup', function(e) {
			if(e.keyCode == 13) {
				oTable.fnFilter(this.value);
			}
		});
		$(target+'_wrapper .dataTables_filter a').bind('click', function(e) {
			var key=$(target+'_wrapper .dataTables_filter input').val();
			oTable.fnFilter(key);
		});
	}

	return {

		//main function to initiate the module
		init: function () {
			if (!$().dataTable) {
				return;
			}
			initTable1();
		}

	};

}();

$(document).ready(function() {
	TableAdvanced.init();

	$('#form_act').bind('click', function(e) {
		$('#userID').attr('readonly', false);
		$('#form_container').slideToggle(500);
		$('#form_master')[0].reset();
	});

	$('#save').click(function(){
		$('#form_master').submit(); 
	});
	
	$('#cancel').click(function(){
		document.getElementById('form_master').reset();
		$('#id').val('');
		$('#act').val('add');
		$('#form_container').slideUp(500);
		loadTbl();
	});
	
	$('#form_container').find('form').validate({
		errorClass: 'help-block',
		errorElement: 'span',
		ignore: 'input[type=hidden]',
		highlight: function(el, errorClass) {
			$(el).parents('.form-group').first().addClass('has-error');
		},
		unhighlight: function(el, errorClass) {
			var $parent = $(el).parents('.form-group').first();
			$parent.removeClass('has-error');
			$parent.find('.help-block').hide();
		},
		errorPlacement: function(error, el) {
			error.appendTo(el.parents('.form-group').find('div:first'));
		},
		submitHandler: function(form) {
			var $theForm = $(form);
			var $data = $theForm.serialize(); 
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url()?>room_type/save',
				data: $data,
				beforeSend: function(){
					$('#save').button('loading');
				},
				complete: function() {
					$('#save').button('reset');
				},
				success: function(data) {
					if(data=='true')
						$('#cancel').click();
					
				}
			});
			return false;
		}
	});	


	/*========= SAVE SETTING ===========*/
	$('#save_setting').click(function(){
		$('#form_setting').submit(); 
	});

	$('#form_setting_container').find('form').validate({
		errorClass: 'help-block',
		errorElement: 'span',
		ignore: 'input[type=hidden]',
		highlight: function(el, errorClass) {
			$(el).parents('.form-group').first().addClass('has-error');
		},
		unhighlight: function(el, errorClass) {
			var $parent = $(el).parents('.form-group').first();
			$parent.removeClass('has-error');
			$parent.find('.help-block').hide();
		},
		errorPlacement: function(error, el) {
			error.appendTo(el.parents('.form-group').find('div:first'));
		},
		submitHandler: function(form) {
			var $theForm = $(form);
			var $data = $theForm.serialize(); 
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url()?>room_type/savesetting',
				data: $data,
				beforeSend: function(){
					$('#save_setting').button('loading');
				},
				complete: function() {
					$('#save_setting').button('reset');
				},
				success: function(data) {
					if(data=='true')
						loadTbl();
						$('#modal_detil').modal('hide');
					
				}
			});
			return false;
		}
	});	
});

function loadTbl(){
	$("#refrensi-table").dataTable().fnDraw();
}

//BUAT EDIT
function set_val(data){
	var isi=data.split('|');
	
	document.getElementById('act').value 		='edit';
        document.getElementById('id').value 		=isi[0];
	document.getElementById('roomType').value 	=isi[1];
	document.getElementById('size').value 		=isi[2];
	document.getElementById('bedType').value 	=isi[3];
	document.getElementById('extraBed').value 	=isi[4];
	document.getElementById('breakfast').value 	=isi[5];
//	document.getElementById('priceA').value 	=isi[6];
//	document.getElementById('priceB').value 		=isi[7];
//	document.getElementById('priceC').value 		=isi[8];
//	document.getElementById('priceD').value 		=isi[9];
//	document.getElementById('priceE').value 	=isi[10];
//	document.getElementById('surcharge').value 	=isi[11];
//	document.getElementById('surcharge2').value 	=isi[11];
	
//	$('#userID').attr('readonly', true);
//	$('#staffName').val('');
//	$('#staffUsername').val('');
//	$('#staffPassword').val('');
//	$('#staffName').attr('readonly', true);
//	$('#staffUsername').attr('readonly', true);
//	$('#staffPassword').attr('readonly', true);
//	$('#staffName').attr('required', false);
//	$('#staffUsername').attr('required', false);
//	$('#staffPassword').attr('required', false);
 
	$('#form_container').slideDown(500);
	$('html, body').animate({scrollTop: 0}, 500);
}

function set_del(data){
	if(confirm("Are you sure to delete this data ?")){
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url() ?>room_type/delete',
			data: { id : data },
			success: function(data) {
				if(data=='true'){
					//$('#cancel').click();
					loadTbl();
				}else if(data=='false'){
					$('#cancel').click();
				}
			}
		});
	}
}

function set_setting(data){
	var isi=data.split('|'); //$row->id|$row->isStatus|$row->isProfile|$row->isPassword
	$('#agent_ID').val(isi[0]);
	if(isi[1]=='0'){
		$('#isStatus_0').prop('checked', true);
		$('#isStatus_1').prop('checked', false);		
	}else{
		$('#isStatus_0').prop('checked', false);
		$('#isStatus_1').prop('checked', true);		
	}

	if(isi[2]=='0'){
		$('#isProfile_0').prop('checked', true);
		$('#isProfile_1').prop('checked', false);		
	}else{
		$('#isProfile_0').prop('checked', false);
		$('#isProfile_1').prop('checked', true);			
	}

	if(isi[3]=='0'){
		$('#isPassword_0').prop('checked', true);
		$('#isPassword_1').prop('checked', false);		
	}else{
		$('#isPassword_0').prop('checked', false);
		$('#isPassword_1').prop('checked', true);			
	}

	/*
	document.getElementById('act').value 		='edit';
	document.getElementById('id').value 		=isi[0];
	document.getElementById('agentName').value 	=isi[1];
	document.getElementById('companyName').value 		=isi[2];
	document.getElementById('address').value 	=isi[3];
	document.getElementById('zipcode').value 	=isi[4];
	document.getElementById('city').value 		=isi[5];
	document.getElementById('country').value 	=isi[6];
	document.getElementById('phone').value 		=isi[7];
	document.getElementById('fax').value 		=isi[8];
	document.getElementById('email').value 		=isi[9];
	document.getElementById('userID').value 	=isi[10];
	//document.getElementById('password').value 	=isi[11];
	document.getElementById('contactName').value=isi[12];
	document.getElementById('contactPosition').value 	=isi[13]; 
	document.getElementById('contactEmail').value 		=isi[14]; 
	document.getElementById('contactMobile').value 		=isi[15]; 
	document.getElementById('contactWA').value 	=isi[16]; 
	document.getElementById('contactBBM').value =isi[17]; 

	$('#userID').attr('readonly', true);
	$('#staffName').val('');
	$('#staffUsername').val('');
	$('#staffPassword').val('');
	$('#staffName').attr('readonly', true);
	$('#staffUsername').attr('readonly', true);
	$('#staffPassword').attr('readonly', true);
	$('#staffName').attr('required', false);
	$('#staffUsername').attr('required', false);
	$('#staffPassword').attr('required', false);
 
	$('#form_container').slideDown(500);
	$('html, body').animate({scrollTop: 0}, 500);
	*/
	$('#modal_detil').modal('show');
}

</script>

<?php include "include_footer.php"; ?>