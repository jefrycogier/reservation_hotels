<?php
// We change the headers of the page so that the browser will know what sort of file is dealing with. Also, we will tell the browser it has to treat the file as an attachment which cannot be cached.
$nama_file = str_replace(" ","-",strtoupper($nama->productName.'-'.date("dM", strtotime($nama->depdate))));

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=".$nama_file.".xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1">
	<thead>
		<th>PNR</th>
		<th>TITTLE</th>
		<th>FIRST NAME</th>
		<th>LAST NAME</th>
		<th>DOB</th>
		<th>PASSPORT NO</th>
		<th>MOBILE</th>
		<th>AGENT</th>
		<th>STATUS</th>
	</thead>
	<tbody>
		<?php
		foreach ($all as $value) {
            if($value->bookingStatus==0){ $txt = 'WL'; } 
            if($value->bookingStatus==1){ $txt = 'DP-NP'; } 
            if($value->bookingStatus==2){ $txt = 'DP-R'; } 
            if($value->bookingStatus==3){ $txt = 'FP-NP'; } 
            if($value->bookingStatus==4){ $txt = 'FP-R'; } 
            if($value->bookingStatus==5){ $txt = 'COMPLETED'; } 
            if($value->bookingStatus==6){ $txt = 'CANCEL'; } 
			echo "<tr>";
			echo "	<td>'".$value->bookingCode."</td>";
			echo "	<td>".$value->gtittle."</td>";
			echo "	<td>".$value->gfname."</td>";
			echo "	<td>".$value->glname."</td>";
			echo "	<td>'".$value->gdob."</td>";
			echo "	<td>'".$value->gpassport."</td>";
			echo "	<td>'".$value->gmobile."</td>";
			echo "	<td>".strtoupper($value->userID)."</td>";
			echo "	<td>".$txt."</td>";
			echo "</tr>";
		}
		?>
	</tbody>
</table>