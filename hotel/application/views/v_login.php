<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title><?php echo $title; ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Reservation Application Administrator" name="description" />
        <meta content="2016 © Multikreasi Abadi Selaras" name="author" />
        <?php include "additional_plugin_header_login.php"; ?>
    <!-- END HEAD -->

    <body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="<?php echo base_url('login/') ?>">
                <img src="http://thegreatholiday.com/logo.png" alt="" />
            </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <?php
                $attributes = array(
                    'class'         => 'login-form', 
                    'id'            => 'form_login', 
                    'autocomplete'  => 'off'
                );
                echo form_open('login/login_proccess', $attributes);
                echo '<h3 class="form-title font-red">Administrator</h3>';
                echo validation_errors('<div class="alert alert-danger display-hide">','</div>');
            ?>
            <div class="form-group">
                <?php
                    echo '<label class="control-label visible-ie8 visible-ie9">Username</label>';
                    $data = array(
                        'name'          => 'userID',
                        'id'            => 'userID',
                        'maxlength'     => '100',
                        'size'          => '50',
                        'class'         => 'form-control form-control-solid placeholder-no-fix',
                        'placeholder'   => 'Username'
                    );
                    echo form_input($data);
                ?>
            </div>
            <div class="form-group">
            <?php
            $data = array(
                    'name'          => 'password',
                    'id'            => 'password',
                    'maxlength'     => '100',
                    'size'          => '50',
                    'class'         => 'form-control form-control-solid placeholder-no-fix',
                    'placeholder'   => 'Password'
            );

            echo form_password($data);
            ?>
            </div>
            Forgot Password?<a href="<?php echo base_url('login/reset_password') ?>"> Reset Here</a>
            <div class="form-actions">
            <?php
                echo form_submit('btn_login','Login','class="btn green uppercase"'); 
            ?>
            </div>
            <?php 
                echo form_close(); 
            ?>
            <!-- END LOGIN FORM -->
        </div>
        <div class="copyright"> 2016 © Multikreasi Abadi Selaras</div>
        <?php include "additional_plugin_footer_login.php"; ?>
    </body>

</html>