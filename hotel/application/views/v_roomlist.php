<?php include "include_header.php"; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<script type="text/javascript" src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>

<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-table"></i>
					<small>ROOM LIST</small>
				</div>
			</div>
			<div class="portlet-body">
				<div id="view-table">
					<div class="table-toolbar"></div>
					<table class="table table-striped table-bordered table-hover" id="refrensi-table">
						<thead>
							<tr>
								<th data-sortable="false">NO</th>
								<th>PRODUCT NAME</th>
								<th>DEPARTURE</th>
								<th>PAX</th>
								<th data-sortable="false">ACTION</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				<div id="view-tree">
					<div id="list-tree" style="margin:15px; font-size:14px;"></div>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_detil" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Room List &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#" class="btn btn-circle purple-plum btn-xs" id="btn_download"><i class="fa fa-download"></i> Download</a></h4>
            </div>
            <div class="modal-body"> 
            	<!--span id="load_data"></span-->
            	<iframe id="load_data" src="" width="100%" height="300" frameborder="0" border="0" cellspacing="0"
        style="border-style: none;"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script> 
	var TableAdvanced = function () {
	var initTable1 = function() {

		/*
		* Initialize DataTables, with no sorting on the 'details' column
		*/
		var target='#refrensi-table';
		var oTable = $(target).dataTable( {
			"aoColumnDefs": [
				{
					//"bSortable": false, "aTargets": [ 0,1 ]
				}
			],
			"aoColumns": [
                { "sWidth": "5%" },  
                { "sWidth": "30%" }, 
                { "sWidth": "15%" }, 
                { "sWidth": "15%" }, 
                { "sWidth": "15%" }
            ],
			"aaSorting": [[1, 'asc']],
			"aLengthMenu": [
				[10, 20, 50, -1],
				[10, 20, 50, "All"] // change per page values here
			],
			// set the initial value
			"iDisplayLength": 10, // default records per page
			"oLanguage": {
				// language settings
				"sLengthMenu": "Display _MENU_ records",
				"sSearch": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
				"sProcessing": '<img src="<?php echo base_url() ?>assets/global/img/loading-spinner-grey.gif"><span>&nbsp;&nbsp;Loading...</span>',
				"sInfoEmpty": "No records found to show",
				"sAjaxRequestGeneralError": "Could not complete request. Please check your internet connection",
				"sEmptyTable":  "No data available in table",
				"sZeroRecords": "No matching records found",
				"oPaginate": {
					"sPrevious": "Prev",
					"sNext": "Next",
					"sPage": "Page",
					"sPageOf": "of"
				}
			},
			"bAutoWidth": true,   // disable fixed width and enable fluid table
			"bSortCellsTop": true, // make sortable only the first row in thead
			"sPaginationType": "bootstrap_full_number", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
			"bProcessing": true, // enable/disable display message box on record load
			"bServerSide": true, // enable/disable server side ajax loading
			"sAjaxSource": "<?=base_url()?>roomlist/get_data", // define ajax source URL
			"sServerMethod": "POST"
		});

		$(target+'_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
		$(target+'_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
		$(target+'_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
		$(target+'_wrapper .dataTables_filter input').unbind();
		$(target+'_wrapper .dataTables_filter input').bind('keyup', function(e) {
			if(e.keyCode == 13) {
				oTable.fnFilter(this.value);
			}
		});
		$(target+'_wrapper .dataTables_filter a').bind('click', function(e) {
			var key=$(target+'_wrapper .dataTables_filter input').val();
			oTable.fnFilter(key);
		});
	}

	return {

		//main function to initiate the module
		init: function () {
			if (!$().dataTable) {
				return;
			}
			initTable1();
		}

	};

}();

$(document).ready(function() {
	TableAdvanced.init();
});

function loadTbl(){
	$("#refrensi-table").dataTable().fnDraw();
}

function view_detil(data){
	$("#load_data").attr("src", "<?php echo base_url() ?>roomlist/view/"+data);
	$("#btn_download").attr("href", "<?php echo base_url() ?>excel/download/"+data);
	$('#modal_detil').modal('show');
}

</script>

<?php include "include_footer.php"; ?>