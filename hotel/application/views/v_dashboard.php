<?php include "include_header.php"; ?>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption caption-md">
                    <i class="icon-bar-chart font-dark hide"></i>
                    <span class="caption-subject font-green-steel uppercase bold">Last Booking</span>
                </div>
            </div>
            <div class="portlet-body">
            <small>
            Status List :<br>
            <span class="label label-sm label-default">WL</span> WAITING LIST 
            <span class="label label-sm label-info">DP-NP</span> DEPOSIT NOT PAID 
            <span class="label label-sm label-success">DP-R</span> DEPOSIT RECEIVED 
            <span class="label label-sm label-warning">FP-NP</span> FULL PAYMENT NOT PAID 
            <span class="label label-sm label-primary">FP-R</span> FULL PAYMENT RECEIVED 
            <span class="label label-sm label-danger">COMPLETED</span> COMPLETED 
            <span class="label label-sm label-default">CANCEL</span> CANCEL 
            </small>
            <hr>
            <br>
                <table class="table table-bordered table-hover">
                    <thead>
                        <th>BOOKED</th>
                        <th>PNR</th>
                        <th>PRODUCT</th>
                        <th>DEPARTURE</th>
                        <th>GUEST</th>
                        <th>STATUS</th>
                        <th>-</th>
                    </thead>
                    <tbody>
                    <?php 
                if(empty($list_booking)){
                    echo '<tr><td colspan="8"><em>No data available</em></td></tr>';
                }else{

                    foreach ($list_booking as $row) {
                        ?>
                        <tr>
                            <td><?php echo strtoupper(date("d M", strtotime($row->createdDate))); ?></td>
                            <td><?php echo strtoupper($row->bookingCode); ?></td>
                            <td><?php echo strtoupper($row->productName); ?></td>
                            <td><?php echo strtoupper($row->cityName.' / '.date("d M", strtotime($row->depdate))); ?></td>
                            <td><?php echo strtoupper($row->guesttotal.' / '.$row->gfname); ?></td>
                            <td>
                            <?php 
                                if($row->bookingStatus==0){ echo '<span class="label label-sm label-default">WL</span>'; } 
                                if($row->bookingStatus==1){ echo '<span class="label label-sm label-info">DP-NP</span>'; } 
                                if($row->bookingStatus==2){ echo '<span class="label label-sm label-success">DP-R</span>'; } 
                                if($row->bookingStatus==3){ echo '<span class="label label-sm label-warning">FP-NP</span>'; } 
                                if($row->bookingStatus==4){ echo '<span class="label label-sm label-primary">FP-R</span>'; } 
                                if($row->bookingStatus==5){ echo '<span class="label label-sm label-danger">COMPLETED</span>'; } 
                                if($row->bookingStatus==6){ echo '<span class="label label-sm label-default">CANCEL</span>'; } 
                            ?>
                            </td>
                            <td><a href="<?php echo base_url().'booking_all/detils/'.$row->id; ?>" class="btn btn-xs btn-info" title="View Detail"> View Detail <i class="fa fa-play-circle"></i></a></td>
                        </tr>
                        <?php
                    }
                }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php include "include_footer.php"; ?>