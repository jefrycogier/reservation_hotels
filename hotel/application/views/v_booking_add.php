<?php include "include_header.php"; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<script type="text/javascript" src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<form role="form" class="form-horizontal" id="form_master" method="post">
<input type="hidden" name="productID" value="<?php echo $all->id; ?>">
<div class="row">
	<div class="col-md-12">
	  <div class="portlet box green">
		<div class="portlet-title">
			<div class="caption">
				<small> A. PRODUCT TOUR DETAIL </small>
			</div>
			<div class="actions">
				<div class="btn-group btn-group-solid"> </div>
			</div>
		</div>
		<div class="portlet-body form" id="form_container" style="display:block;">
			<div class="form-body">
			<div id="alert"></div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="inputSuccess">Departure City <span class="required">*</span></label>
					<div class="col-md-6">
						<b><?php echo $all->cityName; ?></b>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="inputSuccess">Airline <span class="required">*</span></label>
					<div class="col-md-6">
						<b><?php echo $all->airlineName; ?></b>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="inputSuccess">Category <span class="required">*</span></label>
					<div class="col-md-6">
						<b><?php echo $all->categoryName; ?></b>
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label" for="inputSuccess">Days <span class="required">*</span></label>
					<div class="col-md-2">
						<b><?php echo $all->noofdays; ?></b>
					</div>
					<div class="col-md-1"> days </div>
					<div class="col-md-2">
						<b><?php echo $all->noofnights; ?></b>
					</div>
					<div class="col-md-1"> nights </div>
				</div>
			</div>
		</div>
	   </div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
	  <div class="portlet box green">
		<div class="portlet-title">
			<div class="caption">
				<small> B. TOUR DETAILS </small>
			</div>
			<div class="actions">
				<div class="btn-group btn-group-solid"> </div>
			</div>
		</div>
		<div class="portlet-body form" id="form_container" style="display:block;">
			<div class="form-body">
				<div class="form-group">
					<label class="col-md-3 control-label" for="inputSuccess">Tour Name <span class="required">*</span></label>
					<div class="col-md-6">
						<b><?php echo $all->productName; ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="inputSuccess">Highlight <span class="required">*</span></label>
					<div class="col-md-6">
						<b><?php echo $all->highlight; ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="inputSuccess">General Remarks <span class="required">*</span></label>
					<div class="col-md-6">
						<b><?php echo $all->generalRemarks; ?>
					</div>
				</div>
			</div>
		</div>
	   </div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
	  <div class="portlet box green">
		<div class="portlet-title">
			<div class="caption">
				<small> C. DEPARTURE DATE </small>
			</div>
			<div class="actions">
				<div class="btn-group btn-group-solid">
				<!--a class="btn default" href="javascript:;" id="btn_add_depdate" onclick="add_depdate()"><i class="fa fa-plus-square"></i> Add Another Data</a-->
				</div>
			</div>
		</div>
		<div class="portlet-body form" id="form_container" style="display:block;">
			<div class="form-body">
				<table class="table table-bordered table-hover" id="tb_depdate">
					<thead>
						<tr>
							<th>DEP. DATE</th>
							<th>ALLOTMENT</th>
							<th>DP</th>
							<th>ATT</th>
							<th>AS</th>
							<th>CWA</th>
							<th>CWB</th>
							<th>CNB</th>
							<th>SURCHARGE</th>
							<th>EX. SURCHARGE</th>
							<th>DISC</th>
							<th>EX. DISC</th>
							<th>--</th>
						</tr>
					</thead>
					<tbody>
					<?php
					foreach ($list_depdate as $value) {
					?>
						<tr>
							<td><?php echo date("d M Y", strtotime($value->depdate)); ?></td>
							<td><?php echo $value->allotment; ?></td>
							<td><?php echo number_format($value->dpAmount); ?></td>
							<td><?php echo number_format($value->patt); ?></td>
							<td><?php echo number_format($value->pas); ?></td>
							<td><?php echo number_format($value->pcwa); ?></td>
							<td><?php echo number_format($value->pcwb); ?></td>
							<td><?php echo number_format($value->pcnb); ?></td>
							<td><?php echo number_format($value->surcharge); ?></td>
							<td><?php echo number_format($value->surcharge2); ?></td>
							<td><?php echo number_format($value->discount); ?></td>
							<td><?php echo number_format($value->discount2); ?></td>
							<td><input type="radio" name="depdateID" value="<?php echo $value->id; ?>" /></td>
						</tr>
					<?php
					}
					?>
					</tbody>					
				</table>
			</div>
		</div>
	   </div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	
});	
</script>

<div class="row">
	<div class="col-md-12">
	  <div class="portlet box green">
		<div class="portlet-title">
			<div class="caption">
				<small> D. ROOM CONFIGURATION  </small>
			</div>
			<div class="actions">
				<div class="btn-group btn-group-solid"></div>
			</div>
		</div>
		<div class="portlet-body form" id="form_container" style="display:block;">
			<div class="form-body">
				<div class="form-group">
					<div class="col-md-6">
						<div class="mt-checkbox-list">
							<?php
							foreach ($list_room as $data) {
							?>
							<label class="mt-radio"> <?php echo $data->settingName; ?>
                                <input type="radio" name="roomID" value="<?php echo $data->id; ?>" onclick="handleClick('<?php echo $data->id.'|'.$data->as.'|'.$data->att.'|'.$data->cwa.'|'.$data->cwb.'|'.$data->cnb; ?>');" /> <span></span>
                            </label>
							<?php
							}
							?>
                        </div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<div style="border:1px solid black;width:100%;overflow-y:hidden;overflow-x:scroll;">
							<table class="table table-bordered table-hover" id="tb_guest">
								<thead>
									<th>TYPE</th>
									<th>TITTLE</th>
									<th>FIRST NAME</th>
									<th>LAST NAME</th>
									<th>DOB</th>
									<th>PASSPORT NO</th>
								</thead>
								<tbody id="details_tr">
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="form-actions center">
				<div class="col-md-offset-4 col-md-8">
					<button id="save" type="button" class="btn blue"  data-loading-text="Loading..."  tabindex="5">Save</button>
					<button id="cancel" type="button" class="btn default"  tabindex="6">Cancel</button>
				</div>
			</div>
		</div>
	   </div>
	</div>
</div>

</form>

<script> 
function handleClick(dt){
	$('#tb_guest tbody').empty();
	var data = dt.split('|'); console.log(dt); //2|0|2|0|0|0
	//$data->id.'|'.$data->as.'|'.$data->att.'|'.$data->cwa.'|'.$data->cwb.'|'.$data->cnb;
	var as = data[1]/1;
	var att = data[2]/1;
	var cwa = data[3]/1;
	var cwb = data[4]/1;
	var cnb = data[5]/1;
	var i = $('#details_tr').size();
	var total = as+att+cwa+cwb+cnb;
	//for (var i=0; i<total; i++) {
		if(as>0){
			for (var a = 1; a<=as; a++) {
				$('#details_tr').append('<tr><td><input type="hidden" name="gtype[]" value="0">ADULT SINGLE</td><td><select name="gtittle[]"><option value="MR">MR</option><option value="MRS">MRS</option><option value="MS">Ms</option></select></td><td><input type="text" name="gfname[]"></td><td><input type="text" name="glname[]"></td><td><input type="text" name="gdob[]"></td><td><input type="text" name="gpassport[]"></td></tr>');
			}
		}
		if(att>0){
			for (var a = 1; a<=att; a++) {
				$('#details_tr').append('<tr><td><input type="hidden" name="gtype[]" value="1">ADULT TWIN</td><td><select name="gtittle[]"><option value="MR">MR</option><option value="MRS">MRS</option><option value="MS">Ms</option></select></td><td><input type="text" name="gfname[]"></td><td><input type="text" name="glname[]"></td><td><input type="text" name="gdob[]"></td><td><input type="text" name="gpassport[]"></td></tr>');
			}
		}
		if(cwa>0){
			for (var a = 1; a<=cwa; a++) {
				$('#details_tr').append('<tr><td><input type="hidden" name="gtype[]" value="2">CWA</td><td><select name="gtittle[]"><option value="MR">MR</option><option value="MRS">MRS</option><option value="MS">Ms</option></select></td><td><input type="text" name="gfname[]"></td><td><input type="text" name="glname[]"></td><td><input type="text" name="gdob[]"></td><td><input type="text" name="gpassport[]"></td></tr>');
			}
		}
		if(cwb>0){
			for (var a = 1; a<=cwb; a++) {
				$('#details_tr').append('<tr><td><input type="hidden" name="gtype[]" value="3">CWB</td><td><select name="gtittle[]"><option value="MR">MR</option><option value="MRS">MRS</option><option value="MS">Ms</option></select></td><td><input type="text" name="gfname[]"></td><td><input type="text" name="glname[]"></td><td><input type="text" name="gdob[]"></td><td><input type="text" name="gpassport[]"></td></tr>');
			}
		}
		if(cnb>0){
			for (var a = 1; a<=cnb; a++) {
				$('#details_tr').append('<tr><td><input type="hidden" name="gtype[]" value="4">CNB</td><td><select name="gtittle[]"><option value="MR">MR</option><option value="MRS">MRS</option><option value="MS">Ms</option></select></td><td><input type="text" name="gfname[]"></td><td><input type="text" name="glname[]"></td><td><input type="text" name="gdob[]"></td><td><input type="text" name="gpassport[]"></td></tr>');
			}
		}
	//}
	//console.log(total);
/*
	$('#details_tr').append('<tr id="tr_'+res.length+'"><td>'+dt_detil[0]+'</td><td>'+dt_detil[1]+'</td><td>'+dt_detil[3]/1+'</td><td>'+dt_detil[4]+'</td><td>'+dt_detil[5]+'</td><td>'+dt_detil[6]+'</td><td>'+dt_detil[7]+'</td><td>'+dt_detil[8]+'</td><td>'+dt_detil[9]+'</td><td>'+dt_detil[10]+'</td><td>'+dt_detil[11]+'</td><td>'+dt_detil[12]+'</td><td><a href="javascript:void(0)" onclick="delRow('+res.length+')" class="btn btn-xs btn-danger" title="Delete"><i class="fa fa-trash-o"></i></a></td></tr>');
	i++;
*/
}

$(document).ready(function() {

	$('#save').click(function(){
		$('#form_master').submit(); 
	});
	
	$('#cancel').click(function(){
		document.getElementById('form_master').reset();
		$('#id').val('');
		$('#act').val('add');
		$('#form_master')[0].reset();
		document.location.href = '<?php echo base_url()?>booking_dpnotpaid';
	});
	
	$("#form_master").submit(function(e){
		var $data = $(this).serialize();
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url()?>booking_add/save',
			data: $data,
			beforeSend: function(){
				$('#save').button('loading');
			},
			complete: function() {
				$('#save').button('reset');
			},
			success: function(data) {
				if(data=='true')
					$('#cancel').click();
				
			}
		});
		return false;
	});

	/*
	$('#form_master').find('form').validate({
		alert();
		errorClass: 'help-block',
		errorElement: 'span',
		ignore: 'input[type=hidden]',
		highlight: function(el, errorClass) {
			$(el).parents('.form-group').first().addClass('has-error');
		},
		unhighlight: function(el, errorClass) {
			var $parent = $(el).parents('.form-group').first();
			$parent.removeClass('has-error');
			$parent.find('.help-block').hide();
		},
		errorPlacement: function(error, el) {
			error.appendTo(el.parents('.form-group').find('div:first'));
		},
		submitHandler: function(form) {
			var $theForm = $(form);
			var $data = $theForm.serialize(); 
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url()?>product_add/save',
				data: $data,
				beforeSend: function(){
					$('#save').button('loading');
				},
				complete: function() {
					$('#save').button('reset');
				},
				success: function(data) {
					if(data=='true')
						$('#cancel').click();
					
				}
			});
			return false;
		}
	});
	*/
});
</script>

<?php include "include_footer.php"; ?>