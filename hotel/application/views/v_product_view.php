<?php include "include_header.php"; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<script type="text/javascript" src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>

<form role="form" class="form-horizontal" id="form_master" method="post">
<input type="text" name="id" id="id" value="<?php echo $all->id; ?>">
<div class="row">
	<div class="col-md-12">
	  <div class="portlet box green">
		<div class="portlet-title">
			<div class="caption">
				<small> A. GENERAL INFORMATION </small>
			</div>
			<div class="actions">
				<div class="btn-group btn-group-solid"> </div>
			</div>
		</div>
		<div class="portlet-body form" id="form_container" style="display:block;">
			<div class="form-body">
			<div id="alert"></div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="inputSuccess">Departure City <span class="required">*</span></label>
					<div class="col-md-6">					
						<select class="form-control" name="departureID" id="departureID">
						<?php 
							foreach ($list_departure as $data) {
								if($all->departureID==$data->id){ $txt = 'selected'; } else { $txt = ''; }
								echo '<option value="'.$data->id.'" '.$txt.'>'.$data->airportCode.' - '.$data->cityName.'</option>';
							}
						?>								
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="inputSuccess">Airline <span class="required">*</span></label>
					<div class="col-md-6">
						<select class="form-control" name="airlineID" id="airlineID">
						<?php 
							foreach ($list_airline as $data) {
								if($all->airlineID==$data->id){ $txt = 'selected'; } else { $txt = ''; }
								echo '<option value="'.$data->id.'" '.$txt.'>'.$data->airlineCode.' - '.$data->airlineName.'</option>';
							}
						?>								
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="inputSuccess">Category <span class="required">*</span></label>
					<div class="col-md-6">
						<select class="form-control" name="categoryID" id="categoryID">
						<?php 
							foreach ($list_category as $data) {
								if($all->categoryID==$data->id){ $txt = 'selected'; } else { $txt = ''; }
								echo '<option value="'.$data->id.'" '.$txt.'>'.$data->categoryCode.' - '.$data->categoryName.'</option>';
							}
						?>								
						</select>
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label" for="inputSuccess">Days <span class="required">*</span></label>
					<div class="col-md-2">
						<select class="form-control" name="noofdays" id="noofdays">
							<?php
							for($i=3;$i<=18;$i++){ 
								if($all->noofdays==$i){ $txt = 'selected'; } else { $txt = ''; }
								echo '<option value="'.$i.'" '.$txt.'>'.$i.'</option>';
							}
							?>
						</select>
					</div>
					<div class="col-md-1"> days </div>
					<div class="col-md-2">
						<select class="form-control" name="noofnights" id="noofnights">
							<?php
							for($i=2;$i<=17;$i++){ 
								if($all->noofnights==$i){ $txt = 'selected'; } else { $txt = ''; }
								echo '<option value="'.$i.'" '.$txt.'>'.$i.'</option>';
							}
							?>
						</select>
					</div>
					<div class="col-md-1"> nights </div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="inputSuccess">Time Limit <span class="required">*</span></label>
					<div class="col-md-2">
						<select class="form-control" name="timelimit" id="timelimit">
							<?php
							for($i=2;$i<=5;$i++){ 
								if($all->timelimit==$i){ $txt = 'selected'; } else { $txt = ''; }
								echo '<option value="'.$i.'" '.$txt.'>'.$i.'</option>';
							}
							?>
						</select>
					</div>
					<div class="col-md-1"> days </div>
				</div>
			</div>
		</div>
	   </div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
	  <div class="portlet box green">
		<div class="portlet-title">
			<div class="caption">
				<small> B. TOUR DETAILS </small>
			</div>
			<div class="actions">
				<div class="btn-group btn-group-solid"> </div>
			</div>
		</div>
		<div class="portlet-body form" id="form_container" style="display:block;">
			<div class="form-body">
				<div class="form-group">
					<label class="col-md-3 control-label" for="inputSuccess">Tour Name <span class="required">*</span></label>
					<div class="col-md-6">
						<input name="productName" type="text" required="true" class="form-control" id="productName" value="<?php echo $all->productName; ?>">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="inputSuccess">Highlight</label>
					<div class="col-md-6">
						<textarea name="highlight" id="highlight" data-provide="markdown" rows="5"><?php echo $all->highlight; ?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="inputSuccess">General Remarks</label>
					<div class="col-md-6">
						<textarea name="generalRemarks" id="generalRemarks" data-provide="markdown" rows="5"><?php echo $all->generalRemarks; ?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label" for="inputSuccess">File Itinerary <span class="required">*</span></label>
					<div class="col-md-3">
						<input type="file" name="userfile" id="userfile"> 
					</div>
					<div class="col-md-3">
						<span id="ketfile"></span>
					</div>
				</div>
				<input type="hidden" name="fileItinerary" id="fileItinerary" value="<?php echo $all->fileItinerary; ?>">
			</div>
		</div>
	   </div>
	</div>
</div>

<script type="text/javascript">
	/*=== Upload file otomatis saat pilih file ===*/
	$(document).ready(function() {
		if($('#fileItinerary').val()!=''){
			$('#userfile').val('');
			$('#ketfile').html('<a class="btn green btn-xs" href="<?php echo base_url()?>product_files/'+$('#fileItinerary').val()+'" target="_blank"><i class="fa fa-search"></i> View File</a> <a class="btn red btn-xs" onclick="deletefile()"><i class="fa fa-trash"></i> Delete</a>');
		}
		$("#userfile").change(function () {
		    uploadFile();
		});
	});

	function uploadFile(){
		if (($("#userfile"))[0].files.length > 0) {
		    var file = $("#userfile")[0].files[0];
			var formdata = new FormData();
			formdata.append("userfile", file);
			var ajax = new XMLHttpRequest();
			ajax.upload.addEventListener("progress", progressHandler2, false);
			ajax.addEventListener("load", completeHandler2, false);
			ajax.addEventListener("error", errorHandler2, false);
			ajax.addEventListener("abort", abortHandler2, false);
			ajax.open("POST", "<?php echo base_url()?>utils/uploadproductfile/");
			ajax.send(formdata);
		} else {
		    alert("No file chosen!");
		}
	}
	function progressHandler2(event){
		var percent = (event.loaded / event.total) * 100;
		//console.log(percent);
	}
	function completeHandler2(event){
		var data = event.target.responseText.split('*');
		if(data[0]!=''){
			alert(data[1]);
		}else{
			$('#fileItinerary').val(data[1]);
			$('#userfile').val('');
			$('#ketfile').html('<a class="btn green btn-xs" href="<?php echo base_url()?>product_files/'+data[1]+'" target="_blank"><i class="fa fa-search"></i> View File</a> <a class="btn red btn-xs" onclick="deletefile()"><i class="fa fa-trash"></i> Delete</a>');
		}	
	}
	function errorHandler2(event){
		alert("Upload Failed");
	}
	function abortHandler2(event){
		alert("Upload Aborted");
	}
	function deletefile(){
		if(confirm("Are you sure to Delete this file?")){
			/*== Hapus file dari folder 10/11 (Not working :"(()==*/
			 $.ajax({
		        url: '<?php echo base_url()?>utils/deleteproductfile/',
		        type: "POST",
		        data:{'filename':$('#fileItinerary').val()},
		        success: function(data){
		            //console.log(data);
		        }
		    });

			$('#fileItinerary').val('');
			$('#userfile').val('');
			$('#ketfile').html('');
			alert('Deleted successfully.');
		}
	}
</script>

<div class="row">
	<div class="col-md-12">
	  <div class="portlet box green">
		<div class="portlet-title">
			<div class="caption">
				<small> C. DEPARTURE DATE </small>
			</div>
			<div class="actions">
				<div class="btn-group btn-group-solid">
				<a class="btn default" href="javascript:;" id="btn_add_depdate" onclick="add_depdate()"><i class="fa fa-plus-square"></i> Add Another Data</a>
				</div>
			</div>
		</div>
		<div class="portlet-body form" id="form_container" style="display:block;">
			<div class="form-body">
				<table class="table table-bordered table-hover" id="tb_depdate">
					<thead>
						<tr>
							<th>DEP. DATE</th>
							<th>ALLOTMENT</th>
							<th>DP</th>
							<th>ATT</th>
							<th>AS</th>
							<th>CWA</th>
							<th>CWB</th>
							<th>CNB</th>
							<th>SUR.</th>
							<th>EX. SUR.</th>
							<th>DISC</th>
							<th>EX. DISC</th>
							<th>--</th>
						</tr>
					</thead>
					<tbody id="details_tr">
					</tbody>					
				</table>
			</div>
		</div>
	   </div>
	</div>
</div>

<div class="modal fade" id="mdl_add_depdate" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Departure Date</h4>
            </div>
            <div class="modal-body">
            <input type="text" name="depdateID" id="depdateID">
				<fieldset>
					<legend>Date & Allotment</legend>
	            	<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Departure Date <span class="required">*</span></label>
						<div class="col-md-6">
							<input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="<?php echo date('m/d/Y'); ?>" required="true" name="depdate" id="depdate" /> <small>Format. mm/dd/yyyy</small><!-- mm/dd/yyyy -->
						</div>
					</div>
	            	<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Seat Allotment <span class="required">*</span></label>
						<div class="col-md-6">
							<select class="form-control" name="allotment" id="allotment" required="true" />
								<?php
								for($i=15;$i<=45;$i++){
									echo '<option value="'.$i.'">'.$i.'</option>';
								}
								?>
							</select>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>Price</legend>
	            	<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Deposit</label>
						<div class="col-md-6">
							<input class="form-control" type="text" value="0" name="dpAmount" id="dpAmount" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" />
						</div>
					</div>
	            	<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Adult Single</label>
						<div class="col-md-6">
							<input class="form-control" type="text" value="0" name="pas" id="pas" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" />
						</div>
					</div>
	            	<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Adult Twin / Triple</label>
						<div class="col-md-6">
							<input class="form-control" type="text" value="0" name="patt" id="patt" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" />
						</div>
					</div>
	            	<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Child With Adult</label>
						<div class="col-md-6">
							<input class="form-control" type="text" value="0" name="pcwa" id="pcwa" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" />
						</div>
					</div>
	            	<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Child With Bed</label>
						<div class="col-md-6">
							<input class="form-control" type="text" value="0" name="pcwb" id="pcwb" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" />
						</div>
					</div>
	            	<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Child No Bed</label>
						<div class="col-md-6">
							<input class="form-control" type="text" value="0" name="pcnb" id="pcnb" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" />
						</div>
					</div>
	            	<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Surcharge</label>
						<div class="col-md-6">
							<input class="form-control" type="text" value="0" name="surcharge" id="surcharge" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" />
						</div>
					</div>
	            	<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Extra Surcharge</label>
						<div class="col-md-6">
							<input class="form-control" type="text" value="0" name="surcharge2" id="surcharge2" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" />
						</div>
					</div>
	            	<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Discount</label>
						<div class="col-md-6">
							<input class="form-control" type="text" value="0" name="discount" id="discount" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" />
						</div>
					</div>
	            	<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Extra Discount</label>
						<div class="col-md-6">
							<input class="form-control" type="text" value="0" name="discount2" id="discount2" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Remarks</label>
						<div class="col-md-6">
							<textarea class="form-control" name="remarks" id="remarks" /></textarea>
						</div>
					</div>
				</fieldset>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green" id="btn_depdate_save">Add</button>
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="mdl_edit_depdate" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Edit Departure Date</h4>
            </div>
            <div class="modal-body">
            <input type="text" name="eid" id="eid">
            <input type="text" name="edepdateID" id="edepdateID">
				<fieldset>
					<legend>Date & Allotment</legend>
	            	<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Departure Date <span class="required">*</span></label>
						<div class="col-md-6">
							<input class="form-control form-control-inline input-medium date-picker" size="16" type="text" value="<?php echo date('m/d/Y'); ?>" required="true" name="edepdate" id="edepdate" /> <small>Format. mm/dd/yyyy</small><!-- mm/dd/yyyy -->
						</div>
					</div>
	            	<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Seat Allotment <span class="required">*</span></label>
						<div class="col-md-6">
							<select class="form-control" name="eallotment" id="eallotment" required="true" />
								<?php
								for($i=15;$i<=45;$i++){
									echo '<option value="'.$i.'">'.$i.'</option>';
								}
								?>
							</select>
						</div>
					</div>
				</fieldset>
				<fieldset>
					<legend>Price</legend>
	            	<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Deposit</label>
						<div class="col-md-6">
							<input class="form-control" type="text" value="0" name="edpAmount" id="edpAmount" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" />
						</div>
					</div>
	            	<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Adult Single</label>
						<div class="col-md-6">
							<input class="form-control" type="text" value="0" name="epas" id="epas" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" />
						</div>
					</div>
	            	<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Adult Twin / Triple</label>
						<div class="col-md-6">
							<input class="form-control" type="text" value="0" name="epatt" id="epatt" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" />
						</div>
					</div>
	            	<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Child With Adult</label>
						<div class="col-md-6">
							<input class="form-control" type="text" value="0" name="epcwa" id="epcwa" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" />
						</div>
					</div>
	            	<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Child With Bed</label>
						<div class="col-md-6">
							<input class="form-control" type="text" value="0" name="epcwb" id="epcwb" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" />
						</div>
					</div>
	            	<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Child No Bed</label>
						<div class="col-md-6">
							<input class="form-control" type="text" value="0" name="epcnb" id="epcnb" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" />
						</div>
					</div>
	            	<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Surcharge</label>
						<div class="col-md-6">
							<input class="form-control" type="text" value="0" name="esurcharge" id="esurcharge" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" />
						</div>
					</div>
	            	<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Extra Surcharge</label>
						<div class="col-md-6">
							<input class="form-control" type="text" value="0" name="esurcharge2" id="esurcharge2" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" />
						</div>
					</div>
	            	<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Discount</label>
						<div class="col-md-6">
							<input class="form-control" type="text" value="0" name="ediscount" id="ediscount" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" />
						</div>
					</div>
	            	<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Extra Discount</label>
						<div class="col-md-6">
							<input class="form-control" type="text" value="0" name="ediscount2" id="ediscount2" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label" for="inputSuccess">Remarks</label>
						<div class="col-md-6">
							<textarea class="form-control" name="eremarks" id="eremarks" /></textarea>
						</div>
					</div>
				</fieldset>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green" id="btn_depdate_edit_save">Update</button>
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<input type="text" name="isi" id="isi" class="form-control span12" value="<?php echo $list_depdate; ?>">

<script type="text/javascript">
$(document).ready(function() {
	/*== Menghilangkan value 0 saat akan di edit 10/11===*/
	$('#dpAmount,#patt,#pas,#pcwa,#pcwb,#pcnb,#surcharge,#surcharge2,#discount,#discount2').on('click focusin', function() {
	    this.value = '';
	});

	localStorage.clear();

	setawal();

	/*== Add Depdate ==*/
	$("#btn_depdate_save").click(function() {		
	    var isiData = localStorage.getItem("isiData");
		if((isiData==null)||(isiData=='')){
			var prefix = '';
		}else{
			var prefix = localStorage.getItem("isiData")+'*';
		}

		var data 		= $('#depdate').val();
		var pisah		= data.split("/");
		var dt_depdate 	= pisah[1]+'-'+pisah[0]+'-'+pisah[2];
		//depdate|allotment|remarks|dpAmount|patt|pas|pcwa|pcwb|pcnb|surcharge|surcharge2|discount|discount2
		if ($('#allotment').val() != ''){
			var dt_allotment = $('#allotment').val(); 
		} else { 
			var dt_allotment = '0'; 
		}
		var dt_remarks 	= $('#remarks').val();
		if ($('#dpAmount').val() != ''){ 
			var dt_dpAmount = $('#dpAmount').val(); 
		} else { 
			var dt_dpAmount = '0'; 
		}
		if ($('#patt').val() != ''){ 
			var dt_patt = $('#patt').val(); 
		} else { 
			var dt_patt = '0'; 
		}
		if ($('#pas').val() != ''){ 
			var dt_pas = $('#pas').val(); 
		} else { var dt_pas = '0'; 
		}
		if ($('#pcwa').val() != ''){ 
			var dt_pcwa = $('#pcwa').val(); 
		} else { 
			var dt_pcwa = '0'; 
		}
		if ($('#pcwb').val() != ''){ 
			var dt_pcwb = $('#pcwb').val(); 
		} else { 
			var dt_pcwb = '0'; 
		}
		if ($('#pcnb').val() != ''){ 
			var dt_pcnb = $('#pcnb').val(); 
		} else { 
			var dt_pcnb = '0'; 
		}
		if ($('#surcharge').val() != ''){ 
			var dt_surcharge = $('#surcharge').val(); 
		} else { 
			var dt_surcharge = '0'; 
		}
		if ($('#surcharge2').val() != ''){ 
			var dt_surcharge2 = $('#surcharge2').val(); 
		} else { 
			var dt_surcharge2 = '0'; 
		}
		if ($('#discount').val() != ''){ 
			var dt_discount = $('#discount').val(); 
		} else { 
			var dt_discount = '0'; 
		}
		if ($('#discount2').val() != ''){ 
			var dt_discount2 = $('#discount2').val(); 
		} else { 
			var dt_discount2 = '0'; 
		}

		var dataBaru	= dt_depdate+'|'+dt_allotment+'|'+dt_remarks+'|'+dt_dpAmount+'|'+dt_patt+'|'+dt_pas+'|'+dt_pcwa+'|'+dt_pcwb+'|'+dt_pcnb+'|'+dt_surcharge+'|'+dt_surcharge2+'|'+dt_discount+'|'+dt_discount2+'|0';
		var updatedata 	= prefix+''+dataBaru;

		localStorage.setItem("isiData", updatedata);
		var isiData = localStorage.getItem("isiData");
		
		$('#depdate').val('<?php echo date('m/d/Y'); ?>');
		$("#isi").val(updatedata);
		addRow(isiData);
		return false; // dont post it automatically
	});

	/*== Update Depdate ==*/
	$("#btn_depdate_edit_save").click(function() {	
	    var dt = $('#isi').val();
	    var ps = dt.split("*");

	    var output = '';
	    $.each(ps, function (index, value) {
		  if(index==$('#eid').val()-1){
			  	var data 		= $('#edepdate').val();
				var pisah		= data.split("/");
				var dt_depdate 	= pisah[1]+'-'+pisah[0]+'-'+pisah[2];
				//depdate|allotment|remarks|dpAmount|patt|pas|pcwa|pcwb|pcnb|surcharge|surcharge2|discount|discount2
				if ($('#eallotment').val() != ''){
					var dt_allotment = $('#eallotment').val(); 
				} else { 
					var dt_allotment = '0'; 
				}
				var dt_remarks 	= $('#eremarks').val();
				if ($('#edpAmount').val() != ''){ 
					var dt_dpAmount = $('#edpAmount').val(); 
				} else { 
					var dt_dpAmount = '0'; 
				}
				if ($('#epatt').val() != ''){ 
					var dt_patt = $('#epatt').val(); 
				} else { 
					var dt_patt = '0'; 
				}
				if ($('#epas').val() != ''){ 
					var dt_pas = $('#epas').val(); 
				} else { var dt_pas = '0'; 
				}
				if ($('#epcwa').val() != ''){ 
					var dt_pcwa = $('#epcwa').val(); 
				} else { 
					var dt_pcwa = '0'; 
				}
				if ($('#epcwb').val() != ''){ 
					var dt_pcwb = $('#epcwb').val(); 
				} else { 
					var dt_pcwb = '0'; 
				}
				if ($('#epcnb').val() != ''){ 
					var dt_pcnb = $('#epcnb').val(); 
				} else { 
					var dt_pcnb = '0'; 
				}
				if ($('#esurcharge').val() != ''){ 
					var dt_surcharge = $('#esurcharge').val(); 
				} else { 
					var dt_surcharge = '0'; 
				}
				if ($('#esurcharge2').val() != ''){ 
					var dt_surcharge2 = $('#esurcharge2').val(); 
				} else { 
					var dt_surcharge2 = '0'; 
				}
				if ($('#ediscount').val() != ''){ 
					var dt_discount = $('#ediscount').val(); 
				} else { 
					var dt_discount = '0'; 
				}
				if ($('#ediscount2').val() != ''){ 
					var dt_discount2 = $('#ediscount2').val(); 
				} else { 
					var dt_discount2 = '0'; 
				}

				var dataBaru	= dt_depdate+'|'+dt_allotment+'|'+dt_remarks+'|'+dt_dpAmount+'|'+dt_patt+'|'+dt_pas+'|'+dt_pcwa+'|'+dt_pcwb+'|'+dt_pcnb+'|'+dt_surcharge+'|'+dt_surcharge2+'|'+dt_discount+'|'+dt_discount2+'|'+$('#edepdateID').val();
				//var updatedata 	= prefix+''+dataBaru;

				output += dataBaru+'*';
		  }else{
		  	output += value+'*';
		  }
		});

		localStorage.setItem("isiData", output);
		var isiData = localStorage.getItem("isiData");
		
		//$('#depdate').val('<?php echo date('m/d/Y'); ?>');
		$("#isi").val(output);
		$('#mdl_edit_depdate').modal('hide');
		setafteredit();
		return false; // dont post it automatically
	});

	/*== Set after edit depdate 11/11==*/
	function setafteredit(){
		$("#details_tr").empty();

		var isiData = localStorage.getItem("isiData");
		if((isiData==null)||(isiData=='')){
			var prefix = '';
		}else{
			var prefix = localStorage.getItem("isiData")+'*';
		}

		var updatedata = $('#isi').val();
		localStorage.setItem("isiData", updatedata);
		var isiData = localStorage.getItem("isiData");
		
		setRow(isiData);
		return false;
	}

	/*== Set awal depdate 10/11==*/
	function setawal(){
		var isiData = localStorage.getItem("isiData");
		if((isiData==null)||(isiData=='')){
			var prefix = '';
		}else{
			var prefix = localStorage.getItem("isiData")+'*';
		}

		var updatedata = $('#isi').val();
		localStorage.setItem("isiData", updatedata);
		var isiData = localStorage.getItem("isiData");
		
		setRow(isiData);
		return false;
	}

	function setRow(x){
		var res = x.split("*");
		var urutan = res.length-1;
		for(var i=0;i<urutan;i++){
			if(res[i]!=''){
				var dt_detil = res[i].split("|");
				//var i = $('#details_tr').size() + 1;
				$('#details_tr').append('<tr id="tr_'+(i+1)+'"><td>'+dt_detil[0]+'</td><td>'+dt_detil[1]+'</td><td>'+dt_detil[3]/1+'</td><td>'+dt_detil[4]+'</td><td>'+dt_detil[5]+'</td><td>'+dt_detil[6]+'</td><td>'+dt_detil[7]+'</td><td>'+dt_detil[8]+'</td><td>'+dt_detil[9]+'</td><td>'+dt_detil[10]+'</td><td>'+dt_detil[11]+'</td><td>'+dt_detil[12]+'</td><td><a href="javascript:void(0)" onclick="editRow('+(i+1)+')" class="btn btn-xs btn-info" title="Delete"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" onclick="delRow('+(i+1)+')" class="btn btn-xs btn-danger" title="Delete"><i class="fa fa-trash-o"></i></a></td></tr>');
			}
		}
	}

	function addRow(x){
		var res = x.split("*");
		var urutan = res.length-1;

		var dt_detil = res[urutan].split("|");
	
		var i = $('#details_tr').size() + 1;
		$('#details_tr').append('<tr id="tr_'+res.length+'"><td>'+dt_detil[0]+'</td><td>'+dt_detil[1]+'</td><td>'+dt_detil[3]/1+'</td><td>'+dt_detil[4]+'</td><td>'+dt_detil[5]+'</td><td>'+dt_detil[6]+'</td><td>'+dt_detil[7]+'</td><td>'+dt_detil[8]+'</td><td>'+dt_detil[9]+'</td><td>'+dt_detil[10]+'</td><td>'+dt_detil[11]+'</td><td>'+dt_detil[12]+'</td><td><a href="javascript:void(0)" onclick="editRow('+res.length+')" class="btn btn-xs btn-info" title="Delete"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0)" onclick="delRow('+res.length+')" class="btn btn-xs btn-danger" title="Delete"><i class="fa fa-trash-o"></i></a></td></tr>');
		i++;

		$('#mdl_add_depdate').modal('hide');
		//return false;
	}

	/*
	$( "#delAll" ).click(function() {
		localStorage.clear();
		$("#details_tr").empty();
	});
	*/
});	

	function editRow(x){
		var isiData = localStorage.getItem("isiData");
		var res 	= isiData.split("*");
		var dt_detil = res[x-1].split("|");
		//depdate|allotment|remarks|dpAmount|patt|pas|pcwa|pcwb|pcnb|surcharge|surcharge2|discount|discount2
		$('#eallotment').val(dt_detil[1]).attr('selected','selected');
		$('#eremarks').val(dt_detil[2]);
		$('#edpAmount').val(dt_detil[3]);
		$('#epatt').val(dt_detil[4]);
		$('#epas').val(dt_detil[5]);
		$('#epcwa').val(dt_detil[6]);
		$('#epcwb').val(dt_detil[7]);
		$('#epcnb').val(dt_detil[8]);
		$('#esurcharge').val(dt_detil[9]);
		$('#esurcharge2').val(dt_detil[10]);
		$('#ediscount').val(dt_detil[11]);
		$('#ediscount2').val(dt_detil[12]);
		$('#eid').val(x);
		$('#edepdateID').val(dt_detil[13]);
		$('#mdl_edit_depdate').modal('show');
	}

	function delRow(x){
		if(confirm("Are you sure to delete this data?")){		
			var isiData = localStorage.getItem("isiData");
			var res 	= isiData.split("*");
			var hapus 	= isiData.replace(res[x-1], "");

			localStorage.setItem("isiData", hapus);
			var isiData = localStorage.getItem("isiData");

			$("#isi").val(hapus);

			$("#tr_"+x).closest("tr").remove();
		}
	}

	function add_depdate(){
		$('#depdateID').val('0');
		$('#mdl_add_depdate').modal('show');
		//$('#tb_depdate tbody').append('<tr><td>1</td><td>1</td><td>1</td><td>1</td></tr>');
	}
</script>

<div class="row">
	<div class="col-md-12">
	  <div class="portlet box green">
		<div class="portlet-title">
			<div class="caption">
				<small> D. ROOM SETTING </small>
			</div>
			<div class="actions">
				<div class="btn-group btn-group-solid"></div>
			</div>
		</div>
		<div class="portlet-body form" id="form_container" style="display:block;">
			<div class="form-body">
				<div class="form-group">
					<div class="col-md-6">
						<div class="mt-checkbox-list">
							<?php
							foreach ($list_room as $data) {
								$string = $list_product_room;
								$pisah = explode('|',$string);
								$hasil = array_search($data->id,$pisah);
								if(($hasil!='')&&($hasil!=(count($pisah)-1))){
									$txt = 'checked';
								}else{
									$txt = '';
								}
							?>
							<label class="mt-checkbox"> <?php echo $data->settingName; ?>
                                <input type="checkbox" value="<?php echo $data->id; ?>" name="roomID[]" <?php echo $txt; ?> />
                                <span></span>
                            </label>
							<?php
							}
							?>
                        </div>
					</div>
				</div>
			</div>
			<div class="form-actions center">
				<div class="col-md-offset-4 col-md-8">
					<button id="save" type="button" class="btn blue"  data-loading-text="Loading..."  tabindex="5">Save</button>
					<button id="cancel" type="button" class="btn default"  tabindex="6">Cancel</button>
				</div>
			</div>
		</div>
	   </div>
	</div>
</div>

</form>

<script> 
$(document).ready(function() {

	$('#save').click(function(){
		$('#form_master').submit(); 
	});
	
	$('#cancel').click(function(){
		document.getElementById('form_master').reset();
		$('#id').val('');
		$('#act').val('add');
		$('#form_master')[0].reset();
		document.location.href = '<?php echo base_url()?>product';
	});
	
	$("#form_master").submit(function(e){
		var $data = $(this).serialize();
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url()?>product_add/save',
			data: $data,
			beforeSend: function(){
				$('#save').button('loading');
			},
			complete: function() {
				$('#save').button('reset');
			},
			success: function(data) {
				if(data=='true')
					$('#cancel').click();
				
			}
		});
		return false;
	});

	/*
	$('#form_master').find('form').validate({
		alert();
		errorClass: 'help-block',
		errorElement: 'span',
		ignore: 'input[type=hidden]',
		highlight: function(el, errorClass) {
			$(el).parents('.form-group').first().addClass('has-error');
		},
		unhighlight: function(el, errorClass) {
			var $parent = $(el).parents('.form-group').first();
			$parent.removeClass('has-error');
			$parent.find('.help-block').hide();
		},
		errorPlacement: function(error, el) {
			error.appendTo(el.parents('.form-group').find('div:first'));
		},
		submitHandler: function(form) {
			var $theForm = $(form);
			var $data = $theForm.serialize(); 
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url()?>product_add/save',
				data: $data,
				beforeSend: function(){
					$('#save').button('loading');
				},
				complete: function() {
					$('#save').button('reset');
				},
				success: function(data) {
					if(data=='true')
						$('#cancel').click();
					
				}
			});
			return false;
		}
	});
	*/
});
</script>

<?php include "include_footer.php"; ?>