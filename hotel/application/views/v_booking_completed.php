<?php include "include_header.php"; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<script type="text/javascript" src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>

<!--div class="row">
	<div class="col-md-12">
	  <div class="portlet box green">
		<div class="portlet-title">
			<div class="caption">
				<small> <i class="fa fa-note"></i> </small>
			</div>
			<div class="actions">
				 <div class="btn-group btn-group-solid">
				<a class="btn default" href="javascript:;" id="form_act"><i class="fa fa-plus-square"></i>  Add New Product Tour</a>
				</div>
			</div>
		</div>
	   </div>
	</div>
</div-->

<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-table"></i>
					<small>Booking List - COMPLETED</small>
				</div>
			</div>
			<div class="portlet-body">
				<div id="view-table">
					<div class="table-toolbar"></div>
					<table class="table table-striped table-bordered table-hover" id="refrensi-table">
						<thead>
							<tr>
								<th data-sortable="false">NO</th>
								<th>BOOKED</th>
								<th>PNR</th>
								<th>AGENT</th>
								<th>PRODUCT</th>
								<th>DEPARTURE</th>
								<th>GUEST</th>
								<th data-sortable="false">ACTION</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_detil" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Detil</h4>
            </div>
            <div class="modal-body"> 
            	<span id="load_data"></span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script> 
	var TableAdvanced = function () {
	var initTable1 = function() {

		/*
		* Initialize DataTables, with no sorting on the 'details' column
		*/
		var target='#refrensi-table';
		var oTable = $(target).dataTable( {
			"aoColumnDefs": [
				{
					//"bSortable": false, "aTargets": [ 0,1 ]
				}
			],
			"aoColumns": [
                { "sWidth": "5%" },  
                { "sWidth": "10%" }, 
                { "sWidth": "10%" }, 
                { "sWidth": "15%" }, 
                { "sWidth": "30%" }, 
                { "sWidth": "10%" },
                { "sWidth": "10%" },
                { "sWidth": "15%" }
            ],
			"aaSorting": [[1, 'asc']],
			"aLengthMenu": [
				[10, 20, 50, -1],
				[10, 20, 50, "All"] // change per page values here
			],
			// set the initial value
			"iDisplayLength": 10, // default records per page
			"oLanguage": {
				// language settings
				"sLengthMenu": "Display _MENU_ records",
				"sSearch": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
				"sProcessing": '<img src="<?php echo base_url() ?>assets/global/img/loading-spinner-grey.gif"><span>&nbsp;&nbsp;Loading...</span>',
				"sInfoEmpty": "No records found to show",
				"sAjaxRequestGeneralError": "Could not complete request. Please check your internet connection",
				"sEmptyTable":  "No data available in table",
				"sZeroRecords": "No matching records found",
				"oPaginate": {
					"sPrevious": "Prev",
					"sNext": "Next",
					"sPage": "Page",
					"sPageOf": "of"
				}
			},
			"bAutoWidth": true,   // disable fixed width and enable fluid table
			"bSortCellsTop": true, // make sortable only the first row in thead
			"sPaginationType": "bootstrap_full_number", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
			"bProcessing": true, // enable/disable display message box on record load
			"bServerSide": true, // enable/disable server side ajax loading
			"sAjaxSource": "<?=base_url()?>booking_completed/get_data", // define ajax source URL
			"sServerMethod": "POST"
		});

		$(target+'_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
		$(target+'_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
		$(target+'_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
		$(target+'_wrapper .dataTables_filter input').unbind();
		$(target+'_wrapper .dataTables_filter input').bind('keyup', function(e) {
			if(e.keyCode == 13) {
				oTable.fnFilter(this.value);
			}
		});
		$(target+'_wrapper .dataTables_filter a').bind('click', function(e) {
			var key=$(target+'_wrapper .dataTables_filter input').val();
			oTable.fnFilter(key);
		});
	}

	return {

		//main function to initiate the module
		init: function () {
			if (!$().dataTable) {
				return;
			}
			initTable1();
		}

	};

}();

$(document).ready(function() {
	TableAdvanced.init();

	
});

function loadTbl(){
	$("#refrensi-table").dataTable().fnDraw();
}

//BUAT EDIT
function set_val(data){
	var isi=data.split('|');
	
	document.getElementById('act').value='edit';
	document.getElementById('id').value=isi[0];
	document.getElementById('username').value=isi[1];
	document.getElementById('password').value=isi[2];
	document.getElementById('fullname').value=isi[3];
	document.getElementById('position').value=isi[4]; 
 
	$('#form_container').slideDown(500);
	$('html, body').animate({scrollTop: 0}, 500);
}

function set_del(data){
	if(confirm("Are you sure to cancel this booking?")){
		loadTbl();
		/*
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url() ?>product/delete',
			data: { id : data },
			success: function(data) {
				if(data=='true'){
					//$('#cancel').click();
					loadTbl();
				}
			}
		});
		*/
	}
}

function set_view(data){
	var myWindow = window.open("<?php echo base_url('booking_completed/detil/') ?>/"+data, "", "width=500,height=700");
}

function view_detil(data){
	//$value->depdate.'|'.$value->dpAmount.'|'.$value->pas.'|'.$value->patt.'|'.$value->pcwa.'|'.$value->pcwb.'|'.$value->pcnb.'|'.$value->surcharge.'|'.$value->surcharge2.'|'.$value->discount.'|'.$value->discount2.'|'.$value->allotment.'|'.$value->remarks.'*';
	var isi=data.split('*');
	var output = '';
	for (var i = isi.length - 1; i >= 0; i--) {
		output += '<ol>';
		var pisah = isi[i].split('|');
		output += '<li>Departure Date '+pisah[0]+'</li>';
		output += '</ol>';
	}
	$('#load_data').html(output);

	$('#modal_detil').modal('show');
}

</script>

<?php include "include_footer.php"; ?>