<?php include "include_header.php"; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
<script type="text/javascript" src="<?=base_url()?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>

<div class="row">
	<div class="col-md-12">
	  <div class="portlet box green">
		<div class="portlet-title">
			<div class="caption">
				<small> Master Data <i class="fa fa-angle-right"></i> Others <i class="fa fa-angle-right"></i> Term & Condition </small>
			</div>
			<div class="actions">
				 <div class="btn-group btn-group-solid">
				<a class="btn default" href="javascript:;" id="form_act"><i class="fa fa-plus-square"></i> Add New Data</a>
				</div>
			</div>
		</div>
		<div class="portlet-body form" id="form_container" style="display:none;">
			<form role="form" class="form-horizontal" id="form_master" method="post">
			<input type="hidden" name="act" id="act" value="add"/>
			<input type="hidden" name="id" id="id" value=""/>
				<div class="form-body">
				<div id="alert"></div>
					<fieldset>
						<legend>A. GENERAL INFORMATION</legend>
						<div class="form-group">
							<label class="col-md-3 control-label" for="inputSuccess">Content <span class="required">*</span></label>
							<div class="col-md-6">
								<div class="input-icon right">
									<i class="fa fa-info-circle tooltips" data-original-title="Content of Term and Condition (required)" data-container="body"></i>
									<input name="content" type="text" required="true" class="form-control" id="content">
								</div>
							</div>
						</div>
					</fieldset>			
				</div>
				<div class="form-actions center">
					<div class="col-md-offset-4 col-md-8">
						<button id="cancel" type="button" class="btn default">Cancel</button>
						<button id="save" type="button" class="btn blue"  data-loading-text="Loading...">Save</button>
					</div>
				</div>
			</form>
		</div>
	   </div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-table"></i>
					<small>Term & Condition List</small>
				</div>
			</div>
			<div class="portlet-body">
				<div id="view-table">
					<div class="table-toolbar"></div>
					<table class="table table-striped table-bordered table-hover" id="refrensi-table">
						<thead>
							<tr>
								<th data-sortable="false">No</th>
								<th>Sort No</th>
								<th>Content</th>
								<th data-sortable="false">Action</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				<div id="view-tree">
					<div id="list-tree" style="margin:15px; font-size:14px;"></div>
				</div>

			</div>
		</div>
	</div>
</div>

<script> 
	var TableAdvanced = function () {
	var initTable1 = function() {

		/*
		* Initialize DataTables, with no sorting on the 'details' column
		*/
		var target='#refrensi-table';
		var oTable = $(target).dataTable( {
			"aoColumnDefs": [
				{
					//"bSortable": false, "aTargets": [ 0,1 ]
				}
			],
			"aoColumns": [
                { "sWidth": "5%" },  
                { "sWidth": "10%" }, 
                { "sWidth": "70%" }, 
                { "sWidth": "15%" }
            ],
			"aaSorting": [[1, 'asc']],
			"aLengthMenu": [
				[10, 20, 50, -1],
				[10, 20, 50, "All"] // change per page values here
			],
			// set the initial value
			"iDisplayLength": 10, // default records per page
			"oLanguage": {
				// language settings
				"sLengthMenu": "Display _MENU_ records",
				"sSearch": "Search _INPUT_ <a class='btn default bts' href='javascript:void(0);'><i class='fa fa-search'></i></a>",
				"sProcessing": '<img src="<?php echo base_url() ?>assets/global/img/loading-spinner-grey.gif"><span>&nbsp;&nbsp;Loading...</span>',
				"sInfoEmpty": "No records found to show",
				"sAjaxRequestGeneralError": "Could not complete request. Please check your internet connection",
				"sEmptyTable":  "No data available in table",
				"sZeroRecords": "No matching records found",
				"oPaginate": {
					"sPrevious": "Prev",
					"sNext": "Next",
					"sPage": "Page",
					"sPageOf": "of"
				}
			},
			"bAutoWidth": true,   // disable fixed width and enable fluid table
			"bSortCellsTop": true, // make sortable only the first row in thead
			"sPaginationType": "bootstrap_full_number", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
			"bProcessing": true, // enable/disable display message box on record load
			"bServerSide": true, // enable/disable server side ajax loading
			"sAjaxSource": "<?=base_url()?>term/get_data", // define ajax source URL
			"sServerMethod": "POST"
		});

		$(target+'_wrapper .dataTables_filter input').addClass("form-control input-small input-inline"); // modify table search input
		$(target+'_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
		$(target+'_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
		$(target+'_wrapper .dataTables_filter input').unbind();
		$(target+'_wrapper .dataTables_filter input').bind('keyup', function(e) {
			if(e.keyCode == 13) {
				oTable.fnFilter(this.value);
			}
		});
		$(target+'_wrapper .dataTables_filter a').bind('click', function(e) {
			var key=$(target+'_wrapper .dataTables_filter input').val();
			oTable.fnFilter(key);
		});
	}

	return {

		//main function to initiate the module
		init: function () {
			if (!$().dataTable) {
				return;
			}
			initTable1();
		}

	};

}();

$(document).ready(function() {
	TableAdvanced.init();

	$('#form_act').bind('click', function(e) {
		//alert();
		//$('#modal_add').modal('show');
		$('#form_container').slideToggle(500);
		$('#form_master')[0].reset();
	});

	$('#save').click(function(){
		$('#form_master').submit(); 
	});
	
	$('#cancel').click(function(){
		document.getElementById('form_master').reset();
		$('#id').val('');
		$('#act').val('add');
		$('#form_container').slideUp(500);
		loadTbl();
	});
	
	$('#form_container').find('form').validate({
		errorClass: 'help-block',
		errorElement: 'span',
		ignore: 'input[type=hidden]',
		highlight: function(el, errorClass) {
			$(el).parents('.form-group').first().addClass('has-error');
		},
		unhighlight: function(el, errorClass) {
			var $parent = $(el).parents('.form-group').first();
			$parent.removeClass('has-error');
			$parent.find('.help-block').hide();
		},
		errorPlacement: function(error, el) {
			error.appendTo(el.parents('.form-group').find('div:first'));
		},
		submitHandler: function(form) {
			var $theForm = $(form);
			var $data = $theForm.serialize(); 
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url()?>term/save',
				data: $data,
				beforeSend: function(){
					$('#save').button('loading');
				},
				complete: function() {
					$('#save').button('reset');
				},
				success: function(data) {
					if(data=='true')
						$('#cancel').click();
					
				}
			});
			return false;
		}
	});	

});

function loadTbl(){
	$("#refrensi-table").dataTable().fnDraw();
}

//BUAT EDIT
function set_val(data){
	var isi=data.split('|');
	
	document.getElementById('act').value 		='edit';
	document.getElementById('id').value 		=isi[0];
	document.getElementById('content').value 	=isi[1];
 
	$('#form_container').slideDown(500);
	$('html, body').animate({scrollTop: 0}, 500);
}

function set_del(data){
	if(confirm("Are you sure to delete this data ?")){
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url() ?>term/delete',
			data: { id : data },
			success: function(data) {
				if(data=='true'){
					//$('#cancel').click();
					loadTbl();
				}else if(data=='false'){
					$('#cancel').click();
				}
			}
		});
	}
}

</script>

<?php include "include_footer.php"; ?>