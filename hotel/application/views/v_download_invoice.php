<table width="100%" border="0" style="border-collapse: collapse; border-color: #989898; font-family: Segoe, Segoe UI, DejaVu Sans, Trebuchet MS, Verdana,' sans-serif'; font-size: 12px;">
  <tbody>
	<tr>
	  <td style="padding: 5px; text-align: left;"><img src="http://thegreatholiday.com/assets/images/logo.png" width="277px" height="76px" />
	  </td>
	</tr>
  </tbody>
</table>
<br>
<table width="100%" border="1" style="border-collapse: collapse; border-color: #989898; font-family: Segoe, Segoe UI, DejaVu Sans, Trebuchet MS, Verdana,' sans-serif'; font-size: 12px;">
  <tbody>
	<tr>
	  <td style="padding: 5px; text-align: left;">FINAL PAYMENT INVOICE</td>
	</tr>
  </tbody>
</table>
<br>
<table width="100%" border="1" style="border-collapse: collapse; border-color: #989898; font-family: Segoe, Segoe UI, DejaVu Sans, Trebuchet MS, Verdana,' sans-serif'; font-size: 12px;">
  <tbody>
	<tr>
	  <td colspan="2" style="padding: 5px; text-align: left;">Product Details</td>
	  <td colspan="2" style="padding: 5px; text-align: left;">Reservation Details</td>
	</tr>
	<tr>
	  <td width="15%" style="padding: 5px; text-align: left;">Product</td>
	  <td width="50%" style="padding: 5px; text-align: left;"><b><?php echo strtoupper($data_product->productName); ?></b></td>
	  <td width="15%" style="padding: 5px; text-align: left;">Booking No</td>
	  <td width="20%" style="padding: 5px; text-align: left;"><b><?php echo $data_booking->bookingCode; ?></b></td>
	</tr>
	<tr>
	  <td style="padding: 5px; text-align: left;">Dep City</td>
	  <td style="padding: 5px; text-align: left;"><b><?php echo $data_product->cityName; ?></b></td>
	  <td style="padding: 5px; text-align: left;">Booked</td>
	  <td style="padding: 5px; text-align: left;"><b><?php echo date("d M Y", strtotime($data_booking->createdDate)); ?></b></td>
	</tr>
	<tr>
	  <td style="padding: 5px; text-align: left;">Dep Date</td>
	  <td style="padding: 5px; text-align: left;"><b><?php echo date("d M Y", strtotime($data_booking->depdate)); ?></b></td>
	  <td style="padding: 5px; text-align: left;">Status</td>
	  <td style="padding: 5px; text-align: left;"><b>
	  <?php if($data_booking->bookingStatus<2) echo 'NOT PAID'; else echo 'PAID'; ?>
	  </b></td>
	</tr>
	<tr>
	  <td style="padding: 5px; text-align: left;">Airline</td>
	  <td style="padding: 5px; text-align: left;"><b><?php echo $data_product->airlineName; ?></b></td>
	  <td style="padding: 5px; text-align: left;">No Of Adult</td>
	  <td style="padding: 5px; text-align: left;"><b><?php echo $data_adult->TOTAL; ?> pax</b></td>
	</tr>
	<tr>
	  <td style="padding: 5px; text-align: left;">Days</td>
	  <td style="padding: 5px; text-align: left;"><b><?php echo $data_product->noofdays.'D'.$data_product->noofnights.'N'; ?></b></td>
	  <td style="padding: 5px; text-align: left;">No Of Child</td>
	  <td style="padding: 5px; text-align: left;"><b><?php echo $data_child->TOTAL; ?> pax</b></td>
	</tr>
  </tbody>
</table>
<br>
<table width="100%" border="1" style="border-collapse: collapse; border-color: #989898; font-family: Segoe, Segoe UI, DejaVu Sans, Trebuchet MS, Verdana,' sans-serif'; font-size: 12px;">
  <tbody>
	<tr>
	  <td colspan="7" style="padding: 5px; text-align: left;">Guest Details</td>
	</tr>
	<tr>
	  <td width="5%" style="padding: 5px; text-align: left;">No</td>
	  <td width="10%" style="padding: 5px; text-align: left;">Tittle</td>
	  <td width="20%" style="padding: 5px; text-align: left;">First Name</td>
	  <td width="20%" style="padding: 5px; text-align: left;">Last Name</td>
	  <td width="10%" style="padding: 5px; text-align: left;">Cat.</td>
	  <td width="15%" style="padding: 5px; text-align: left;">DOB</td>
	  <td style="padding: 5px; text-align: left;">Tour Price</td>
	</tr>
	<?php 
	  	$no = 1;
		foreach ($data_guest as $value) {
			$data_generate->tourPrice; //9*21500000|10*21500000|11*21500000|
			$pisah = explode("|", $data_generate->tourPrice);
			$tourPrice = '';
			for ($i=0; $i < count($pisah); $i++) { 
				$pisahlagi = explode("*", $pisah[$i]);
				if($pisahlagi[0]==$value->id){
					$tourPrice = $pisahlagi[1];
				}
			}
	?>
	<tr>
	  <td style="padding: 5px; text-align: left;"><?php echo $no; ?></td>
	  <td style="padding: 5px; text-align: left;"><?php echo $value->gtittle; ?></td>
	  <td style="padding: 5px; text-align: left;"><?php echo $value->gfname; ?></td>
	  <td style="padding: 5px; text-align: left;"><?php echo $value->glname; ?></td>
	  <td style="padding: 5px; text-align: left;">
	  	<?php
			if($value->gtype==0) echo 'SGL';
			if($value->gtype==1) echo 'ADL';
			if($value->gtype==2) echo 'CWA';
			if($value->gtype==3) echo 'CWB';
			if($value->gtype==4) echo 'CNB';
		?>
	  </td>
	  <td style="padding: 5px; text-align: left;"><?php echo $value->gdob; ?></td>
	  <td style="padding: 5px; text-align: left;"><?php echo number_format($tourPrice); ?></td>
	</tr>
	<?php $no++; } ?>
	<tr>
	  <td colspan="6" style="padding: 5px; text-align: right;"><b>Total Tour Price </b></td>
	  <td style="padding: 5px; text-align: left;"><?php echo number_format($data_generate->totTourPrice); ?></td>
	</tr>
	<tr>
	  <td colspan="6" style="padding: 5px; text-align: left;">Deposit Paid</td>
	  <td style="padding: 5px; text-align: left;">(-) <?php echo number_format($data_generate->totDP); ?></td>
	</tr>
	<tr>
	  <td colspan="6" style="padding: 5px; text-align: left;"><?php echo ucwords(strtolower($data_generate->addCharge)); ?></td>
	  <td style="padding: 5px; text-align: left;"><?php echo number_format($data_generate->addChargePrice); ?></td>
	</tr>
	<tr>
	  <td colspan="6" style="padding: 5px; text-align: left;"><?php echo ucwords(strtolower($data_generate->addCharge2)); ?></td>
	  <td style="padding: 5px; text-align: left;"><?php echo number_format($data_generate->addChargePrice2); ?></td>
	</tr>
	<tr>
	  <td colspan="6" style="padding: 5px; text-align: right;"><b>Grand Total </b></td>
	  <td style="padding: 5px; text-align: left;"><?php echo number_format($data_generate->grandTotal); ?></td>
	</tr>
  </tbody>
</table>
<br>
<table width="100%" border="1" style="border-collapse: collapse; border-color: #989898; font-family: Segoe, Segoe UI, DejaVu Sans, Trebuchet MS, Verdana,' sans-serif'; font-size: 12px;">
  <tbody>
	<tr>
	  <td style="padding: 5px; text-align: left;">Bank</td>
	</tr>
	<tr>
		<td><b><?php echo $data_bank->bankName; ?></b>, branch <b><?php echo $data_bank->branch; ?></b> (<?php echo $data_bank->currency; ?>)<br>
		<b><?php echo $data_bank->accountNo; ?></b><br><b><?php echo $data_bank->accountName; ?></b></td>
	</tr>
  </tbody>
</table>
<br>
<table width="100%" border="1" style="border-collapse: collapse; border-color: #989898; font-family: Segoe, Segoe UI, DejaVu Sans, Trebuchet MS, Verdana,' sans-serif'; font-size: 12px;">
  <tbody>
	<tr>
	  <td style="padding: 5px; text-align: left;">Term &amp; Conditions</td>
	</tr>
	<tr>
		<td>
			<?php
			echo '<ol>';
			foreach($data_term as $row){
				echo '<li>'.$row->content.'</li>';
			}
			echo '</ol>';
			?>
		</td>
	</tr>
  </tbody>
</table>
<br>
<table width="100%" border="1" style="border-collapse: collapse; border-color: #989898; font-family: Segoe, Segoe UI, DejaVu Sans, Trebuchet MS, Verdana,' sans-serif'; font-size: 12px;">
  <tbody>
	<tr>
	  <td style="padding: 5px; text-align: left;">This is a computer Generated Confirmation, signature is not required</td>
	</tr>
  </tbody>
</table>