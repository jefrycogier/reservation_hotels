<?php include "additional_plugin_header.php"; ?>
<table class="table table-bordered table-hover">
	<thead>
		<th>PNR</th>
		<th>TITTLE</th>
		<th>FIRST NAME</th>
		<th>LAST NAME</th>
		<th>DOB</th>
		<th>PASSPORT NO</th>
	</thead>
	<tbody>
		<?php
		foreach ($all as $value) {
            if($value->bookingStatus==0){ $txt = '<span class="label label-sm label-default">WL</span>'; } 
            if($value->bookingStatus==1){ $txt = '<span class="label label-sm label-info">DP-NP</span>'; } 
            if($value->bookingStatus==2){ $txt = '<span class="label label-sm label-success">DP-R</span>'; } 
            if($value->bookingStatus==3){ $txt = '<span class="label label-sm label-warning">FP-NP</span>'; } 
            if($value->bookingStatus==4){ $txt = '<span class="label label-sm label-primary">FP-R</span>'; } 
            if($value->bookingStatus==5){ $txt = '<span class="label label-sm label-danger">COMPLETED</span>'; } 
            if($value->bookingStatus==6){ $txt = '<span class="label label-sm label-default">CANCEL</span>'; } 
			echo '<tr>';
			echo '	<td>'.$value->bookingCode.'</td>';
			echo '	<td>'.$value->gtittle.'</td>';
			echo '	<td>'.$value->gfname.'</td>';
			echo '	<td>'.$value->glname.'</td>';
			echo '	<td>'.$value->gdob.'</td>';
			echo '	<td>'.$value->gpassport.'</td>';
			echo '	<td>'.$txt.'</td>';
			echo '</tr>';
		}
		?>
	</tbody>
</table>